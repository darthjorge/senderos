<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileGallery extends Model
{
    //
    protected $table = "profile_gallery";
}
