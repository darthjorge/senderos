<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
 	
 	protected $fillable = ['status_code', 'numero', 'mcuadrados', 'precio_unidad', 'precio_final', 'manzana'];

 	public function saveLote($data)
 	{
 	        
 	        $this->numero = $data['numero'];
 	        $this->mcuadrados = $data['mcuadrados'];
 	        $this->precio_unidad = $data['precio_unidad'];
 	        $this->precio_final = $data['precio_final'];
 	        $this->manzana = $data['manzana'];
 	        $this->status_code = $data['status_code'];
 	        $this->save();
 	        return 1;
 	}

 	# definiendo la relación
 	public function status_lote()
 	{
 	    return $this->belongsTo(App\Status_lote::class);
 	}
}
