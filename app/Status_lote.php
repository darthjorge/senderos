<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status_lote extends Model
{
    //
    protected $fillable = [
        'name', 'description'
    ];

    public function saveStatus($data)
    {
            
            $this->name = $data['name'];
            $this->description = $data['description'];
            $this->save();
            return 1;
    }

    public function updateStatus($data)
    {
            $status = $this->find($data['id']);
            $status->name = $data['name'];
            $status->description = $data['description'];
            $status->save();
            return 1;
    }

    public function lotes(){
        return $this->hasMany(App\Lotes::class);
    }
}
