<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileFooter extends Model
{
    //
    protected $table = "profile_footer";
}
