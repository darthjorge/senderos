<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status_lote;

class StatusController extends Controller
{
    //
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Status_lote::get();
        
        return view('status.index',compact('status'));
    }

	public function create()
	{
	   return view('status.create');
	}

   	public function store(Request $request)
   	{
       $status = new Status_lote();
       $data = $this->validate($request, [
           'name'=>'required',
           'description'=> 'required'
       ]);
      
       $status->saveStatus($data);
       return redirect('/status')->with('success', 'Nuevo Status ha sido creado.');
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       $status = Status_lote::where('id', $id)->first();

       return view('status.edit', compact('status', 'id'));
   }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       $status = new Status_lote();
       $data = $this->validate($request, [
       		'name'=> 'required',
          	'description'=>'required'
       ]);
       $data['id'] = $id;
       $status->updateStatus($data);

       return redirect('/status')->with('success', 'Se ha actualizado el status de los Lotes');
   }
}
