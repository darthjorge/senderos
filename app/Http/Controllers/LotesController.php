<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lote;
use App\Status_lote;

class LotesController extends Controller
{
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          $lotes = Lote::get();
          
          return view('lotes.index',compact('lotes'));
      }

      public function index_sold(){
        $lotes = Lote::where('status_code', '=', 7)->get();

        #var_dump($lotes);

        return view('lotes.sold', compact('lotes'));
      }

    /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */

    public function create()
    {
      $status = Status_lote::get();
      #var_dump($status);

      return view('lotes.create', compact('status'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   	public function store(Request $request)
   	{
       $lote = new Lote();
       $data = $this->validate($request, [
           'numero'=>'required',
           'mcuadrados' => 'required',
           'precio_unidad'=> 'required',
           'precio_final' => 'required',
           'manzana' => 'required',
           'status_code' => 'required'

       ]);
      
       $lote->saveLote($data);
       return redirect('/lotes')->with('success', 'Nuevo Lote ha sido creado.');
   }

     public function edit($id)
     {
         $lotes = Lote::where('id', $id)->first();
         $status = Status_lote::get();

         return view('lotes.edit', compact('lotes', 'id', 'status'));
     }
}

