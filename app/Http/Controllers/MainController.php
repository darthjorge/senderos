<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{
    //

    public function index()
    {
    	return view('frontend/index');
    }

    public function contacto()
    {
    	return view('frontend/contacto');
    }

    public function desarrollos()
    {
        return view('frontend/desarrollos');
    }   

    public function amenidades()
    {
    	return view('frontend/amenidades');
    }

    public function ciudadMayakoba()
    {
        return view('frontend/ciudad-mayakoba');
    }

    public function ubicacion()
    {
        return view('frontend/ubicacion');
    }

    public function blog()
    {
        return view('frontend/blog');
    }
    
    public function desarrollosDisponibilidad()
    {
        return view('frontend/desarrollos-disponibilidad');
    }

    public function galeria()
    {
        return view('frontend/galeria');
    }

    public function aviso()
    {
        return view('frontend/aviso-privacidad');
    }
    
    public function avisoCompleto()
    {
        return view('frontend/aviso-privacidad-completo');
    }
    
    public function avisoIngles()
    {
        return view('frontend/aviso-privacidad-ingles');
    }

    public function gracias()
    {
        return view('frontend/gracias');
    }
    
    public function send(Request $request)
    {
        
        $firstname = $request->get("firstname");
        $lastname = $request->get("lastname");
        $phone = $request->get("phone");
        $email = $request->get("email");
        $interesting = $request->get("interesting");
        $reason = $request->get("reason");
        $message = $request->get("message");
        
        
        $body = '
    		<p>Nombre: '.$firstname." ".$lastname.'</p>
    		<p>Teléfono: '.$phone.'</p>
    		<p>Email: '.$email.'</p>
    		<p>Interesado en:'.$interesting.'</p>
    		<p>Motivo: '.$reason.'</p>
    		<p>Mensaje: '.$message.'</p>';
    
    	$headers = "MIME-Version: 1.0\r\n";
    	$headers .= "Content-type: text/html; charset=utf8\r\n";
    	$headers .= "From: info@senderosciudadmayakoba.com \r\n";
    	$subject  = "Contacto";
    
    	//$to = "victoru@molcajetemkt.com";
    	$to = "leads@senderosciudadmayakoba.com, info@senderosciudadmayakoba.com, contacto@senderosciudadmayakoba.com, victor@molcajetemkt.com";


        
    	if($firstname !="" && $lastname !="" && $phone !="" && $email !="" && $interesting !="Interesados en" && $reason !="Motivo" && $message !="")
    	{
    		if(mail(utf8_decode($to),utf8_decode($subject),utf8_decode($body),utf8_decode($headers)))
    		{
    			 

                $bodyhtmlclient = '<!DOCTYPE html>
                <meta charset=UTF-8" lang="es"/>
                <head>
                <title>Senderos de Mayakoba</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                </head>
                <body style="margin: 0; padding: 0;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                        <tr>
                            <td style="padding: 10px 0 30px 0;">
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                                    <tr>
                                        <td align="center" bgcolor="#938268" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                                            <img src="http://senderosmayakoba.mx/images/general/logotipo-senderos-ciudad-mayakoba.png" alt="Senderos de Mayakoba" width="300" height="auto" style="display: block;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                                        <h1 style="text-align: center; color: #153643; font-family: Arial, sans-serif; font-size: 29px; line-height: 28px;">
                                                            Bienvenidos a Senderos Ciudad Mayakoba
                                                        </h1>
                                                        <p style="padding: 20px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px; text-align:justify">
                                                            En el corazón de la Riviera Maya aparece el innovador y confortable Residencial donde podrás edificar la casa de tus sueños, formando parte de Ciudad Mayakoba.
                                                        </p>
                                                        <p style="padding: 10px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px; text-align:justify">
                                                            Gracias por comunicarte con Senderos Ciudad Mayakoba, un representante se pondrá en contacto contigo para asistirte lo antes posible.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#938268" style="padding: 30px 30px 30px 30px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
                                                        Todos los Derechos Reservados &reg; Senderos Ciudad Mayakoba.<br/>
                                                    </td>
                                                    <td align="right" width="25%">
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </body>
                </html>';

                mail(utf8_decode($email),utf8_decode("CONTACTO"),utf8_decode($bodyhtmlclient),utf8_decode($headers));


                /* api property base */

                $weblisting_endpoint = "https://senderosmayakoba.secure.force.com/pba__WebserviceListingsQuery";
                $weblisting_token = "36d46742016dceda69f4bfe1cbbc6fc4";

                $web_to_prospect_endpoint = "https://senderosmayakoba.secure.force.com/services/apexrest/pba/webtoprospect/v1/";
                $web_to_prospect_token = "36d46742016dceda69f4bfe1cbbc6fc4";

                if (array_key_exists("WEBLISTINGS_ENDPOINT", $_ENV)){
                  $weblisting_endpoint = $_ENV["WEBLISTINGS_ENDPOINT"];
                }

                if (array_key_exists("WEBLISTINGS_TOKEN", $_ENV)){
                  $weblisting_token = $_ENV["WEBLISTINGS_TOKEN"];
                }

                if (array_key_exists("WEBTOPROSPECT_ENDPOINT", $_ENV)){
                  $web_to_prospect_endpoint = $_ENV["WEBTOPROSPECT_ENDPOINT"];
                }

                if (array_key_exists("WEBTOPROSPECT_TOKEN", $_ENV)){
                  $web_to_prospect_token = $_ENV["WEBTOPROSPECT_TOKEN"];
                }

                $dev_env = false;

                $api_lead_source   = "Web";


                $firstname = $request->get("firstname");
                $lastname = $request->get("lastname");
                $phone = $request->get("phone");
                $email = $request->get("email");
                $interesting = $request->get("interesting");
                $reason = $request->get("reason");
                $message = $request->get("message");
                $api_description = $request->get("message");

                /* $api_first_name    = $_POST["nombre"];
                $api_last_name     = $_POST["apellido"];
                $api_email         = $_POST["email"];
                $api_phone         = $_POST["telefono"];
                $api_affair        = $_POST["asunto"];
                $api_reason        = $_POST["motivo"];
                $api_comments      = $_POST["comentarios"];  */

                /*$api_description  = "
                Interesado en : $api_affair \n
                Motivo : $api_reason \n
                Comentarios: $api_comments"; */

                //$api_description = htmlspecialchars_decode($api_description);

                $query = array (
                  'prospect' => array(
                    'token' => $web_to_prospect_token,
                    'contact' => array (
                      'LeadSource' => $api_lead_source,
                      'FirstName' => $firstname,
                      'LastName' => $lastname,
                      'Email' => $email,
                      'Phone' => $phone,
                      'Description' => $api_description
                    )
                ));

                $query = json_encode($query); 

                $curl = curl_init($web_to_prospect_endpoint);
                // Accept any server (peer) certificate on dev envs
                if($dev_env) {
                  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                }
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(  "Content-type: application/json"));
                $info = curl_getinfo($curl);
                $response = curl_exec($curl);

                $jsonResponse =  json_decode($response);

                curl_close($curl);


                /* api property base */

    			return Redirect::to("gracias");
    			
    		}


    	}
    	
    	return Redirect::back();
    	
    }
    

}
