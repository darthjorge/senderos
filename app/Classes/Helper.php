<?php

namespace App\Classes;

use File;
use Session;
use DB;
use Config;
use Auth;
// Laravel Location Service
use LaravelLocalization;

// Llamamos modelos a usar
use App\Language;

class Helper {

	public static function convertToHoursMins($time, $format = '%d:%d') {
	    settype($time, 'integer');
	    if ($time < 1) {
	        return sprintf($format, 0, 0);
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public static function remove_accent($str) { 
  		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
  		
  		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
  		return str_replace($a, $b, $str); 
	} 

	public static function toWord($word){
		$word = str_replace('_', ' ', $word);
		$word = str_replace('-', ' ', $word);
		$word = ucwords($word);
		return $word;
	}

	public static function activityShow(){
		$side = ['left','right'];
		$index=array_rand($side);
		echo $side[$index];
	}

	public static function verifyCsrf($token){
		if($token == Session::token())
			return 1;
		else
			return 0;
	}

	public static function create_slug($string){
	   	$slug=preg_replace('/[^A-Za-z0-9-]+/', '_', strtolower($string));
	   	return $slug;
	}

	public static function create_slug_friendly($string) {
	   	$slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), Helper::remove_accent($string))); 
	   	return $slug;
	}

	public static function showDateTime($time = '',$niceDate = false)
	{
		if($time == '')
			return;

		if(config('config.date_format') == 'dd mm YYYY')
			$format = 'd-m-Y';
		elseif(config('config.date_format') == 'dd MM YYYY')
			$format = 'd-M-Y';
		elseif(config('config.date_format') == 'mm dd YYYY')
			$format = 'm-d-Y';
		elseif(config('config.date_format') == 'MM dd YYYY')
			$format = 'M-d-Y';

		if($niceDate == true)
		{
			// return date($format);
			return date('d M Y',strtotime($time));
		}

		if(config('config.time_format') == '24hrs')
			return date($format.',H:i',strtotime($time));
		else
			return date($format.',h:i a',strtotime($time));
	}

	public static function showTime($time = ''){
		if($time == '')
			return;
		if(config('config.time_format') == '24hrs')
			return date('H:i',strtotime($time));
		else
			return date('h:i a',strtotime($time));
	}

	public static function showDate($date = ''){
		if($date == '' || $date == null)
			return;
		return date('d M Y',strtotime($date));
	}
	
	public static function getLanguage() {

		// Identificamos el idioma actual
        $lang = LaravelLocalization::getCurrentLocale();

 

        // Buscamos si el idioma está dado de alta
        $language = Language::where('code', 'like', $lang)->first(['id', 'name', 'code']);

        // Verificamos que si existan registros correspondientes a ese idioma en la tabla
        if (is_null($language)) 
        {
            // No hubo registros de idioma. 
            // Página no encontrada
            return false;
        } else {
        	return $language;
        }
	}

	/**
	 * [getRoomType description]
	 * @return [array] [Arreglo con los tipos de habitaciones disponibles]
	 */
	public static function getRoomType() {
		$room_type = \AmbianceSuites\RoomType::where('status', 1)->get();
		return $room_type;
	}

	public static function getRooms() {
		$language = Helper::getLanguage();
		$rooms = DB::table('rooms')->
			leftJoin('room_description', 'rooms.idRoom', '=', 'room_description.idRoom')->
			where('rooms.active', '=', true)->
			where('room_description.idLanguage', '=', $language['idLanguage'])->
			get(['room_description.idRoom','rooms.idRoomType','room_description.title', 'room_description.short_text']);
		return $rooms;
	}

	public static function generate_url($variable) {
		$string = '';

		if(is_array($variable)) {
			foreach ($variable as $row) {
		 		$string .= '/' . Helper::create_slug_friendly($row);
			}
		}  else {
			$string = Helper::create_slug_friendly($variable);
		}
		
		$lang_url = LaravelLocalization::getLocalizedURL(null, $string);

		return $lang_url;
	}

	//obtenemos una coleccion de fechas a partir de un rango de fechas continuo
	static function getDateRange($start,$end,$type="")
	{
		
		$dates = array();

		$begin = new DateTime($start);
		$end = new DateTime($end);
		$end = $end->modify( '+1 day' ); 

		$interval = new \DateInterval('P1D');
		$daterange = new \DatePeriod($begin, $interval ,$end);

		foreach($daterange as $date)
		{
		    $dates[] = $date->format("Y/m/d");
		}

		array_pop ($dates);

		return $dates;

	}

	//funcion para convertir un numero entero o doble hasta dos decimales
	static function getDecimalValue($value,$type="")
	{

		$count = explode('.',$value);
		$valueLocal = null;

		if(isset($count[1]))
		{
			if(strlen($count[1]) == 2)
			{
				$valueLocal = $value;
				return array("value" => $valueLocal);
			}

			if(strlen($count[1]) > 2)
			{	
				//si el tipo de valor pasado es el tax dejar maximo 4 decimales
				if($type =="tax")
				{
					if( strlen($count[1]) == 3)
					{
						$valueLocal = number_format((float)$value, 3, '.', '');
					}

					if( strlen($count[1]) >= 4)
					{
						$valueLocal = number_format((float)$value, 4, '.', '');
					}
				}

				if($type =="paypal")
				{
					$valueLocal = number_format((float)$value, 2, '.', '');
				}

				if($type =="")
				{
					$valueLocal = number_format((float)$value, 2, '.', '');
				}

				return array("value" => $valueLocal);
			}

			if(strlen($count[1]) == 0)
			{
				$valueLocal = ($value.".00");
				return array("value" => $valueLocal);
			}

			if(strlen($count[1]) == 1)
			{
				$valueLocal = ($value."0");
				return array("value" => $valueLocal);
			}
						
		}

		else
		{
			$valueLocal = ($value.".00");
			return array("value" => $valueLocal);
		}

	}

	//funcion para obtener el precio de acuerdo a la divisa
	static function getExchangeRate($price, $currencyCode)
	{

		//si la divisa es diferente a mxn se hace la conversion
		if($currencyCode !="MXN")
		{
			$price = currency($price, $currencyCode, 'MXN', false);  
			$currencyCodeLocal = $currencyCode;

			return array("price" => $price,"currencyCode" => $currencyCodeLocal);
		}
		else
		{
			$price = currency($price, null, null, false);
			return array("price" => $price, "currencyCode" => "");
		}

	}

	static function getPrice($price, $currencyCode="")
	{

		if($currencyCode == "")
		{
			$currencyCode = currency()->getUserCurrency();
		}

		$price_formatted = currency($price, $currencyCode); 

		return $price_formatted;
		
	}

	// static function getPrice2($price, $currencyCode="")
	// {

	// 	if($currencyCode == "")
	// 	{
	// 		$currencyCode = currency()->getUserCurrency();
	// 	}

	// 	$price_formatted = currency($price, $currencyCode); 

	// 	return $price_formatted;
		
	// }

	//solo trae el precio sin nada extra
	static function getPriceUnformatted($price, $currencyCode="")
	{

		if($currencyCode == "" || $currencyCode == "MXN")
		{
			$currencyCode = currency()->getUserCurrency();
		}

		$price_unformatted = currency($price, $currencyCode, null, false); 

		return $price_unformatted;
	}

	//obtener las tarifas
	static function getRate()
	{

		$today = new Carbon("today");
		$today = date_format($today,"Y-m-d");

		//obtener tarifa
		$rrd = DB::table('room_rate_dates as rrd')
				->where('rrd.date',$today)
				->first();

		if(isset($rrd))
		{

			$roomPrice = DB::table('room_price as rp')
				->where('rp.idRate',$rrd->idRate)
				->limit(3)
				->get();

		}
		else
		{

			$roomPrice = DB::table('room_price as rp')
				->where('rp.idRate',1)
				->limit(3)
				->get();

		}

		$currencyCode = Session::get('code');
    	$sql = CurrencyModel::where('code',$currencyCode)->first();
		$currencyCodeDB = 'MXN';

		if(isset($roomPrice))
		{

			foreach($roomPrice as $key => $value)
			{
				
				if(isset($sql))
				{

					if($sql->code !="MXN")
					{

						$currencyCodeDB = $sql->code;

						$getExchangeRate = Helper::getDecimalValue($value->one_person);
						$one_person = $getExchangeRate["value"];

						$getValue = Helper::getExchangeRate($one_person,$currencyCodeDB);
						$finalPrice = $getValue["price"];
						$roomPrice[$key]->one_person = $finalPrice;

					}

				}

			}

		}//

		return $roomPrice;
		
	}

	//verificar que un archivo exista
	static function FileExist($pathFile)
	{

		//si el archivo existe
		if(file_exists($pathFile))
		{
			return 1;
		}
		else
		{
			return 0;
		}

	}

	//funcion para obtener el impuesto de la base de datos
	static function getTax()
	{

		//obtener valor del impuesto en la base de datos
		$sqlPanelConfig = PanelConfig::where('id',1)->first();

		if(isset($sqlPanelConfig))
		{
			//agregar validacion 0 hasta 99 para tax
			if($sqlPanelConfig->tax > 0 && $sqlPanelConfig->tax < 100)
			{

				//obtener el impuesto de la base de datos y formatearlo para que contenga decimal
				$getTaxDecimal = Self::getDecimalValue($sqlPanelConfig->tax,"tax");
				$taxDecimal = $getTaxDecimal["value"];

				//hacer el calculo del tax entre 100
		    	$taxDecimalCalculated = ($taxDecimal/100);

				return Array("valid" => true, "tax" => $taxDecimalCalculated);

			}
			else
			{
				return Array("valid" => false, "tax" => NULL);
			}
		}
		else
		{
			return Array("valid" => false, "tax" => NULL);
		}		
	}

	//funcion para aplicar una promocion a una reserva
	static function getPromotionData($subTotal,$reservationCurrencyCode,$reservationPromotion)
	{
		

		$newSubTotal = -1;
		$band = 0;

		//si hay alguna promocion relacionada a la reserva, aplicarle el descuento
		if(isset($reservationPromotion))
		{

			$type = $reservationPromotion->promo_type;

			$discount = $reservationPromotion->promo_discount;
			
			if($reservationCurrencyCode !="MXN")
			{
				$discount = Self::getPriceUnformatted($discount,$reservationCurrencyCode);
			}

			//si es un descuento valido por porcentaje
			if($discount > 0 && $discount <= 100 && $type == 1)
			{

				$newSubTotal =  $subTotal - ($subTotal * ($discount) / 100);

				if($newSubTotal >= 0)
				{
					if($newSubTotal == 0)
					{
						$newSubTotal = 0;
						$band = 1;
					}
					else
					{
						$newSubTotal = $newSubTotal;
						$band = 1;
					}
				}
			}

			//si es un descuento valido por cantidad
			if($type == 2 && $discount > 0)
			{	

				$newSubTotal = $subTotal - $discount;

				if($newSubTotal < 0)
				{
					$newSubTotal = 0;
				}

				$band = 1;
			}

			return Array("band" => $band, "newSubTotal" => $newSubTotal);

		}
		else
		{
			return Array("band" => $band, "newSubTotal" => $newSubTotal);
		}

	}

	static function getRackRate()
	{

		$start = '2017-01-01';
		$end = '2022-12-31';
		$name = "Rack";

		return Array("start" => $start,"end" => $end, "name" => $name);
		
	}

	public static function uploadProductImage($request_file, $path, $width = null, $height = null, $thumbnail = null)
	{

		// Watermark
		// $company = 'hands_of_mexico';
		// $watermark = config('constants.company.'.$company.'.logo');

		$extension = $request_file->getClientOriginalExtension();
		$filename = uniqid();

		//verificar que la imagen no exista
		$trozos = explode(".", $request_file);

		//ruta del archivo
		$filePath = config('constants.upload_path.'.$path);
	
		$verifiedNameImage = Self::checkExists($trozos,$filePath,$filename,true);
			
		if(!is_null($verifiedNameImage))
		{
			$filename = $verifiedNameImage;
		}

		if(!File::isDirectory($filePath))
		{
			File::makeDirectory($filePath,0777, true);
		}
		
		//guarda imagen del request
		$request_file->move(config('constants.upload_path.'.$path), $filename.".".$extension);
	    
	    $img = Image::make(config('constants.upload_path.'.$path).$filename.".".$extension);

	    // We resize image according to $width and $height
	    $img->resize($width, $height, function ($constraint) {
	        $constraint->aspectRatio();
	    });

	    // Full file name
	    $file = config('constants.upload_path.'.$path).$filename.".".$extension;

	    $thumbnailFolder = "thumbnails/";

		if(sizeof($thumbnail) > 0)
		{
	    	foreach ($thumbnail as $key => $thumb) 
	    	{
	    		$new_path = config('constants.upload_path.'.$path) . $thumbnailFolder. $thumb["width"] . "x" . $thumb["height"] . "/";

	    		if(!File::isDirectory($new_path))
	    		{
	    			File::makeDirectory($new_path,0777, true);
	    		}

	    		File::copy($file, $new_path.$filename.".".$extension);

	    		$img_thumbnail = Image::make($new_path . $filename.".".$extension);

	    		$img_thumbnail->fit($thumb["width"], $thumb["height"], function ($constraint) {
		            $constraint->aspectRatio();
		        });

	   //  		$image_watermark = Image::make($watermark);
	   //  		$image_watermark->resize($thumb["width"] * 0.7, null, function ($constraint) {
	   //  			$constraint->aspectRatio();
				// });

	   //  		$img_thumbnail->insert($image_watermark, 'center');
	    		


	    		$img_thumbnail->save($new_path.$filename.".".$extension);
	    	}
	    }

	    // $image_watermark = Image::make($watermark);

	    // // We resize watermark according to $height
	    // $image_watermark->resize($img->width() * 0.7, null, function ($constraint) {
	    //     $constraint->aspectRatio();
	    // });

	    // // Watermark inserted to base image
	    // $img->insert($image_watermark, 'center');

	    $response = $img->save(config('constants.upload_path.'.$path).$filename.".".$extension);

	    if($response == true)
	    {
			return $filename.".".$extension;
	    }
	    else 
	    {
	    	return false;
	    }
	}


	// Delete image
	public static function removeImage($filename, $path, $thumbnail = null)
	{

		$existFile = Self::FileExist(config('constants.upload_path.'.$path).$filename);
	
		if($existFile)
		{
			$response = File::delete(config('constants.upload_path.'.$path).$filename);

			$thumbnailFolder = "thumbnails/";

			if(sizeof($thumbnail) > 0)
			{
				foreach ($thumbnail as $key => $thumb)
				{
					$new_path = config('constants.upload_path.'.$path) . $thumbnailFolder. $thumb["width"] . "x" . $thumb["height"] . "/";
					
					$thumbnailExist = Self::FileExist($new_path.$filename);

					if($thumbnailExist)
					{
						File::delete($new_path.$filename);
					}

				}
			}

			if($response == true)
			{
				return null;
			}
			else
			{
				return $response;
			}	
		}
		

	}

	//funcion para verificar que el nombre del archivo no exista y si existe relation
    static function checkExists($file,$path,$idUnique = 0,$check= false)
    {
    	
        $i = 0;

        //nombre personalizado
        $stringFile = Self::getRandomString($idUnique);
         
        //mientras el archivo exista entramos y obtenemos un numero distinto
        while(file_exists($path.$stringFile.".".end($file)))
        {
            $i++;
            $stringFile = Self::getRandomString($idUnique);
        }

        //devolvemos el nuevo nombre de la imagen
        if($check)
        {
        	return $stringFile;
        }
        else
        {
        	return $stringFile.".".end($file);
        }
       

    }

    //funcion para generar string unico
    static function getRandomString($idUnique)
    {

    	if($idUnique !=0 && $idUnique > 0)
        {
        	$customString = "img_".$idUnique."_".time();
        }
        else
        {
        	$customString = "img_".time();
        }

        return $customString;

    }

    static function getLocale()
    {
    	if(Session::has('locale'))
    		\App::setLocale(Session::get('locale'));
    }

    //funcion para ajustar las cantidades que se heredan de otra tarifa
    static function getNumber($number,$extendsRateQuantity,$extendsRateOperator,$extendsRateType)
    {
    	return Self::operation($number,$extendsRateQuantity,$extendsRateOperator,$extendsRateType);
    }

    static function operation($number,$extendsRateQuantity,$extendsRateOperator,$extendsRateType)
    {	
    	
    	// $result = NULL;

    	// si es menos y porcentaje
    	if($extendsRateOperator == 2 && $extendsRateType == 2)
    	{
    		$result = $number - (($number * $extendsRateQuantity)/100);
    		// dd($result);
    	}

    	// si es menos y cantidad
    	if($extendsRateOperator == 2 && $extendsRateType == 1)
    	{
    		$result = $number - ($extendsRateQuantity);
    	}

    	// si es mas y cantidad
    	if($extendsRateOperator == 1 && $extendsRateType == 1)
    	{
    		$result = $number + ($extendsRateQuantity);
    	}

    	// si es mas y cantidad
    	if($extendsRateOperator == 1 && $extendsRateType == 2)
    	{
    		$result = $number + (($number * $extendsRateQuantity)/100);
    	}

    	if($result < 1)
    		$result = 0;

    	return $result;
    	
    }

}
 

?>