<?php

Route::get('/','MainController@index');

Route::get('contacto','MainController@contacto');
Route::get('amenidades','MainController@amenidades');
Route::get('ciudad-mayakoba','MainController@ciudadMayakoba');
Route::get('desarrollos','MainController@desarrollos');
Route::get('ubicacion','MainController@ubicacion');
//Route::get('blog','MainController@blog');
Route::get('desarrollos/disponibilidad','MainController@desarrollosDisponibilidad');
Route::get('galeria','MainController@galeria');
Route::get('aviso-privacidad','MainController@aviso');
Route::get('aviso-privacidad-completo','MainController@avisoCompleto');
Route::get('aviso-privacidad-ingles','MainController@avisoIngles');

Route::get('gracias','MainController@gracias');

Route::post('contacto','MainController@send');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

# lotes routes:
Route::get('/create/lotes','LotesController@create');
Route::post('/create/lotes','LotesController@store');
Route::get('/lotes', 'LotesController@index');
Route::get('/edit/lotes/{id}','LotesController@edit');
Route::post('/edit/lotes/{id}','LotesController@update');
Route::patch('/edit/lotes/{id}','LotesController@update');
Route::get('/sold/lotes', 'LotesController@index_sold');


# status routes:
Route::get('/create/status', 'StatusController@create');
Route::post('/create/status', 'StatusController@store');
Route::get('/status', 'StatusController@index');
Route::get('/edit/status/{id}','StatusController@edit');
Route::post('/edit/status/{id}','StatusController@update');
Route::patch('/edit/status/{id}','StatusController@update');

# cotizador routes:
Route::get('/cotizador', 'CotizadorController@index');



