
<?php

$bodyhtmlprop = '<!DOCTYPE html>
<meta charset=UTF-8" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Senderos de Mayakoba</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#938268" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="http://senderosmayakoba.mx/images/general/logotipo-senderos-ciudad-mayakoba.png" alt="Creating Email Magic" width="300" height="auto" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<h1 style="text-align: center; color: #153643; font-family: Arial, sans-serif; font-size: 29px; line-height: 20px;">
											Senderos de Mayakoba
										</h1>
										<p style="padding: 20px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
											<ul>
												<li>Nombre: '.$nombre.'</li>
												<li>Apellido: '.$apellido.'</li>
												<li>Tel&eacute;fono: '.$telefono.'</li>
												<li>E-mail: '.$correo.'</li>
												<li>Asunto: '.$asunto.'</li>
												<li>Motivo: '.$motivo.'</li>
												<li>Comentarios: '.$comentarios.'</li>
											</ul>
										</p>
										<!--<img width="100%" height="auto" src="http://senderosmayakoba.mx/web/image/ubicacion/banner.jpg" alt="" />-->

									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#938268" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										Todos los Derechos Reservados &reg; Senderos de Mayakoba.<br/>
									</td>
									<td align="right" width="25%">
										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>';

?>