<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->

<?php 
  if ( 'es' === $GLOBALS['q_config']['language'])
  {
    echo'
	<footer style="width: 100%; padding:20px 0;">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-xs-12 footer-tel" style="margin-top: 5px;">
                <span class="flaticon-cellphone67 white-text"></span>
                <span class="white-text maven size-13">+52 (984) 109 13 70</span>
            </div>
            <div class="col-sm-4 col-xs-12 footer-redes">
                <span class="white-text maven size-13">Síguenos en:</span>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="https://www.facebook.com/senderosmayakoba"><span class="white-color flaticon-socialnetwork83"></span></a>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="https://twitter.com/SenderosMk"><span class="white-color flaticon-logo22"></span></a>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="http://instagram.com/senderosmayakoba"><span class="white-color flaticon-instagram19"></span></a>
                <span class="white-text maven size-13" style="margin-left: 10px;"> /SenderosNorte</span>
            </div>
            <div class="col-sm-6 col-xs-12 col-sm-offset-0 footer-news">
                <form role="form " class="" id="news">
                    <div class="form-group row heightfooter">
                        <span class="white-text maven size-13 col-sm-5 col-xs-12" style="margin-top:7px;">Suscríbete a nustro Newsletter:</span>
                        <input style="width:auto; border-radius: 0; border:0px;height: 30px" type="email" class="form-control col-sm-5 col-xs-8 heightfooter" id="email" placeholder="">
                        <button type="submit" class="btn btn-default btn-footer col-sm-2 col-xs-3 noto-sans heightfooter padding012">ENVIAR</button>
                    </div>
                          
                </form>
            </div>
        </div>
    </div>
    <hr class="hr-footer" style="margin-top:10px;">
    <div class="container">
        <div class="col-sm-12 col-xs-12 text-center">
            <p class="text-center white-text maven size-13" style="margin-bottom:0">Todos los Derechos Reservados ® Senderos Mayakoba</p>
        </div>
    </div>
  </footer>';
}
if ( 'en' === $GLOBALS['q_config']['language'])
  {
   echo'   <footer style="width: 100%; padding:20px 0;">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-xs-12 footer-tel" style="margin-top: 5px;">
                <span class="flaticon-cellphone67 white-text"></span>
                <span class="white-text maven size-13">+52 (984) 109 13 70</span>
            </div>
            <div class="col-sm-4 col-xs-12 footer-redes">
                <span class="white-text maven size-13">Follow us:</span>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="https://www.facebook.com/senderosmayakoba"><span class="white-color flaticon-socialnetwork83"></span></a>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="https://twitter.com/SenderosMk"><span class="white-color flaticon-logo22"></span></a>
                <a class="white-text no-decoration hover-blue" style="vertical-align: -webkit-baseline-middle;" href="http://instagram.com/senderosmayakoba"><span class="white-color flaticon-instagram19"></span></a>
                <span class="white-text maven size-13" style="margin-left: 10px;"> /SenderosNorte</span>
            </div>
            <div class="col-sm-6 col-xs-12 col-sm-offset-0 footer-news">
                <form role="form " class="" id="news">
                    <div class="form-group row heightfooter">
                        <span class="white-text maven size-13 col-sm-5 col-xs-12" style="margin-top:7px;">Suscribe to our Newsletter:</span>
                        <input style="width:auto; border-radius: 0; border:0px;height: 30px" type="email" class="form-control col-sm-5 col-xs-8 heightfooter" id="email" placeholder="">
                        <button type="submit" class="btn btn-default btn-footer col-sm-2 col-xs-3 noto-sans heightfooter padding012">SEND</button>
                    </div>
                          
                </form>
            </div>
        </div>
    </div>
    <hr class="hr-footer" style="margin-top:10px;">
    <div class="container">
        <div class="col-sm-12 col-xs-12 text-center">
            <p class="text-center white-text maven size-13" style="margin-bottom:0">All rights reserved ® Senderos Mayakoba</p>
        </div>
    </div>
  </footer>';
   } 
?>
<?php 
  if ( 'en' === $GLOBALS['q_config']['language'])
  {
    echo'<div id="contacto-popup" style="display:none" class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h3 class="maven bold size-25 white-text" style="margin-bottom: 20px"> CONTACT</h3>
            <form class="contacto-form" id="form">
              <div class="form-group">
                <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail1" name="nombre" placeholder="Name">
              </div>
              <div class="form-group">
                <input type="email" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail2" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail3" name="telefono" placeholder="Phone">
              </div>
              <div class="form-group">
                <label class="col-md-12 roboto bold size-20 white-text" for="form-asunto" style="margin-top:-5px; margin-left: 10px;">Interested in:</label>
                <div class="col-md-10" style="margin-bottom:20px">
                  <input type="checkbox" name="asunto" value="Lotes Residenciales" checked="" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Residential Lots</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Brokers" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Brokers</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Financiamiento"><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Financing</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Otros" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Others</span>

                </div>
              </div>
              <div class="form-group">
                <textarea placeholder="Mensaje" name="mensaje" class="form-control without-borders col-sm-12 col-xs-12 margin-bottom-20" id="form-mensaje" rows="3" style="padding-top: 5px; resize: none;"></textarea>
              </div>
              <div class="form-group">
                <div class="col-md-12" style="padding: 0;">
                  <button type="submit" class="btn btn-default size-22 pull-right form-contacto-enviar" style="font-size: 22px;">SEND</button>
                  <div id="response" class="roboto bold size-20 white-text"></div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-6 contacto-datos" style="">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h4 class="maven bold size-24 white-text" style="margin-bottom: 15px">Contact Us</h4>
            <p class="roboto bold size-20 white-text" style="margin-bottom: 0">Address</p>
            <p class="roboto  size-18 white-text "><a class="no-decoration"  href="https://www.google.com.mx/maps/place/Senderos+de+Mayakoba/@20.857312,-87.2346321,10z/data=!4m5!1m2!2m1!1sBoulevard+Playa+del+Carmen,+km.+299.!3m1!1s0x8f4e42cd9d6da4a1:0x597bf59f65985891" target="_blank">Boulevard Playa del Carmen, km. 299.</a></p>
            <p class="roboto  size-18 white-text ">Phone: +52 (984) 109 13 70</p><br>
            <p class="roboto  size-18 white-text ">Cancún:</p>
            <p class="roboto  size-18 white-text "><a class="no-decoration" href="https://www.google.com.mx/maps/dir//Calle+Palma+Real,+Canc%C3%BAn,+Q.R./@21.1119728,-86.887284,13z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x8f4c2a52a9f1ee8b:0xe7ca2c8d95f08275!2m2!1d-86.8522643!2d21.1119764" target="_blank">Av. Huayacan,Plaza Palmaris Local 4 C.P. 77560</a></p>
            <p class="roboto  size-18 white-text "style="margin-bottom: 0">Teléfono: +52 (988) 884 41 45</p>
            <br>
            <p class="roboto bold size-20 white-text">Teléfonos:</p>
            <p class="roboto  size-18 white-text " style="margin-bottom: 20px">
            800 National: 01-800-890 5858<br>
            800 USA: 1-844-256 5828<br>
            800 CANADA:  1-844-422 4926</p>
            <br>
            <p class="roboto  size-18 white-text" style="margin-bottom: 15px"><span class="flaticon-black218 icono-sobre"></span>  info@senderosmayakoba.mx</p>
            <ul>
            <li class="redes-contacto-form">
                          <a class="padding-0 no-decoration  hover-blue"href="https://www.facebook.com/SenderosNorte" target="_blank"><span class="flaticon-socialnetwork83 color0"></span></a> 
                          <a class="padding-0 no-decoration  hover-blue" href="https://www.instagram.com/senderosnorte" target="_blank"><span class="flaticon-instagram19 color0"></span></a>
                          <a class="padding-0 no-decoration  hover-blue" href="https://www.twitter.com/senderosnorte" target="_blank"><span class="flaticon-logo22 color0"></span></a>
                          <span class="roboto  size-18 white-text" style="margin-top: 10px;"> /SenderosNorte</span>
                      </li>
                      </ul>
          </div>
        </div>
      </div>
    </div>
  </div>';
  }
  if ( 'es' === $GLOBALS['q_config']['language'])
  {
    echo'<div id="contacto-popup" style="display:none" class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h3 class="maven bold size-25 white-text" style="margin-bottom: 20px"> CONTACTO</h3>
            <form class="contacto-form" id="form">
              <div class="form-group">
                <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail1" name="nombre" placeholder="Nombre">
              </div>
              <div class="form-group">
                <input type="email" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail2" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail3" name="telefono" placeholder="Teléfono">
              </div>
              <div class="form-group">
                <label class="col-md-12 roboto bold size-20 white-text" for="form-asunto" style="margin-top:-5px; margin-left: 10px;">Interesado en:</label>
                <div class="col-md-10" style="margin-bottom:20px">
                  <input type="checkbox" name="asunto" value="Lotes Residenciales" checked="" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Lotes Residenciales</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Brokers" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Brokers</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Financiamiento"><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Financiamiento</span>
                  <br>
                  <input type="checkbox" name="asunto" value="Otros" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Otros</span>

                </div>
              </div>
              <div class="form-group">
                <textarea placeholder="Mensaje" name="mensaje" class="form-control without-borders col-sm-12 col-xs-12 margin-bottom-20" id="form-mensaje" rows="3" style="padding-top: 5px; resize: none;"></textarea>
              </div>
              <div class="form-group">
                <div class="col-md-12" style="padding: 0;">
                  <button type="submit" class="btn btn-default size-22 pull-right form-contacto-enviar" style="font-size: 22px;">ENVIAR</button>
                  <div id="response" class="roboto bold size-20 white-text"></div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-6 contacto-datos" style="">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h4 class="maven bold size-24 white-text" style="margin-bottom: 15px">Datos de contacto</h4>
            <p class="roboto bold size-20 white-text" style="margin-bottom: 0">Dirección</p>
            <p class="roboto  size-18 white-text "><a class="no-decoration"  href="https://www.google.com.mx/maps/place/Senderos+de+Mayakoba/@20.857312,-87.2346321,10z/data=!4m5!1m2!2m1!1sBoulevard+Playa+del+Carmen,+km.+299.!3m1!1s0x8f4e42cd9d6da4a1:0x597bf59f65985891" target="_blank">Boulevard Playa del Carmen, km. 299.</a></p>
            <p class="roboto  size-18 white-text ">Teléfono: +52 (984) 109 13 70</p><br>
            <p class="roboto  size-18 white-text ">Cancún:</p>
            <p class="roboto  size-18 white-text "><a class="no-decoration" href="https://www.google.com.mx/maps/dir//Calle+Palma+Real,+Canc%C3%BAn,+Q.R./@21.1119728,-86.887284,13z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x8f4c2a52a9f1ee8b:0xe7ca2c8d95f08275!2m2!1d-86.8522643!2d21.1119764" target="_blank">Av. Huayacan,Plaza Palmaris Local 4 C.P. 77560</a></p>
            <p class="roboto  size-18 white-text "style="margin-bottom: 0">Teléfono: +52 (988) 884 41 45</p>
            <br>
            <p class="roboto bold size-20 white-text">Teléfonos:</p>
            <p class="roboto  size-18 white-text " style="margin-bottom: 20px">
            800 Nacional: 01-800-890 5858<br>
            800 USA: 1-844-256 5828<br>
            800 CANADA:  1-844-422 4926</p>
            <br>
            <p class="roboto  size-18 white-text" style="margin-bottom: 15px"><span class="flaticon-black218 icono-sobre"></span>  info@senderosmayakoba.mx</p>
            <ul>
            <li class="redes-contacto-form">
                          <a class="padding-0 no-decoration  hover-blue" href="https://www.facebook.com/SenderosNorte" target="_blank"><span class="flaticon-socialnetwork83 color0" ></span></a> 
                          <a class="padding-0 no-decoration  hover-blue" href="https://www.instagram.com/senderosnorte" target="_blank"><span class="flaticon-instagram19 color0"></span></a>
                          <a class="padding-0 no-decoration  hover-blue" href="https://www.twitter.com/senderosnorte" target="_blank"><span class="flaticon-logo22 color0"></span></a>
                          <span class="roboto  size-18 white-text" style="margin-top: 10px;"> /SenderosNorte</span>
                      </li>
                      </ul>
          </div>
        </div>
      </div>
    </div>
  </div>';
  }
?>
<div id="contacto-popup" style="display:none" class="container">
  <div class="row">
    <div class="col-sm-6">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h3 class="maven bold size-25 white-text" style="margin-bottom: 20px"> CONTACTO</h3>
          <form class="contacto-form" id="form">
            <div class="form-group">
              <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail1" name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group">
              <input type="email" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail2" name="email" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" required class="form-control col-sm-12 col-xs-12 margin-bottom-20" id="exampleInputEmail3" name="telefono" placeholder="Teléfono">
            </div>
            <div class="form-group">
              <label class="col-md-12 roboto bold size-20 white-text" for="form-asunto" style="margin-top:-5px; margin-left: 10px;">Interesado en:</label>
              <div class="col-md-10" style="margin-bottom:20px">
                <input type="checkbox" name="asunto" value="Lotes Residenciales" checked="" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Lotes Residenciales</span>
                <br>
                <input type="checkbox" name="asunto" value="Brokers" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Brokers</span>
                <br>
                <input type="checkbox" name="asunto" value="Financiamiento"><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Financiamiento</span>
                <br>
                <input type="checkbox" name="asunto" value="Otros" ><span class="roboto  size-18 white-text" style="vertical-align: middle;"> Otros</span>

              </div>
            </div>
            <div class="form-group">
              <textarea placeholder="Mensaje" name="mensaje" class="form-control without-borders col-sm-12 col-xs-12 margin-bottom-20" id="form-mensaje" rows="3" style="padding-top: 5px; resize: none;"></textarea>
            </div>
            <div class="form-group">
              <div class="col-md-12" style="padding: 0;">
                <button type="submit" class="btn btn-default size-22 pull-right form-contacto-enviar" style="font-size: 22px;">ENVIAR</button>
                <div id="response" class="roboto bold size-20 white-text"></div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-sm-6 contacto-datos" style="">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h4 class="maven bold size-24 white-text" style="margin-bottom: 15px">Datos de contacto</h4>
          <p class="roboto bold size-20 white-text" style="margin-bottom: 0">Dirección</p>
          <p class="roboto  size-18 white-text "><a class="no-decoration"  href="https://www.google.com.mx/maps/place/Senderos+de+Mayakoba/@20.857312,-87.2346321,10z/data=!4m5!1m2!2m1!1sBoulevard+Playa+del+Carmen,+km.+299.!3m1!1s0x8f4e42cd9d6da4a1:0x597bf59f65985891" target="_blank">Boulevard Playa del Carmen, km. 299.</a></p>
          <p class="roboto  size-18 white-text ">Teléfono: +52 (984) 109 13 70</p><br>
          <p class="roboto  size-18 white-text ">Cancún:</p>
          <p class="roboto  size-18 white-text "><a class="no-decoration" href="https://www.google.com.mx/maps/dir//Calle+Palma+Real,+Canc%C3%BAn,+Q.R./@21.1119728,-86.887284,13z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x8f4c2a52a9f1ee8b:0xe7ca2c8d95f08275!2m2!1d-86.8522643!2d21.1119764" target="_blank">Av. Huayacan,Plaza Palmaris Local 4 C.P. 77560</a></p>
          <p class="roboto  size-18 white-text "style="margin-bottom: 0">Teléfono: +52 (988) 884 41 45</p>
          <br>
          <p class="roboto bold size-20 white-text">Teléfonos:</p>
          <p class="roboto  size-18 white-text " style="margin-bottom: 20px">
          800 Nacional: 01-800-890 5858<br>
          800 USA: 1-844-256 5828<br>
          800 CANADA:  1-844-422 4926</p>
          <br>
          <p class="roboto  size-18 white-text" style="margin-bottom: 15px"><span class="flaticon-black218 icono-sobre"></span>  info@senderosmayakoba.mx</p>
          <ul>
          <li class="redes-contacto-form">
                        <a class="padding-0 no-decoration  hover-blue" href="https://www.facebook.com/SenderosNorte" target="_blank"><span class="flaticon-socialnetwork83 color0"></span></a> 
                        <a class="padding-0 no-decoration  hover-blue" href="https://www.instagram.com/senderosnorte" target="_blank"><span class="flaticon-instagram19 color0"></span></a>
                        <a class="padding-0 no-decoration  hover-blue" href="https://www.twitter.com/senderosnorte" target="_blank"><span class="flaticon-logo22 color0"></span></a>
                        <span class="roboto  size-18 white-text" style="margin-top: 10px;"> /SenderosMayakoba</span>
                    </li>
                    </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="contacto-popup-send" style="display:none" class="col-sm-6 col-sm-offsert-3">
  <img src="http://placehold.it/600X300">
</div>

      <script src="../bower_components/jquery/dist/jquery.min.js"></script>
      <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

      <script src="../static/js/menu.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" />
      <!--Fullpage-->
      <!--<link rel="stylesheet" type="text/css" href="../static/js/jquery.fullPage.css" />-->
      <script src="../static/js/jquery.easings.min.js"></script>
      <script type="text/javascript" src="../static/js/query.slimscroll.min.js"></script>
      <!--<script type="text/javascript" src="../static/js/jquery.fullPage.js"></script>-->
      <script>
        var $ =jQuery.noConflict();
        $(document).ready(function(){
          $('#menu-noticias').addClass('background2');
        });
      </script>
      <script type="text/javascript">
          $(document).ready(function() {
              $(".contacto-popup").fancybox({
                
                onStart: function(){
                  $(".close-menu").trigger("click");
                  alert("estart")
                },
                beforeShow : function(){
             $(".fancybox-skin").addClass("custom-contact-popup");
             $(".close-menu").trigger("click");
            }
              });
          });
      </script>

      <script>
  var $ =jQuery.noConflict();
    $(document).ready(function(){
        $(".esp-change").click(function(e){
            e.preventDefault();
            //alert($(this).attr('rel'));
            window.location = $(this).attr('rel');
        });
        $(".eng-change").click(function(e){
            e.preventDefault();
            //alert($(this).attr('rel'));
            window.location = $(this).attr('rel');
        });
    });
</script>

    <!--<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA_TbxX1m1biZuaHgiZKmrrUPpvA046twA&sensor=false"></script>
    <script type="text/javascript" src="web/js/mapa.js"></script>-->
  
	<?php wp_footer(); ?>
</body>
</html>