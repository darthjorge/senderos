<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<style>
	.text-blog img{
		width: 100%;
		height: auto;
		padding-bottom: 30px;
	}

	.single-content-text{
		padding: 0px;
	}
    .single-content-text a{
        color: #5ca5c0;
    }
    .single-content-text a:hover{
        color: #31a393;
    }
</style>
<div class="container-fluid ">
    <div class="row textura-madera"> 
        <div class="container" style="position:relative">
            <div class="row">
                <div class="col-sm-12" style=" padding:15px 30px;">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6 logo">
                            <img class="img-responsive" src="../static/images/general/logotipo-sendero-norte(2x).png">
                        </div>
                        <div class="col-sm-4 col-xs-6 menu pull-right">
                            <a class="cd-primary-nav-trigger no-decoration" href="#">
                                <span class="white-text flaticon-configuration21" style="margin-right: 8px"></span>
                                <span class="cd-menu-text maven bold size-20 white-text"> MENÚ</span>
                            </a> <!-- cd-primary-nav-trigger -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
                if ( 'es' === $GLOBALS['q_config']['language'])
                    {
                        echo'
                            <nav>
                                <ul class="cd-primary-nav">

                                        
                                        <a class="close-menu no-decoration" href="#"><span class="white-text flaticon-cross108"></span></a>
                                        <p class="lang-menu" style="">
                                            <a class=" no-decoration esp-change" href="#" rel="http://senderosmayakoba.mx/senderos-norte/blog/?lang=es"><span class="maven bold size-20 white-text   menu-item" style="padding: 2px 5px">ESP</span></a>
                                            <span class="maven bold size-20 white-text">/</span>
                                            <a class=" no-decoration eng-change" href="#" rel="http://senderosmayakoba.mx/senderos-norte/blog/?lang=en"><span class="maven bold size-20 white-text  menu-item" style="padding: 2px 5px">ENG</span></a>
                                        </p>
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/" class="maven bold size-20 white-text menu-item" id="item-menu-home">INICIO</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/master-plan" class="maven bold size-20 white-text menu-item" id="item-menu-master-plan">MASTER PLAN</a>
                                        </li>
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/amenidades" class="maven bold size-20 white-text menu-item" id="item-menu-amenidades">AMENIDADES</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/ubicacion" class="maven bold size-20 white-text menu-item" id="item-menu-ubicacion">UBICACIÓN</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/disponibilidad" class="maven bold size-20 white-text menu-item" id="item-menu-disponibilidad">DISPONIBILIDAD</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/financiamiento" class="maven bold size-20 white-text menu-item" id="item-menu-financiamiento">FINANCIAMIENTO</a>
                                        </li>
                                        
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a class="maven bold size-20 white-text menu-item dropdown-toggle" id="item-menu-galeria" data-toggle="dropdown" href="#">GALERÍA</a>
                                            <ul class="dropdown-menu">
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/renders" class="maven bold size-20 white-text menu-item" id="item-menu-renders">Imágenes</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/avance" class="maven bold size-20 white-text menu-item" id="item-menu-avance">Avance de Obra</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/eventos" class="maven bold size-20 white-text menu-item" id="item-menu-eventos">Eventos</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/videos" class="maven bold size-20 white-text menu-item" id="item-menu-videos">Videos</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/blog" class="maven bold size-20 white-text menu-item" id="item-menu-noticias">NOTICIAS</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="#contacto-popup" class="maven bold size-20 white-text menu-item contacto-popup">CONTACTO</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php" class="maven bold size-20 white-text menu-item contacto-popup">SENDEROS DE MAYAKOBA</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <hr class="linea-menu ">
                                        </li>

                                        <li class="maven bold size-20 white-text col-sm-4 col-sm-offset-4 col-xs-12 margin-bottom-20">Suscríbete a nuestro Newsletter</li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <form role="form">
                                                <div class="form-group r">
                                                    <input type="email" class="noto-sans color8 italic form-control input-newsletter col-sm-9 col-xs-9" id="email" placeholder="Email">
                                                    <button  type="submit" class="btn btn-default col-sm-3 col-xs-3 btn-send">ENVIAR</button>
                                                </div>
                                            </form>
                                        </li>
                                        <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <li class="redes-menu">
                                                <a class="white-text no-decoration facebook-icon padding-0 hover-blue"href="https://www.facebook.com/SenderosNorte" targert="_blank"><span class="flaticon-socialnetwork83 color0"></span></a> 
                                                <a class="white-text no-decoration instagram-icon padding-0 hover-blue" href="https://www.instagram.com/senderosnorte" targert="_blank"><span class="flaticon-instagram19 color0"></span></a>
                                                <a class="white-text no-decoration tweet-icon padding-0 hover-blue" href="https://www.twitter.com/senderosnorte" targert="_blank"><span class="flaticon-logo22 color0"></span></a>
                                                <span class="maven bold size-20 white-text redes-texto-menu" style="margin-top: 10px;float: right;"> /SenderosNorte</span>
                                            </li>
                                        </div>
                                </ul>                   
                            </nav>';
                    }
                if ( 'en' === $GLOBALS['q_config']['language'])
                    {
                        echo'
                            <nav>
                                <ul class="cd-primary-nav">

                                        
                                        <a class="close-menu no-decoration" href="#"><span class="white-text flaticon-cross108"></span></a>
                                        <p class="lang-menu" style="">
                                            <a class=" no-decoration esp-change" href="#" rel="http://senderosmayakoba.mx/senderos-norte/blog/?lang=es"><span class="maven bold size-20 white-text   menu-item" style="padding: 2px 5px">ESP</span></a>
                                            <span class="maven bold size-20 white-text">/</span>
                                            <a class=" no-decoration eng-change" href="#" rel="http://senderosmayakoba.mx/senderos-norte/blog/?lang=en"><span class="maven bold size-20 white-text  menu-item" style="padding: 2px 5px">ENG</span></a>
                                        </p>
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-home">HOME</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/master-plan?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-master-plan">MASTER PLAN</a>
                                        </li>
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/amenidades?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-amenidades">AMENITIES</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/ubicacion?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-ubicacion">LOCATION</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/disponibilidad?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-disponibilidad">DISPONIBILITY</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php/financiamiento?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-financiamiento">FINANCING</a>
                                        </li>
                                        
                                        
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a class="maven bold size-20 white-text menu-item dropdown-toggle" id="item-menu-galeria" data-toggle="dropdown" href="#">GALERY</a>
                                            <ul class="dropdown-menu">
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/renders?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-renders">Images</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/avance?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-avance">Building in progress</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/eventos?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-eventos">Events</a></li>
                                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-norte/index.php/videos?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-videos">Videos</a></li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-norte/blog?lang=en" class="maven bold size-20 white-text menu-item" id="item-menu-noticias">NEWS</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="#contacto-popup" class="maven bold size-20 white-text menu-item contacto-popup">CONTACT</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php?lang=en" class="maven bold size-20 white-text menu-item contacto-popup">SENDEROS DE MAYAKOBA</a>
                                        </li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <hr class="linea-menu ">
                                        </li>

                                        <li class="maven bold size-20 white-text col-sm-4 col-sm-offset-4 col-xs-12 margin-bottom-20">Subscribe to our Newsletter</li>
                                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <form role="form">
                                                <div class="form-group r">
                                                    <input type="email" class="noto-sans color8 italic form-control input-newsletter col-sm-9 col-xs-9" id="email" placeholder="Email">
                                                    <button  type="submit" class="btn btn-default col-sm-3 col-xs-3 btn-send">SEND</button>
                                                </div>
                                            </form>
                                        </li>
                                        <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                                            <li class="redes-menu">
                                                <a class="white-text no-decoration facebook-icon padding-0 hover-blue" href="https://www.facebook.com/SenderosNorte" targert="_blank"><span class="flaticon-socialnetwork83 color0"></span></a> 
                                                <a class="white-text no-decoration instagram-icon padding-0 hover-blue" href="https://www.instagram.com/senderosnorte" targert="_blank"><span class="flaticon-instagram19 color0"></span></a>
                                                <a class="white-text no-decoration tweet-icon padding-0 hover-blue" href="https://www.twitter.com/senderosnorte" targert="_blank"><span class="flaticon-logo22 color0"></span></a>
                                                <span class="maven bold size-20 white-text redes-texto-menu" style="margin-top: 10px;float: right;"> /SenderosNorte</span>
                                            </li>
                                        </div>
                                </ul>                   
                            </nav>';
                    }
            ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class = "single-post-noticia crema-bg">
	<div class = "container">
		<div class="row">
			<section class="col-md-10 col-md-offset-1">
				<div class = "row">
                    <div class="col-xs-12 text-right">
                        <?php
                        if ( 'es' === $GLOBALS['q_config']['language'])
                            {
                            echo'<p class="text-right" style="margin-top:50px"><a class="no-decoration" href="javascript:history.back()"><span class="volver-atras-blog maven bold white-text size-19 flaticon-arrows"> VOLVER ATRÁS</span></a></p>';
                            }
                        if ( 'en' === $GLOBALS['q_config']['language'])
                            {
                            echo'<p class="text-right" style="margin-top:50px"><a class="no-decoration" href="javascript:history.back()"><span class="volver-atras-blog maven bold white-text size-19 flaticon-arrows"> BACK</span></a></p>';
                            }
                        ?>
                    </div>
					<div class = "col-xs-12 text-center"> 
						<h1 class = "text-center maven medium size-50 strong-gray"><?php  the_title(); ?></h1>
					</div>
                    <div class="col-sm-2  col-sm-offset-5">
                        <hr class="hr-parallax">
                    </div>
				</div>
				<div class = "row">
					<div class = "col-md-12 text-center img-single-content">
						<?php 
							/*if ( has_post_thumbnail() ) { 
								$image_id = get_post_thumbnail_id();
								$image_url = wp_get_attachment_image_src($image_id,'large', true);
								echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="'. $image_url[1] .'">';
							} */
						?>
					</div>
				</div>
				<div class = "row single-content-text">
					<div class = "col-md-12 text-blog">
						<?php
							the_content();
						?>
					</div>
				</div>
				<div class="row">
                    <div class="col-sm-12">
                        <hr class="hr-parallax">
                    </div> 
                    <div class="col-sm-4">
                    <p class="roboto gray-text size-17"><b><?php if ( 'es' === $GLOBALS['q_config']['language'])
                    {echo"Fecha de publicación:";}if ( 'en' === $GLOBALS['q_config']['language'])
                    {echo"Date:";}?></b> <?php print_r(the_date(' F jS, Y')); ?></p>
                        
                    </div>
                    <!--<div class="col-sm-2">
                        <a href="javascript:history.back()"> <p class="roboto gray-text size-17 no-decoration bold">Volver Atrás</p></a>
                    </div>-->
                    <div class="col-sm-3 pull-right">
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:title="Departamentos Pitahaya I y Pitahaya II" addthis:description="Departamentos en condominio en la zona sur de Cancún, localizado en el fraccionamiento Residencial Palmaris, rodeados de áreas verdes y con seguridad privada las 24 hrs." addthis:url="http://departamentospitahaya.com">
                            <a class="addthis_button_preferred_1"></a>
                            <a class="addthis_button_preferred_2"></a>
                            <a class="addthis_button_preferred_3"></a>
                            <a class="addthis_button_preferred_4"></a>
                            <a class="addthis_button_compact"></a>
                        <!--<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>-->
                        </div>
                    </div>           
                </div>  
			</section>
		</div>
	</div>
</div>
<?php endwhile; ?>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5661cab4a36796dc" async="async"></script>
<?php get_footer(); ?>