<?php
session_start(); 
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1); 
require 'vendor/autoload.php'; 
Twig_Autoloader::register();  
$app = new \Slim\Slim();  
$loader = new Twig_Loader_Filesystem('templates');  
$twig = new Twig_Environment($loader, array(  /*'cache' => 'cache',*/ ));  
//BASE URL
define('BASE_URL', 'http://www.senderosmayakoba.mx/');  //Sobreescribir por la ruta de su proyecto.

$languages = array('es','en');
if(isset($_GET['lang'])){
    if(in_array($_GET['lang'], $languages)) {
        $_SESSION['lang'] = $_GET['lang'];
    }
}
if(isset($_SESSION['lang'])){
    define('LANG', in_array($_SESSION['lang'], $languages) ? $_SESSION['lang'] : 'es');
}
else{
    define('LANG','es');
}
$lang_redirect="http://".$_SERVER['HTTP_HOST'].$app->request()->getPath();

$data=array(
	'BASE_URL' => constant('BASE_URL'),
    'REDIRECT_URL' => $lang_redirect,
    'LANG' => LANG,
);

include 'content.php';

$app->get('/', function() use ($twig,$data,$general,$home,$plan,$seo) { 	
	$data["my_title"] = "home";
	$data["general"] = $general;
	$data["home"] = $home;
	$data["plan"] = $plan;
	$data["seo"] = $seo;
	
	// print_r($data["plan"]);
    echo $twig->render('home.html',$data);  
});

$app->get('/senderos-mayakoba', function() use ($twig,$data,$general,$senderos_mayakoba,$seo){  
   	$data["my_title"] = "senderos-mayakoba";
 	$data["general"] = $general;
   	$data["senderos_mayakoba"]= $senderos_mayakoba;
   	$data["seo"] = $seo;
    $data["plan"] = array(
		'1' => array('url' =>'http://www.senderosmayakoba.mx/images/home/amenidades-senderos.png', 'des' =>'Descripcion de la imagen 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/home/amenidades-cenotes.png', 'des' =>'Descripcion de la imagen 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/home/amenidades-parque.png', 'des' =>'Descripcion de la imagen 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/home/amenidades-casa-club.png', 'des' =>'Descripcion de la imagen 4' ),
		);
	$data["images"] = array(
		'1' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/avance-obra-senderos-mayakoba(1).jpg', 'des' =>'Descripcion de la imagen 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/avance-obra-senderos-mayakoba(2).jpg', 'des' =>'Descripcion de la imagen 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/avance-obra-senderos-mayakoba(3).jpg', 'des' =>'Descripcion de la imagen 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/avance-obra-senderos-mayakoba(4).jpg', 'des' =>'Descripcion de la imagen 4' ),
		);		
    echo $twig->render('senderos-mayakoba.html',$data);  
});

$app->get('/senderos-norte', function() use ($twig,$data,$general,$senderos_norte,$seo){  
    $data["my_title"] = "about";
    $data["general"] = $general;
   	$data["senderos_norte"]= $senderos_norte;
   	$data["seo"] = $seo;
    $data["plan"] = array(
		'1' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-tamano-lotes.png', 'des' =>'Senderos Norte descripcion 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-hectareas-ciclopista.png', 'des' =>'Senderos Norte descripcion 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-parque-pet-friendly.png', 'des' =>'Senderos Norte descripcion 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-senderos-cenotes.png', 'des' =>'Senderos Norte descripcion 4' ),
		);
		$data["images"] = array(
		'1' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/norte-avance-obra-senderos-mayakoba(1).jpg', 'des' =>'Descripcion de la imagen 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/norte-avance-obra-senderos-mayakoba(2).jpg', 'des' =>'Descripcion de la imagen 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/norte-avance-obra-senderos-mayakoba(3).jpg', 'des' =>'Descripcion de la imagen 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/avancedeobra/full/norte-avance-obra-senderos-mayakoba(4).jpg', 'des' =>'Descripcion de la imagen 4' ),
		);		
    echo $twig->render('senderos-norte.html',$data);  
});

$app->get('/galeria', function() use ($twig,$data,$general,$galeria,$seo){ 
	$data["general"] = $general;
   	$data["galeria"]= $galeria;
    $data["my_title"] = "about";
    $data["seo"] = $seo;
    $data["plan"] = array(
		'1' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-tamano-lotes.png', 'des' =>'Ciudad Mayakoba descripcion 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-hectareas-ciclopista.png', 'des' =>'Ciudad Mayakoba descripcion 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-parque-pet-friendly.png', 'des' =>'Ciudad Mayakoba descripcion 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-senderos-cenotes.png', 'des' =>'Ciudad Mayakoba descripcion 4' ),
		'5' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-tamano-lotes.png', 'des' =>'Ciudad Mayakoba descripcion 1' ),
		'6' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-hectareas-ciclopista.png', 'des' =>'Ciudad Mayakoba descripcion 2' ),
		'7' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-parque-pet-friendly.png', 'des' =>'Ciudad Mayakoba descripcion 3' ),
		'8' => array('url' =>'http://www.senderosmayakoba.mx/images/senderos-norte/amenidades-senderos-cenotes.png', 'des' =>'Ciudad Mayakoba descripcion 4' ),
		);
    echo $twig->render('galeria.html',$data);  
});

$app->get('/ciudad-mayakoba', function() use ($twig,$data,$general,$ciudad_mayakoba,$seo){  
    $data["my_title"] = "about";
    $data["general"] = $general;
   	$data["ciudad_mayakoba"]= $ciudad_mayakoba;
   	$data["seo"] = $seo;
    $data["plan"] = array(
		'1' => array('url' =>'images/ciudad-mayakoba/amenidades-consultorios-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 1' ),
		'2' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-parque-metropolitano-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 2' ),
		'3' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-villa-comercial-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 3' ),
		'4' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-ciclovias-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 4' ),
		'5' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-universidades-colegios-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 1' ),
		'6' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-areas-verdes-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 2' ),
		'7' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-laboratorios-medicos-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 3' ),
		'8' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/amenidades-corredores-biologicos-cd-mayakoba.png', 'des' =>'Ciudad Mayakoba descripcion 4' ),
		);
	    $data["images"] = array(
			'1' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/entrada-ciudad-mayakoba.jpg'),
			'2' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/ciclovias-ciudad-mayakoba.jpg'),
			'3' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/entrada-ciudad-mayakoba(2).jpg'),
			'4' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/ciclovias-ciudad-mayakoba(2).jpg'),
			'5' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/centro-comercial-ciudad-mayakoba.jpg'),
			'6' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/palapa-principal-ciudad-mayakoba.jpg'),
			'7' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/village-andador-verde-ciudad-mayakoba.jpg'),
			'8' => array('url' =>'http://www.senderosmayakoba.mx/images/ciudad-mayakoba/full-photos/yoga-ciudad-mayakoba.jpg'),
		);
    echo $twig->render('ciudad-mayakoba.html',$data);  
});

$app->get('/noticias', function() use ($twig,$data){  
    $data["my_title"] = "about";
    echo $twig->render('noticias.html',$data);  
});

$app->get('/contacto', function() use ($twig,$data,$general,$contacto,$seo){
	$data["general"] = $general;
   	$data["contacto"]= $contacto;
   	$data["seo"] = $seo;
    $data["my_title"] = "about";
    echo $twig->render('contacto.html',$data);  
});

$app->run();  