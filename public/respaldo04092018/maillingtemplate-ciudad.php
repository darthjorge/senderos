
<?php

$bodyhtmlclient = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Senderos de Mayakoba</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#938268" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="http://senderosmayakoba.mx/web/image/General/logo.png" alt="Senderos de Mayakoba" width="300" height="auto" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<h1 style="text-align: center; color: #153643; font-family: Arial, sans-serif; font-size: 29px; line-height: 20px;">
											Bienvenidos a Senderos de Mayakoba
										</h1>
										<p style="padding: 20px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px; text-align:justify">
											En el corazón de la Riviera Maya aparece el innovador y confortable Residencial donde podrás edificar 
											la casa de tus sueños, formando parte de Ciudad Mayakoba.
										</p>
										<p style="padding: 10px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px; text-align:justify">
											Gracias por comunicarte con Senderos de Mayakoba, un representante se pondrá en contacto contigo para asistirte lo antes posible.
										</p>
										<img width="100%" height="auto" src="http://senderosmayakoba.mx/web/image/ubicacion/banner.jpg" alt="" />

									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#938268" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										Todos los Derechos Reservados &reg; Senderos de Mayakoba.<br/>
									</td>
									<td align="right" width="25%">
										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>';
?>