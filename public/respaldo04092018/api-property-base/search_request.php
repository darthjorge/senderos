<?php

require 'config.php';

$api_lead_source   = "Web";

$api_first_name    = $_POST["nombre"];
$api_last_name     = $_POST["apellido"];
$api_email         = $_POST["email"];
$api_phone         = $_POST["telefono"];
$api_affair        = $_POST["asunto"];
$api_reason        = $_POST["motivo"];
$api_comments      = $_POST["comentarios"];

$api_description  = "
Interesado en : $api_affair \n
Motivo : $api_reason \n
Comentarios: $api_comments";

$api_description = htmlspecialchars_decode($api_description);

$query = array (
  'prospect' => array(
    'token' => $web_to_prospect_token,
    'contact' => array (
      'LeadSource' => $api_lead_source,
      'FirstName' => $api_first_name,
      'LastName' => $api_last_name,
      'Email' => $api_email,
      'Phone' => $api_phone,
      'Description' => $api_description
    )
));

$query = json_encode($query); 

$curl = curl_init($web_to_prospect_endpoint);
// Accept any server (peer) certificate on dev envs
if($dev_env) {
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
}
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(  "Content-type: application/json"));
$info = curl_getinfo($curl);
$response = curl_exec($curl);

$jsonResponse =  json_decode($response);

curl_close($curl);

// if (isset($jsonResponse->{"errorMessage"})) {
//   die("Error: " . $jsonResponse->{"errorMessage"});
// } else {
//   echo "Success";
// }

?>
