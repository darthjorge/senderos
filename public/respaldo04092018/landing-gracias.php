<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
  <head>
    <title>Landing | Senderos de Mayakoba</title>
    <meta charset="UTF-8">  
    <link rel="shortcut icon" href="web/image/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="keywords" content="Senderos de Mayakoba, mayakoba, senderos de mayakoba, terrenos, venta de terrenos, Residencial, ciudad Mayakoba">
    <meta name="description" content="En el corazón de la Riviera Maya aparece el innovador y confortable Residencial donde podrás edificar la casa de tus sueños, formando parte Ciudad Mayakoba.">
    <META name="robots" content="index, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <base href="http://www.senderosmayakoba.mx/"/>
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>-->
    <link href="web/js/bootstrap-3.1.1/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="web/css/main.css" rel="stylesheet" media="screen">
    <link href="web/css/style.css" rel="stylesheet" media="screen">


    <style>
      .menu-absolute{position:absolute;z-index:10}.caption-segura{left:auto;right:0;padding-bottom:30px;bottom:24%;width:48%}.caption-preventa,.caption-terrenos{left:auto;right:0;padding-bottom:30px;bottom:12%;width:39%}.caption-financiamiento{left:auto;right:0;padding-bottom:30px;bottom:15%;width:53%}.pleca a,.pleca a:hover{text-decoration:none}
      #header{background-image: url('/web/image/landing/header-background.png'); background-size: contain;}
      .logo{padding: 30px 0;}
      #content{padding: 50px 0; background-image: url('/web/image/landing/backgournd.jpg');background-size: 100% 100%; margin-top: -50px}
      .carousel-caption {
        
        left: 0;
        width: 50%;
        padding: 0;
        bottom: 0;
        height: 100%;
      }
      #form{  width: 50%;
  position: absolute;
  top: 30px;
  z-index: 500;
  color: #000;}
  .contact-footer .btn-enviar {

  background: #F2B66D!important;
}
#response{color: #fff;}
  @media (max-width: 767px) {
  .form-group {
  margin-bottom: 0px; 
}
#form{width: 100%!important;position: relative!important;}
}
    </style>
  </head> 
  
  <body class="">
    <header id="header" >
      <div class="clearfix container-fluid logo">
          <div class="col-sm-4 col-sm-offset-4 text-center">
            <a href="http://www.senderosmayakoba.mx/"><img class="img-responsive" src="web/image/landing/logo-senderos.png"></a>          
          </div>
      </div>  
    </header>


    <div id="content" class="clearfix container-fluid">
      <div class="col-sm-10 col-sm-offset-1">
        <h2 class="text-center montserrat color1 hidden-xs" style="  font-size: 23px;">Vive en el mejor lugar de Playa del Carmen y construye el hogar de tus sueños.</h2>
        <h2 class="text-center montserrat color1 hidden-xs" style="  font-size: 23px;">¡Contáctanos ahora!</h2>
        <h2 class="text-center montserrat color1 visible-xs" style="  font-size: 23px;">Gracias, tus datos han sido enviados.</h2>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators 
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
              <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            </ol>-->

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="./web/image/landing/imagen-promocion(1).jpg" alt="Presentación de Senderos">
                  <div class="carousel-caption  background3 hidden-xs">
                  </div>
              </div>
              <div class="item ">
                <img src="./web/image/landing/imagen-financiamineto(2).jpg" alt="Presentación de Senderos">
                  <div class="carousel-caption  background2 hidden-xs">
                  </div>
              </div>
              <div class="item ">
                <img src="./web/image/landing/imagen-pagos(3).jpg" alt="Presentación de Senderos">
                  <div class="carousel-caption  background5 hidden-xs">
                  </div>
              </div>
              
            </div>
            <form method = "POST" role="form" class="form contact-section" id="form">
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-nombre">Nombre</label>
                <div class="col-md-8">
                  <input type="text"required name="nombre" class="form-control without-borders" id="form-nombre">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-email">Mail</label>
                <div class="col-md-8">
                  <input type="email" required name="email" class="form-control without-borders" id="form-email" >
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-tel">Teléfono</label>
                <div class="col-md-8">
                  <input type="text" required name="telefono" class="form-control without-borders" id="form-tel">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-ciudad" style="margin-top:-5px">Ciudad</label>
                <div class="col-md-8">
                  <input type="text" required name="ciudad" class="form-control without-borders" id="form-ciudad">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-asunto" style="margin-top:-5px">Asunto</label>
                <div class="col-md-8">
                  <input type="text" required name="asunto" class="form-control without-borders" id="form-asunto">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-md-offset-1" for="form-mensaje">Mensaje</label>
                <div class="col-md-8">
                  <textarea name="mensaje" class="form-control without-borders" id="form-mensaje" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                  <input type="checkbox" name="option1" value="Milk"> <span class="montserrat color1">Deseo recibir noticias sobre Senderos de Mayakoba</span><br>
                </div>
              </div> 
               <input class = "newsletter-input open-sans px12" type="hidden" class="form-control" id="newsletter" required name="tipo" value="landing">
              <div class ="form-group">
                <div class = "row contact-footer">
                <div class="col-md-7 col-md-offset-1"><span class="text-center montserrat color1" id="response"> Gracias, tus datos han sido enviados.</span></div>
                  <!--div class = "col-md-8">
                   <a href="master-plan.php" style="float:left;">
                    <div type="" class="btn-disponibilidad background4 ">
                      <span class=" montserrat">Ver disponibilidad de lotes</span>
                      <span class="icon-circle-right vertical-aling"></span>
                    </div>
                  </a>  
                  </div-->
                  <div class = "col-md-3">

                    <button type="submit" class="btn-enviar background8 " style="margin-top: 0;">
                      <span class=" montserrat">Enviar</span>
                      <span class="icon-circle-right vertical-aling"></span>
                    </button>
                  </div>
                </div>
              </div>
              
            </form>
            <!-- Controls 
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>-->
        </div>

      </div>

    </div>
    <footer>
    </footer>
    
      <div itemscope itemtype="http://schema.org/Organization" style="display:none">
        <span itemprop="name">Senderos De Mayakoba</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">Boulevard Playa del Carmen, Km. 299.</span>
            <span itemprop="postalCode">77500</span>
            <span itemprop="addressLocality">Playa del Carmen Q. Roo</span>
          ,
        </div>
          Tel:<span itemprop="telephone">+52 (984) 109 13 70 </span>,
          Fax:<span itemprop="faxNumber">+52 (984) 109 13 70 </span>,
          E-mail: <span itemprop="email">info@senderosmayakoba.mx</span>
        <span itemprop="member" itemscope itemtype="http://schema.org/Organization">
        </span>,
        <span itemprop="member" itemscope itemtype="http://schema.org/Organization">
        </span>,
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!--<script src="web/js/jquery.js" defer></script>-->
    
    <link rel="stylesheet" type="text/css" href="web/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="web/js/fancybox/jquery.fancybox.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="web/css/bannerscollection_zoominout.css">
    <script src = "web/js/swfobject.js" defer></script>
    <!--<script src="vendor/cycle2.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="web/js/bootstrap-3.1.1/js/bootstrap.min.js" defer></script>
    <script src="vendor/fancybox/jquery.fancybox.js" defer></script>
    <script src="web/js/validate.js" defer></script>
    <script src="web/js/lazyImage.js" defer></script>
    <script src="web/js/jquery-ui.js" defer></script>
    <script src="web/js/bannerscollection_zoominout.js" defer></script>
    {% block js %}{% endblock %}
    <script type="text/javascript">
    $(document).ready(function(){

      $('.form').validate({
        submitHandler: function(form) {
            $('.btn-enviar').attr('disabled','disabled');
              $.ajax("landing-mail.php",{
            data:$("#form").serialize(),
            type:'post',
            cache:false,
            beforeSend: function(result){
                $("#response").html("Espere...");
            },
            success: function(result){
                console.log(result);
                var obj = jQuery.parseJSON(result);
                if (obj.response === "contacto"){
                    window.location="http://www.senderosmayakoba.mx/contacto-gracias.php#contact-title";
                }else if(obj.response === "landing"){
                    window.location="http://www.senderosmayakoba.mx/landing.php/gracias";
                }else{
                    window.location="http://www.senderosmayakoba.mx/index.php";
                };

                /*if(result === "contacto"){
                    
                }else if(result == "news"){
                    window.location="http://www.senderosmayakoba.mx/index.php";
                }else{
                }*/
            },
            error:function(result){

            }
        })
    }});

});
    </script>
    <!--<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA_TbxX1m1biZuaHgiZKmrrUPpvA046twA&sensor=false"></script>
    <script type="text/javascript" src="web/js/mapa.js"></script>-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-60781305-1', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- Google Code for Landing Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 937629747;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "U-zXCNngtmEQs7CMvwM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/937629747/?label=U-zXCNngtmEQs7CMvwM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript> 
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/952538608/?label=vit_COu11F8Q8KuaxgM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript> 


      <script type="text/javascript">
      setTimeout(function(){var a=document.createElement("script");
      var b=document.getElementsByTagName("script")[0];
      a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0024/3262.js?"+Math.floor(new Date().getTime()/3600000);
      a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
      </script>

  </body>

</html>