<?php 
    $content= array();
    $genral=array();
    if (LANG == 'es'){
        $general = array(
                'menu' => array(
                'agendar_cita'=>'ENVIAR MENSAJE',                   
                'menu'=>'MENÚ',    
                'home'=>'INICIO',    
                'senderos'=>'SENDEROS',    
                'senderos_norte'=>'SENDEROS NORTE',    
                'gallery'=>'GALERÍA',    
                'ciudad_mayakoba'=>'CIUDAD MAYAKOBA',    
                'news'=>'NOTICIAS',
                'contact'=>'CONTACTO',
                'suscribete' => 'SUSCRÍBETE A NUESTRO NEWSLETTER:',
                'derechos'=>'TODOS LOS DERECHOS RESERVADOS ® SENDEROS CIUDAD MAYAKOBA',
                'enviar'=>'ENVIAR',
            ),
        );
        $home = array(
            // SENDEROS
            'title_senderos' =>array('text'=>'BIENVENIDOS A SENDEROS'),
            'lotes_res' =>array('text'=>'Lotes Residenciales'),
            'description_senderos1' =>array('text'=>'SENDEROS de Ciudad Mayakoba es un proyecto diseñado y creado por un grupo de inversionistas mexicanos en conjunto con el grupo OHL Desarrollos.'),
            'description_senderos2' =>array('text'=>'SENDEROS de Ciudad Mayakoba también contará con áreas comerciales que brindarán servicios de primera necesidad para toda la comunidad, en una zona cercana, pero independiente del área residencial, garantizando privacía y calidad de vida.'),
            // ICONOS-NUMEROS
            'iconos_numeros' =>array('plantas'=>'PLANTAS NATIVAS','plantas2'=>'RESCATADAS','recorridos'=>'SENDEROS PARA','recorridos2'=>'RECORRIDOS','areas'=>'DE ÁREAS VERDES','areas2'=>'EN EL DESARROLLO','animales'=>'PASOS DE ANIMALES','animales2'=>'EN 20 CALLES'),

            //DISPONIBILIDAD
            'title_disponibilidad' =>array('text'=>'DISPONIBILIDAD','description1'=>'¡Te invitamos a conocer la disponibilidad de nuestros proyectos! Navega a través de los planos, da clic en el lote que te interese ','description2'=>'y conoce un poco más sobre los terrenos y sus características.'),

            // AMENIDADES
            'amenidades' =>array('title'=>'AMENIDADES','amenidades_description'=>'SENDEROS de Ciudad Mayakoba se muestra como un magnífico acoplamiento entre el lujo y la naturaleza, un oasis de diseño sofisticado con amenidades y servicios innovadores. ¡Descúbrelos todos!','amenidades_description1'=>'SENDEROS de Ciudad Mayakoba se muestra como un magnífico acoplamiento entre el lujo y la naturaleza, un oasis de diseño sofisticado con amenidades y servicios innovadores.','amenidades_description2'=>'¡Haz clic en los iconos y descúbrelos todos!'),

            // ICONOS AMENIDADES
            'iconos_amenidades' =>array('1'=>'SENDEROS','2'=>'CENOTES','3'=>'PARQUES','4'=>'CASA CLUB','5'=>'CICLOPISTA','6'=>'MEMBRESÍA','7'=>'NATURALEZA','8'=>'CIUDAD MAYAKOBA'),

            // MASTER PLAN
            'master_plan' =>array('title'=>'MASTER PLAN','description'=>'Te invitamos a conocer más acerca de este gran desarrollo. ¡Navega a través del Plan Maestro, da click en el área que estés interesado y conoce más acerca de Senderos!','button'=>'EXPLORAR'),

            // MASTER PLAN MAPA
            'master_plan_mapa' =>array('1'=>'1. Acceso Principal a Senderos','2'=>'2. Acceso Principal a Senderos Norte','3'=>'3. Casa Club','4'=>'4. Gran Cenote','5'=>'5. Parque Sendero de los Cenotes','6'=>'6. Cenote Mágico','7'=>'7. Parque Alameda de los Niños','8'=>'8. Parque Rosaleda','9'=>'9. Pet Park','10'=>'10. Baby Park','11'=>'11. Sendero Sur','12'=>'12. Acceso por Boulevard Parque México','13'=>'13. Área Comercial','14'=>'14. The Village Mayakoba','15'=>'15. Mayakoba Country Club','16'=>'*Estas imágenes son meramente de uso ilustrativo y pueden cambiar sin previo aviso.'),

            'financiamiento' =>array('title'=>'FINANCIAMIENTO','1'=>'Lotes desde 160','2'=>'Excelentes planes de financiamiento propio en pesos','3'=>'30% de enganche','4'=>'Hasta 36 meses de financiamiento','5'=>'Plusvalía garantizada'),
        
        );
        $plan = array(
            '1' => array('url' =>'images/home/amenidades-imagenes/amenidades-senderos.jpg', 'title' => 'SENDEROS', 'des' =>'El eje del proyecto son los senderos que recorren todo el residencial y se conectan para caminar en las áreas verdes que les rodean y a su vez comunican con los parques. A una distancia muy corta de tu casa siempre habrá un sendero que te lleve a disfrutar los atractivos de SENDEROS de Ciudad Mayakoba.'),

            '2' => array('url' =>'images/home/amenidades-imagenes/amenidades-cenotes.jpg', 'title' => 'CENOTES', 'des' =>'SENDEROS de Ciudad Mayakoba cuenta con dos cenotes naturales dentro del residencial: el Gran Cenote, ideal para meditar, practicar yoga o simplemente observar a las aves que ahí también acuden, y el Cenote Mágico, donde podrá refrescarse, leer y tomar el sol.'),
            
            '3' => array('url' =>'images/home/amenidades-imagenes/amenidades-parque.jpg', 'title' => 'PARQUES', 'des' =>'<p>SENDEROS de Ciudad Mayakoba cuenta con cinco parques:</p> <p> - Parque Sendero de los Cenotes: selva nativa, observación de aves, pacíficos recorridos, auténtico contacto con la naturaleza.</p> <p>- Parque Alameda de los Niños: juegos interactivos, fuentes, pista de patinaje, foro infantil.</p><p>- Baby Park: un lugar pensado especialmente para los más pequeños del hogar.</p><p> - Parque de las Mascotas: área de juegos y descanso para las mascotas de la familia. </p><p>- Parque Mini Veleros: Un espacio donde podrás disfrutar de la tranquilidad y belleza de nuestro espejo de agua mientras te diviertes navegando tu barco a escala. </p><p>- Parque de los Niños: un lugar lleno de juegos y diversión para disfrutar con la familia.</p>'),

            '4' => array('url' =>'images/home/amenidades-imagenes/amenidades-casa-club.jpg', 'title' => 'CASA CLUB', 'des' =>'<p>Punto de encuentro para jóvenes y adultos, contará con:</p> <p>- Gimnasio con máquinas para tonificación muscular, máquinas para entrenamiento aeróbico. </p><p>- Alberca con 2 carriles de 25 metros, dimensionamiento olímpico para entrenamiento y practica de natación semi profesional. </p><p>- Alberca recreativa con barras de snack y zona de asoleaderos. </p><p>- Zona para Yoga y Tai Chi. </p><p>- Salón de usos múltiples con capacidad máxima para 60 personas; para reuniones, eventos, conferencias, proyecciones etc. </p><p>- Duchas y sanitarios. </p><p>- Terraza para eventos al aire libre con equipamiento para preparación de alimentos al carbón y con capacidad máxima de 60 personas. </p><p>- Cancha de pádel.</p>'),

            '5' => array('url' =>'images/home/amenidades-imagenes/amenidades-ciclopistas.jpg', 'title' => 'CICLOPISTA', 'des' =>'SENDEROS de Ciudad Mayakoba privilegia el uso de la bicicleta y ha diseñado una ciclopista a lo largo de las vialidades principales, donde además de pedalear podrás realizar actividades como andar en patineta y ejercitarte rodeado de un ambiente natural; los recorridos podrán ser tan amplios como tu resistencia física.' ),

            '6' => array('url' =>'images/home/amenidades-imagenes/amenidades-membresia.jpg', 'title' => 'MEMBRESÍA', 'des' =>'<p>Respaldado por la gran marca Mayakoba, podrás disfrutar de los siguientes servicios:</p> <p>- Golf: Paquetes de golf exclusivos para residentes de Senderos de Ciudad Mayakoba. </p><p>- Descuentos en Casa Club y en Restaurante Koba. </p><p>- Descuento en canchas de tenis y paddle. </p><p>- Descuento en tiro con arco, kayak y cricket. </p><p>- Uso de la Casa Club de Mayakoba. </p><p>- Uso de las instalaciones de el “Pueblito e Iglesia” para eventos. </p>'),

            '7' => array('url' =>'images/home/amenidades-imagenes/amenidades-naturaleza.jpg', 'title' => 'NATURALEZA', 'des' =>'SENDEROS de Ciudad Mayakoba es un acoplamiento perfecto entre el lujo y la naturaleza, un residencial que respeta y aprecia el medio ambiente. Contamos con más de 130 mil metros cuadrados de áreas verdes respetando la vegetación nativa, corredores ecológicos y pasos de animales en veinte calles del residencial. Tu hogar estará siempre rodeado de naturaleza.' ),

            '8' => array('url' =>'images/home/amenidades-imagenes/amenidades-ciudad-mayakoba.jpg', 'title' => 'CIUDAD MAYAKOBA', 'des' =>'SENDEROS forma parte de Ciudad Mayakoba, un innovador desarrollo residencial que cuenta con todos los servicios a tan solo unos pasos: área comercial, escuelas y universidades, centros culturales, hospital y mucho más.'),
        );
        $senderos_norte = array(
            // SENDEROS - NORTE
            'senderos' =>array('title'=>'SENDEROS NORTE','description'=>'Senderos Norte ofrece predios unifamiliares desde 160 m², con seguridad 24/7, servicios subterráneos, avenidas de bajo tráfico, áreas verdes, senderos y parques que cuentan con eco-iluminación para respetar el escenario principal: la naturaleza.'),
            'iconos_senderos' =>array('1'=>'LOTES DESDE 160 ','2'=>'5 KM DE  CICLOPISTA','3'=>'PET PARK','4'=>'SENDEROS Y CENOTES'),
            'disponibilidad_senderos' =>array('disponibilidad_senderos'=>'DISPONIBILIDAD'),
            'senderos_select' =>array('1'=>'Etapa 1','2'=>'Etapa 2','3'=>'Etapa 3'),
            'avances_de_obra' =>array('title'=>'AVANCES DE OBRA','description'=>'Te invitamos a conocer el progreso que Senderos Norte para que descubras cómo este increíble proyecto va culminando su desarrollo.'),
           
        );

        $senderos_mayakoba = array(
            // SENDEROS - MAYAKOBA
            'senderos' =>array('title'=>'SENDEROS','description'=>'Senderos ofrece lotes residenciales desde 300 m², siendo la primera comunidad integralmente planeada en la Riviera Maya;  diseñada con base en cuatro pilares: sustentabilidad ambiental, planeación urbana, comunidad participativa y seguridad. '),

            'iconos_senderos' => array('1'=>'LOTES DESDE 300 ','2'=>'5 KM DE CICLOPISTA','3'=>'PARQUE DE LOS NIÑOS','4'=>'SENDEROS Y CENOTES',),

            'disponibilidad' => array('title'=>'DISPONIBILIDAD','etapa1'=>'Etapa 1','etapa2'=>'Etapa 2','etapa3'=>'Etapa 3','etapa4'=>'Etapa 4'),

            'avances_de_obra' => array('title'=>'AVANCES DE OBRA','description'=>'Te invitamos a conocer el progreso de Senderos para que descubras cómo este increíble proyecto va culminando su desarrollo.'),

        );


        $galeria = array(
            // GALERIA
            'galeria' =>array('title'=>'GALERÍA','description'=>'Dale un vistazo a lo que será el concepto único que Senderos de Ciudad Mayakoba propone, con amplias áreas verdes, un diseño inigualable, un modelo sustentable y un estilo eco-chic único en su tipo.','imagen1'=>'Acceso Senderos','imagen2'=>'Acceso Senderos Norte','imagen3'=>'Casa Club','imagen4'=>'Gran Cenote','imagen5'=>'Cruce peatonal','imagen6'=>'Cenote Mágico','imagen7'=>'Parque de los Niños','imagen8'=>'Parque de los Niños','imagen9'=>'Acceso Boulevard Parque Mexico','imagen10'=>'Acceso Senderos Mayakoba','imagen11'=>'Gran Cenote','imagen12'=>'Parque Sendero de los Cenotes','imagen13'=>'Acceso Senderos Norte','imagen14'=>'Acceso Parque Sendero de los Cenotes'),
            
           
        );
        $ciudad_mayakoba = array(
            // CIUDAD MAYAKOBA
            'title_mayakoba' =>array('text'=>'CIUDAD MAYAKOBA'),
            'description_senderos1' =>array('text'=>'Ciudad Mayakoba es la primer Comunidad Planeada Sustentable de la Riviera Maya, México. Comprometida con la preservación de los ecosistemas y la calidad de vida de sus   habitantes.'),
            // ICONOS-NUMEROS
            'iconos_numeros' =>array('plantas'=>'ÁREAS VERDES DE','plantas2'=>'CONSERVACIÓN','recorridos'=>'DE','recorridos2'=>'CICLOVÍAS','areas'=>'ÁREAS VERDES EN GRAN','areas2'=>'PARQUE METROPOLITANO','animales'=>'CRUCES PARA FAUNA','animales2'=>'EN LAS VIALIDADES'),

            // AMENIDADES
            'amenidades' =>array('title'=>'AMENIDADES','amenidades_description'=>'Caracterizado por distintas tipologías de viviendas Ciudad Mayakoba contempla además diferentes servicios como: áreas comerciales, espacios públicos y equipamientos, entre otros.'),
            // ICONOS AMENIDADES
            'iconos_amenidades' =>array('1'=>'HOSPITAL Y CONSULTORIOS','2'=>'GRAN PARQUE METROPOLITANO','3'=>'VILLA COMERCIAL','4'=>'CICLOVÍAS Y ANDADORES','5'=>'UNIVERSIDADES Y COLEGIOS','6'=>'EXTENSAS ÁREAS VERDES','7'=>'LABORATORIOS MÉDICOS','8'=>'CORREDORES BIOLÓGICOS'),
        );

        $contacto = array(
            // CONTACTO
            'contacto' =>array('title'=>'CONTACTO','description'=>'Senderos está ubicado a tan sólo 5 minutos del centro de Playa del Carmen, y cerca de los más emblemáticos lugares de la Riviera Maya. ¡No dudes en ponerte en contacto con nosotros para recibir más información de este maravilloso residencial!'),

            'iconos_contacto' =>array('title1'=>'TELÉFONOS','1'=>'Playa del Carmen:','2'=>'+52(984)109 13 70','3'=>'Cancún:','4'=>'+52(988)884 41 45'),

            'iconos_contacto2' =>array('title2'=>'DIRECCIÓN','1'=>'Boulevard Playa del Carmen kilómetro 299'),
            'iconos_contacto3' =>array('title3'=>'CORREO ELECTRÓNICO','1'=>'Rellena el cuestionario para recibir más información.'),
            'iconos_contacto4' =>array('title4'=>'REDES SOCIALES','1'=>'@SenderosMayakoba'),

            'formulario' =>array('1'=>'NOMBRE','2'=>'EMAIL',
                                 'select1'=>'INTERESADO EN:','select2'=>'Senderos Mayakoba Lotes desde 300m²','select3'=>'Senderos Norte Lotes desde 160m²','3'=>'APELLIDOS','4'=>'TELÉFONO',
                                 'select4'=>'MOTIVO:','select5'=>'Información','select6'=>'Financiamiento','select7'=>'','7'=>'MENSAJE',
                                 '8'=>'ENVIAR MENSAJE'),

           
        );
        $seo = array(
            // seo
            'home' =>array('title'=>'Bienvenidos a Senderos de Ciudad Mayakoba','description'=>'Senderos es un residencial planeado cuidadosamente que combina la elegancia y el lujo con la naturaleza y la conservación del medioambiente.','keywords'=>'senderos de mayakoba, senderos mayakoba, venta terrenos mayakoba'),
            'senderos' =>array('title'=>'Senderos - Senderos de Ciudad Mayakoba','description'=>'Senderos ofrece terrenos desde 300 m² para que puedas construir el hogar de tus sueños.','keywords'=>'terrenos mayakoba, senderos de mayakoba, senderos mayakoba'),
            'senderos_norte' =>array('title'=>'Senderos Norte - Senderos de Ciudad Mayakboa','description'=>'Senderos Norte ofrece terrenos unifamiliares desde 160 m², con seguridad 24/7, areas verdes y muchas otras amenidades. ','keywords'=>'senderos norte, mayakoba, senderos de mayakoba'),
            'galeria' =>array('title'=>'Galería - Senderos de Ciudad Mayakoba','description'=>'Descubre las fotos y renders de cómo será el concepto único que Senderos de Ciudad Mayakoba propone.','keywords'=>'fotos senderos mayakoba, fotos senderos de mayakoba, fotos mayakoba'),
            'ciudad_mayakoba' =>array('title'=>'Ciudad Mayakoba - Senderos de Ciudad Mayakoba','description'=>'Senderos forma parte de Ciudad Mayakoba, un innovador desarrollo residencial que cuenta con todos los servicios a tan solo unos pasos.','keywords'=>'ciudad mayakoba, ciudad de mayakoba, ciudad mayakoba playa del carmen'),
            'noticias' =>array('title'=>'Blog - Senderos de Ciudad Mayakoba','description'=>'Descubre las últimas noticias y útiles consejos sobre turismo para disfrutar de los alrededores de lo que será tu futuro hogar en Playa del Carmen.','keywords'=>'blog de viajes, blog de turismo, blog playa del carmen'),
            'contacto' =>array('title'=>'Contacto - Senderos de Ciudad Mayakoba','description'=>'Senderos tiene una ubicación privilegiada a tan sólo 5 minutos del centro de Playa del Carmen. Contáctanos a través de teléfono, email o redes sociales.','keywords'=>'telefono senderos de mayakoba, correo senderos de mayakoba, ubicación senderos de mayakoba'),
        );
    }
    else {
     $general = array(
            'menu' => array(
                'agendar_cita'=>'SEND MESSAGE',                   
                'menu'=>'MENU',    
                'home'=>'HOME',    
                'senderos'=>'SENDEROS',    
                'senderos_norte'=>'SENDEROS NORTE',    
                'gallery'=>'GALLERY',    
                'ciudad_mayakoba'=>'CIUDAD MAYAKOBA',    
                'news'=>'NEWS',
                'contact'=>'CONTACT',
                'suscribete' => 'SUSCRIBE TO OUR NEWSLETTER:',
                'derechos'=>'ALL RIGHTS RESERVED ® SENDEROS MAYAKOBA',
                'enviar'=>'SEND',
            ),
        );
        $home = array(
            // SENDEROS
            'title_senderos' =>array('text'=>'WELCOME TO SENDEROS'),
            'lotes_res' =>array('text'=>'Residential Lots'),
            'description_senderos1' =>array('text'=>'SENDEROS of Ciudad Mayakoba is a project designed and created by a group of Mexican investors along with OHL Desarrollos.'),
            'description_senderos2' =>array('text'=>'SENDEROS of Ciudad Mayakoba will also feature commercial areas that provide essential services to the community, in an area nearby, but independent of residential area, guaranteeing privacy and quality of life.
            '),
            // ICONOS-NUMEROS
            'iconos_numeros' =>array('plantas'=>'NATIVE PLANTS','plantas2'=>'RESCUED','recorridos'=>'PATHS FOR','recorridos2'=>'TREKKING','areas'=>'OF GREEN AREAS','areas2'=>'IN THE COMPLEX','animales'=>'ANIMAL PATHS','animales2'=>'IN 20 STREETS'),

            // AMENIDADES
            'amenidades' =>array('title'=>'AMENITIES','amenidades_description'=>'SENDEROS of Ciudad Mayakoba features a magnificent pairing of luxury with nature, offering a tranquil oasis of sophisticated design with innovative services. Discover all of them!','amenidades_description1'=>'SENDEROS of Ciudad Mayakoba features a magnificent pairing of luxury with nature, offering a tranquil oasis of sophisticated design with innovative services.','amenidades_description2'=>'¡Click on the icons and discover them all!'),







            // ICONOS AMENIDADES
            'iconos_amenidades' =>array('1'=>'SENDEROS','2'=>'CENOTES','3'=>'PARKS','4'=>'CLUB HOUSE','5'=>'INFINITY BIKE PATH','6'=>'MEMBERSHIP','7'=>'NATURE','8'=>'CIUDAD MAYAKOBA'),
            //DISPONIBILIDAD
            'title_disponibilidad' =>array('text'=>'AVAILABILITY','description1'=>'We invite you to know the availability of our projects! Surf across the plan, click on the lot that interests you and know a','description2'=>'little more about the land and its characteristics.'),

            // MASTER PLAN
            'master_plan' =>array('title'=>'MASTER PLAN','description'=>'We invite you to know more about this great Project. Browse through our Master Plan and click in the area in which you are interested to learn more about.','button'=>'EXPLORE'),

            // MASTER PLAN MAPA
            'master_plan_mapa' =>array('1'=>'1. Principal Access to Senderos','2'=>'2. Principal Access to Senderos Norte','3'=>'3. Club House','4'=>'4. Gran Cenote','5'=>'5. Sendero de los Cenotes Park','6'=>'6. Cenote Magico','7'=>'7. Alameda de los Niños Park','8'=>'8. Rosaleda Park','9'=>'9. Pet Park','10'=>'10. Baby Park','11'=>'11. Sendero Sur','12'=>'12. Access by Boulevard Parque Mexico','13'=>'13. Comercial Area','14'=>'14. The Village Mayakoba','15'=>'15. Mayakoba Country Club','16'=>'*These images are merely illustrative purposes only and may change without notice.'),
            //FINANCIAMIENTO
            'financiamiento' =>array('title'=>'FINANCING','1'=>'Lots from 160','2'=>'Financing in pesos','3'=>'30% of down payment','4'=>'Up to 36 months of funding','5'=>'Guaranteed appreciation'),
        
        );
        $plan = array(
            '1' => array('url' =>'images/home/amenidades-imagenes/amenidades-senderos.jpg', 'title' => 'SENDEROS', 'des' =>'The axis of the project centers around the connected paths that wind through the residential complex surrounded by green areas that communicate with the parks. There will allways be a path nearby your house to lead you to enjoyment of the attractions of SENDEROS of Ciudad Mayakoba.'),

            '2' => array('url' =>'images/home/amenidades-imagenes/amenidades-cenotes.jpg', 'title' => 'CENOTES', 'des' =>'SENDEROS of Ciudad Mayakoba Norte have two natural cenotes (sinkholes): the Gran Cenote, ideal for meditating, doing yoga, or simply watching the birds that also flock there and the Cenote Magico where the you can cool off, read, and sunbathe.'),
            
            '3' => array('url' =>'images/home/amenidades-imagenes/amenidades-parque.jpg', 'title' => 'PARKS', 'des' =>'<p>SENDEROS of Ciudad Mayakoba has six parks:</p> <p> - Parque de los Cenotes: nature, bird-watching, peaceful walks.</p> <p>- Parque Alameda de los Niños (Children’s Park): interactive games, fountains, a mini-soccerfield, and a labyrinth of neatly trimmed bushes.</p><p>- Baby Park: designed especially for the little ones.</p><p> - Parque de las Mascotas (Pet Park): play and rest area for the family’s pets</p><p>- Parque Mini Veleros: a space where you can enjoy the tranquility and beauty of our mirrorof water while you have fun navigating your boat at scale.</p><p>- Parque de los Niños (Children’s Park): a place full of games and fun to enjoy with the familiy</p>'),

            '4' => array('url' =>'images/home/amenidades-imagenes/amenidades-casa-club.jpg', 'title' => 'CLUB HOUSE', 'des' =>'<p>Modern Club House which will be a meeting point for young people and adults, where you can enjoy with its first class facilities:</p> <p>- Gym</p><p>- Swimming pool with two lanes of 25 meters.</p><p>- Recreational swimming pool with snack bars and more.</p><p>- Yoga and Tai Chi zone</p><p>- Multi-purpose salon with máximum capacity for 60 people; for meetings, events, conferences, etc.</p><p>- Showers and bathrooms</p><p>- Terrace for outside events outside with food preparation equipment and with a maximum capacity of 60 people.</p><p>- Paddle tennis court</p>'),

            '5' => array('url' =>'images/home/amenidades-imagenes/amenidades-ciclopistas.jpg', 'title' => 'INFINITY BIKE PATH', 'des' =>'SENDEROS of Ciudad Mayakoba favors the use of the bicycle and has designed a bicycle path along the main streets. You will be able to ride a bicycle and perform other activities such as skating and exercising in natural surroundings.'),

            '6' => array('url' =>'images/home/amenidades-imagenes/amenidades-membresia.jpg', 'title' => 'MEMBERSHIP', 'des' =>'<p>Backed by Mayakoba brand, you can enjoy the following services:</p> <p>- Exclusive golf packages for residents of Senderos Mayakoba. </p><p>- Discounts in the Club House and at Koba Restaurant. </p><p>- Tennis and paddle-ball court discounts. </p><p>- Discounts on archery, kayaking, and croquet. </p><p>- Use of the Mayakoba Club House. </p> <p>- Use of the facilities of the Pueblito e Iglesia for events. </p>'),

            '7' => array('url' =>'images/home/amenidades-imagenes/amenidades-naturaleza.jpg', 'title' => 'NATURE', 'des' =>'SENDEROS of city Mayakoba is the perfect link between the luxury and the nature, a residential that respects and appreciates the environment. We have over 130 thousand square meters of green areas while respecting the native vegetation, ecological corridors and paths for animals in twenty streets of the residential. Your home will be always surrounded by nature.' ),

            '8' => array('url' =>'images/home/amenidades-imagenes/amenidades-ciudad-mayakoba.jpg', 'title' => 'CIUDAD MAYAKOBA', 'des' =>'SENDEROS is part of Ciudad Mayakoba, an innovative residential development that has all the amenities just a few steps away: shopping area, schools and universities, cultural centres, hospital and much more.'),
        );
        $senderos_norte = array(
            // SENDEROS - NORTE
            'senderos' =>array('title'=>'SENDEROS NORTE','description'=>'Senderos Norte offers residential lots from 160 m² with security 24/7, underground services, avenues of concrete hydraulic private traffic, with gardens and parks that have eco-ilumination to respect the principal scenery: the nature.'),
            
            'iconos_senderos' =>array('1'=>'LOTS FROM 160','2'=>'5KM OF BIKEPATHS','3'=>'PET PARK','4'=>'PATHS AND CENOTES'),

            'disponibilidad_senderos' =>array('disponibilidad_senderos'=>'AVAILABILITY'),
            'senderos_select' =>array('1'=>'Etapa 1','2'=>'Etapa 2','3'=>'Etapa 3'),
            'avances_de_obra' =>array('title'=>'CONSTRUCTION PROGRESS','description'=>'We invite you to know the construction progress of Senderos Norte. You will witness how this incredible Project gradually culminates into an innovative new residential development.'),
           
        );
        $senderos_mayakoba = array(
            // SENDEROS - MAYAKOBA
            'senderos' =>array('title'=>'SENDEROS','description'=>'Senderos offers residential lots from 300 m², being the first integrally planned community in the Riviera Maya; based on four pillars: integrated community, security, environmental sustainability and urban planning.'),

            'iconos_senderos' => array('1'=>'LOTS FROM 300 ','2'=>'5 KM OF BIKEPATHS','3'=>'CHILDREN’S PARK','4'=>'PATHS AND CENOTES',),

            'disponibilidad' => array('title'=>'AVAILABILITY','etapa1'=>'Stage 1','etapa2'=>'Stage 2','etapa3'=>'Stage 3','etapa4'=>'Stage 4'),

            'avances_de_obra' => array('title'=>'CONSTRUCTION PROGRESS','description'=>'We invite you to know the construction progress of Senderos. You will witness how this incredible Project gradually culminates into an innovative new residential development.'),

        );


        $galeria = array(
            // GALERIA
            'galeria' =>array('title'=>'GALLERY','description'=>'Take a look at what will be the unique concept that Senderos Norte proposes, with spacious green areas, unparalleling design, a sustainable model and a style eco-chic unique in its kind.','imagen1'=>'Principal Access to Senderos','imagen2'=>'Principal Access to Senderos Norte','imagen3'=>'Club House','imagen4'=>'Gran Cenote','imagen5'=>'Pedestrian crossing','imagen6'=>'Cenote Magico','imagen7'=>"Children's Park",'imagen8'=>"Children's Park",'imagen9'=>'Access by Boulevard Parque Mexico','imagen10'=>'Principal Access to Senderos','imagen11'=>'Gran Cenote','imagen12'=>'Sendero de los Cenotes Park','imagen13'=>'Principal Access to Senderos Norte','imagen14'=>'Access to Sendero de los Cenotes Park'),
            
           
        );
        $ciudad_mayakoba = array(
            // CIUDAD MAYAKOBA
            'title_mayakoba' =>array('text'=>'CIUDAD MAYAKOBA'),
            'description_senderos1' =>array('text'=>'Ciudad Mayakoba is the first Sustainable Planned Community of the Riviera Maya, Mexico. Committed to the preservation of ecosystems and the quality of life of its inhabitants'),
            // ICONOS-NUMEROS
            'iconos_numeros' =>array('plantas'=>'OF GREEN','plantas2'=>'CONSERVATION AREAS','recorridos'=>'SENDEROS PARA','recorridos2'=>'RECORRIDOS','areas'=>'OF GREEN AREAS IN THE','areas2'=>'GRAN PARQUE METROPOLITANO','animales'=>'CROSSINGS FOR FAUNA','animales2'=>'IN THE STREETS'),

            // AMENIDADES
            'amenidades' =>array('title'=>'AMENITIES','amenidades_description'=>'Characterized by different types of housing, Ciudad Mayakoba also provides various services such as: shopping areas, public spaces, facilities, among others.'),
            // ICONOS AMENIDADES
            'iconos_amenidades' =>array('1'=>"HOSPITAL AND DOCTOR'S OFFICES",'2'=>'GREAT CENTRAL PARK','3'=>'COMMERCIAL VILLAGE','4'=>'BIKE AND TREKKING PATHS','5'=>'UNIVERSITIES AND SCHOOLS','6'=>'VAST GREEN AREAS','7'=>'MEDICAL LABORATORIES','8'=>'BIOLOGICAL CORRIDORS'),
        );

        $contacto = array(
            // CONTACTO
            'contacto' =>array('title'=>'CONTACT','description'=>'SENDEROS de Ciudad Mayakoba is located just 5 minutes from the Centre Town of Playa del Carmen, and close to the most emblematic places of the Riviera Maya. Do not hesitate to get in touch with us to receive more information on this wonderful residential!'),

            'iconos_contacto' =>array('title1'=>'TELEPHONES','1'=>'Playa del Carmen:','2'=>'+52(984)109 13 70','3'=>'Cancún:','4'=>'+52(988)884 41 45'),

            'iconos_contacto2' =>array('title2'=>'ADRESS','1'=>'Boulevard Playa del Carmen kilometer 299.'),
            'iconos_contacto3' =>array('title3'=>'E-MAIL','1'=>'Fill in the form to receive more information.'),
            'iconos_contacto4' =>array('title4'=>'SOCIAL NETWORKS','1'=>'@SenderosMayakoba'),

            'formulario' =>array('1'=>'FIRST NAME','2'=>'E-MAIL',
                                 'select1'=>'INTERESTED IN:','select2'=>'Senderos lots from 300m²','select3'=>'Senderos Norte lots from 160m²','3'=>'LAST NAME','4'=>'TELEPHONE',
                                 'select4'=>'REASON:','select5'=>'Information','select6'=>'Financing','select7'=>'','7'=>'MESSAGE',
                                 '8'=>'SEND MESSAGE'),

           
        );
        $seo = array(
            // seo
            'home' =>array('title'=>'Welcome to Senderos de Ciudad Mayakoba','description'=>'Senderos is a residential planned carefully that combines elegance and luxury with nature and the preservation of the environment.','keywords'=>'senderos de mayakoba, senderos mayakoba, real estate playa del carmen'),
            'senderos' =>array('title'=>'Senderos - Senderos de Ciudad Mayakoba','description'=>'Senderos offers 300 m² land so you can build the home of your dreams.','keywords'=>'land for sale in mayakoba, senderos mayakoba'),
            'senderos_norte' =>array('title'=>'Senderos Norte - Senderos de Ciudad Mayakboa','description'=>'Senderos Norte offers single-family properties with an average area of 160 m².','keywords'=>'senderos norte, mayakoba, senderos de mayakoba'),
            'galeria' =>array('title'=>'Gallery - Senderos de Ciudad Mayakoba','description'=>'See all the photos and how is the builing process of this unique concept proposed by Senderos de Ciudad Mayakoba.','keywords'=>'photos senderos mayakoba, photos senderos de mayakoba, photos mayakoba'),
            'ciudad_mayakoba' =>array('title'=>'Ciudad Mayakoba - Senderos de Ciudad Mayakoba','description'=>'Senderos is part of Ciudad Mayakoba, an innovative residential development that has all the amenities just a step away.','keywords'=>'ciudad mayakoba, ciudad de mayakoba, ciudad mayakoba playa del carmen'),
            'noticias' =>array('title'=>'Blog - Senderos de Ciudad Mayakoba','description'=>'Discover the latest news and helpful tips on tourism to enjoy the surroundings of what will be your future home in Playa del Carmen.','keywords'=>'travel blog, tourism blog, playa del carmen blog'),
            'contacto' =>array('title'=>'Contact - Senderos de Ciudad Mayakoba','description'=>'"Senderos de Ciudad Mayakoba is located just 5 minutes from the Centre Town of Playa del Carmen. Contact us via phone, email or social networks.
                "','keywords'=>'phone senderos de mayakoba, email senderos de mayakoba, location senderos de   mayakoba, contact form senderos de mayakoba'),
        );
        

    }



?>