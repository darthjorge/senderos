<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
  <head>
    <title>Landing | Senderos Norte</title>
    <meta charset="UTF-8">  
    <link rel="shortcut icon" href="web/image/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta property="og:url"content="http://www.senderosmayakoba.mx/senderos-norte.php/" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Senderos Norte" />
    <meta property="og:description" content="¡Senderos Norte abre una puerta llena de oportunidades en Ciudad Mayakoba! Vive rodeado de naturaleza con seguridad para tu familia y una infraestructura de primer nivel." />
    <meta property="og:image" content="http://www.senderosmayakoba.mx/web/image/landing-snorte/imagen-portico-sendero-norte.jpg" />
    <meta property="fb:app_id" content="966242223397117" /> 

    <meta name="keywords" content="Senderos de Mayakoba, mayakoba, senderos de mayakoba, terrenos, venta de terrenos, Residencial, ciudad Mayakoba">
    <meta name="description" content="En el corazón de la Riviera Maya aparece el innovador y confortable Residencial donde podrás edificar la casa de tus sueños, formando parte Ciudad Mayakoba.">
    <META name="robots" content="index, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://www.senderosmayakoba.mx/"/>
    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,700italic,400italic' rel='stylesheet' type='text/css'>
    <link href="web/js/bootstrap-3.1.1/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="web/css/main.css" rel="stylesheet" media="screen">
    <link href="web/css/style.css" rel="stylesheet" media="screen">


    <style>
      .menu-absolute{position:absolute;z-index:10}.caption-segura{left:auto;right:0;padding-bottom:30px;bottom:24%;width:48%}.caption-preventa,.caption-terrenos{left:auto;right:0;padding-bottom:30px;bottom:12%;width:39%}.caption-financiamiento{left:auto;right:0;padding-bottom:30px;bottom:15%;width:53%}.pleca a,.pleca a:hover{text-decoration:none}
      #header{background-image: url('/web/image/landing/header-background.png'); background-size: contain;}
      .logo{padding: 30px 0;}

      .carousel-caption {
        
        left: 0;
        width: 50%;
        padding: 0;
        bottom: 0;
        height: 100%;
      }

  .contact-footer .btn-enviar {

  background: #F2B66D!important;
}
#response{color: #fff;}
  @media (max-width: 767px) {
  .form-group {
  margin-bottom: 0px; 
}
#form{width: 100%!important;position: relative!important;}
}

.madera{background-image: url('/web/image/landing-snorte/textura-header-madera.jpg');}
.portico{background-image: url('/web/image/landing-snorte/imagen-portico-sendero-norte.jpg'); background-size: cover;}
.crema{background-color: rgba(228, 222, 206, 0.9); padding-top: 15px; padding-bottom: 15px;}
.maven{font-family: 'Maven Pro', sans-serif;}
.roboto{font-family: 'Roboto', sans-serif;}
.black{color:#000!important;}
.bg-boton, .bg-boton:hover{background-color: #70abc3!important;padding: 8px 15px;
    border: none;
    float: right;
    margin-top: 20px;
    margin-right: 28px;
    text-align: center;
    max-width: 160px;
    margin-top: 18px;
    text-decoration: none;
    display: block;
    vertical-align: bottom;}

  .linea-form{border-right: 2px solid #000;}
  .color1{color:#493c27!important;}
  .color2{color:#2f1e03!important;}
  .color3{color:#646464!important;}
  input, textarea{font-size: 16px; color:#64646; font-style: italic;font-family: 'Roboto', sans-serif;}
    </style>

  
  
  </head> 
  
  <body class="">
    <header id="header" >
      <div class="clearfix container-fluid logo madera">
          <div class="col-sm-4 col-sm-offset-4 text-center">
            <img class="img-responsive" src="web/image/landing-snorte/logotipo-sendero-norte(2x).png">
          </div>
      </div>  
    </header>


    <div id="content" class="clearfix container-fluid portico">

      <div class="col-sm-10 col-sm-offset-1 crema"  style="position:relative; overflow:hidden;">
        <img src="web/image/landing-snorte/icono-bg-izquierda.png" style="
          position: absolute;
          left: -61px;
          top: -80px;
          opacity: 0.7;">
        <img src="web/image/landing-snorte/icono-bg-derecha.png" style="
          position: absolute;
          right: -150px;
          top: -80px;
          opacity: 0.7;">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
              <div class="col-sm-12" style="margin-bottom:30px;">
                <h2 class="text-center maven color1" style="  font-size: 20px; font-wight:600">¡Senderos Norte abre una puerta llena de oportunidades en Ciudad Mayakoba! Vive rodeado de naturaleza con seguridad para tu familia y una infraestructura de primer nivel. ¡Lotes desde $580,000 pesos!</h2>
              </div> 
              <div class="col-sm-6 linea-form">
                <h2 class="text-left maven color2" style="  font-size: 23px; font-weight:800; margin-top: 0; margin-bottom: 30px;">CONTACTO</h2>
                <form method = "POST" role="form" class="form contact-section row" id="form">
                    <div class="form-group">
                      
                      <div class="col-md-11">
                        <input type="text"required name="nombre" class="form-control without-borders" id="form-nombre" placeholder="Nombre">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-11">
                        <input type="email" required name="email" class="form-control without-borders" id="form-email" placeholder="E-mail">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-11">
                        <input type="text" required name="telefono" class="form-control without-borders" id="form-tel" placeholder="Teléfono">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-11 col-md-offset-1 color2 roboto italic bold" for="form-asunto" style="margin-top:-5px; font-size:13px">Interesado en:</label>
                      <div class="col-md-6" style="margin-bottom:10px">
                        <input type="checkbox" name="asunto" value="Lotes Residenciales" checked="" style="vertical-align:baseline;"><span class="color3 roboto"> Lotes Residenciales</span>
                        <br>
                        <input type="checkbox" name="asunto" value="Promociones" style="vertical-align: baseline;"><span class="color3 roboto "> Promociones</span>
                      </div>
                      <div class="col-md-6" style="margin-bottom:25px">
                        <input type="checkbox" name="asunto" value="Financiamiento" style="vertical-align: baseline;"><span class="color3 roboto "> Financiamiento</span>
                        <br>
                        <input type="checkbox" name="asunto" value="Brokers" style="vertical-align: baseline;"><span class="color3 roboto "> Brokers</span>
                        <br>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-11">
                        <textarea name="mensaje" class="form-control without-borders" id="form-mensaje" rows="3" placeholder="Mensaje"></textarea>
                      </div>
                    </div> 
                     <input class = "newsletter-input open-sans px12" type="hidden" class="form-control" id="newsletter" required name="tipo" value="landing">
                    <div class ="form-group">
                      <div class = "row contact-footer">
                        <!--div class = "col-md-8">
                         <a href="master-plan.php" style="float:left;">
                          <div type="" class="btn-disponibilidad background4 ">
                            <span class=" montserrat">Ver disponibilidad de lotes</span>
                            <span class="icon-circle-right vertical-aling"></span>
                          </div>
                        </a>  
                        </div-->
                        <div class="col-md-7 col-md-offset-1"><span class="text-center maven color1" style="  font-size: 18px; font-wight:600" id="response"> Gracias, tus datos han sido enviados.</span></div>
                        <div class = "col-md-4 col-md-offset-7 " style="padding:0;">
                          <button type="submit" class="bg-boton " style="margin-top: 20px;">
                            <span class=" maven bold" style="font-size:18px; color:#fff;">Enviar</span>
                            <span class="icon-circle-right vertical-aling " style="font-size:18px; color:#fff;"></span>
                          </button>
                        </div>
                      </div>
                    </div>
                    <span class="text-center montserrat color1" id="response"></span>
                </form>
              </div>
              <div class="col-sm-6" style="padding-top:20px;">
                <div class="row">
                  <div class="col-sm-10 col-sm-offset-1">
                    <h4 class="maven color2 bold" style="font-size:18px">DATOS DE CONTACTO</h4>
                    <p class="">
                      <span class="roboto color1 bold">Dirección</span><br>
                      <span class="color3 roboto  bold">Playa del Carmen</span><br>
                      <span class="color3 roboto ">Boulevard Playa del Carmen, km. 299.</span><br>
                      <br>
                      <span class="color3 roboto  bold">Cancún</span><br>
                      <span class="color3 roboto ">Av. Huayacan, Lt 64 MZ 141 SMZ 310 Plaza Palmaris Local 4 C.P. 77560</span><br>
                    </p>
                    <br>
                    <p class="">
                      <span class="roboto color1 bold">Teléfonos:</span><br>
                      <span class="color3 roboto  bold">Playa del Carmen: </span><spanclass="color3 roboto">+52 (984) 109 13 70</span><br>
                      <span class="color3 roboto  bold">Cancún: </span><spanclass="color3 roboto">+52 (988) 884 41 45</span><br>
                      <span class="color3 roboto  bold">800 Nacional: </span><spanclass="color3 roboto">01-800-890 5858</span><br>
                      <span class="color3 roboto  bold">800 USA: </span><spanclass="color3 roboto">1-844-256 5828</span><br>
                      <span class="color3 roboto  bold">800 CANADA: </span><spanclass="color3 roboto">1-844-422 4926</span><br>
                      
                    </p>
            
                    <br>
                    <a href="mailto:info@senderosmayakoba.mx"><p class="color3 roboto  bold"><img src='/web/image/landing-snorte/icono-mail(2x).png' style="width: 10%;">  info@senderosmayakoba.mx</p></a>
                    <ul style="list-style-type:none;padding:0;margin:0;">
                    <li class="redes-contacto-form" style="list-style-type:none;padding:0;margin:20px 0 0 0;">
                                  <a class="padding-0" href="https://www.facebook.com/SenderosNorte" target="_blank"><img src='/web/image/landing-snorte/icono-facebook(2x).png' style="    width: 10%;"></a> 
                                  <a class="padding-0" href="https://www.twitter.com/senderosnorte" target="_blank"><img src='/web/image/landing-snorte/icono-twitter(2x).png' style="    width: 10%;"></a>
                                  <a class="padding-0" href="https://www.instagram.com/senderosnorte " target="_blank"><img src='/web/image/landing-snorte/icono-instagram(2x).png' style="    width: 10%;"></a>
                                  <span class="color3 roboto  bold" style="margin-top: 10px;"> /SenderosNorte</span>
                              </li>
                              </ul>
                  </div>
                </div>

              </div>
            </div>    
          </div>   
              

        </div>
      </div>

    </div>
    <footer>
    </footer>
    
      <div itemscope itemtype="http://schema.org/Organization" style="display:none">
        <span itemprop="name">Senderos De Mayakoba</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">Boulevard Playa del Carmen, Km. 299.</span>
            <span itemprop="postalCode">77500</span>
            <span itemprop="addressLocality">Playa del Carmen Q. Roo</span>
          ,
        </div>
          Tel:<span itemprop="telephone">+52 (984) 109 13 70 </span>,
          Fax:<span itemprop="faxNumber">+52 (984) 109 13 70 </span>,
          E-mail: <span itemprop="email">info@senderosmayakoba.mx</span>
        <span itemprop="member" itemscope itemtype="http://schema.org/Organization">
        </span>,
        <span itemprop="member" itemscope itemtype="http://schema.org/Organization">
        </span>,
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!--<script src="web/js/jquery.js" defer></script>-->
    
    <link rel="stylesheet" type="text/css" href="web/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="web/js/fancybox/jquery.fancybox.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="web/css/bannerscollection_zoominout.css">
    <script src = "web/js/swfobject.js" defer></script>
    <!--<script src="vendor/cycle2.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="web/js/bootstrap-3.1.1/js/bootstrap.min.js" defer></script>
    <script src="vendor/fancybox/jquery.fancybox.js" defer></script>
    <script src="web/js/validate.js" defer></script>
    <script src="web/js/lazyImage.js" defer></script>
    <script src="web/js/jquery-ui.js" defer></script>
    <script src="web/js/bannerscollection_zoominout.js" defer></script>
    <script type="text/javascript">
    $(document).ready(function(){

      $('.form').validate({
        submitHandler: function(form) {
            $('.bg-boton').attr('disabled','disabled');
            $.ajax("landing-mail-snorte.php",{
            data:$("#form").serialize(),
            type:'post',
            cache:false,
            beforeSend: function(result){
                $("#response").html("Espere...");
            },
            success: function(result){
                console.log(result);
                var obj = jQuery.parseJSON(result);
                if (obj.response === "contacto"){
                    window.location="http://www.senderosmayakoba.mx/contacto-gracias.php#contact-title";
                }else if(obj.response === "landing"){
                    window.location="http://www.senderosmayakoba.mx/senderos-norte.php/gracias";
                }else{
                    window.location="http://www.senderosmayakoba.mx/index.php";
                };

                /*if(result === "contacto"){
                    
                }else if(result == "news"){
                    window.location="http://www.senderosmayakoba.mx/index.php";
                }else{
                }*/
            },
            error:function(result){

            }
        })
    }});
});
    </script>
    <!--<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA_TbxX1m1biZuaHgiZKmrrUPpvA046twA&sensor=false"></script>
    <script type="text/javascript" src="web/js/mapa.js"></script>-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-60781305-1', 'auto');
      ga('send', 'pageview');

    </script>


      <script type="text/javascript">
      setTimeout(function(){var a=document.createElement("script");
      var b=document.getElementsByTagName("script")[0];
      a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0024/3262.js?"+Math.floor(new Date().getTime()/3600000);
      a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
      </script>

  </body>

</html>