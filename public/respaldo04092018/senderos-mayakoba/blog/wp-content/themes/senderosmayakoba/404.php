<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<style>
	.no-found{
		margin-bottom: 30px;
	}
</style>
<div class = "container no-found">
	<div class="row">
		<section class="col-md-10 col-md-offset-1">
			<div class = "row">
				<div class = "col-xs-12 text-center">
					<h1 class = "title montserrat color3"><?php _e( 'Not Found', 'twentythirteen' ); ?></h1>
					<h2 class = "title"><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'twentythirteen' ); ?></h2>
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentythirteen' ); ?></p>
				</div>
			</div>   
		</section>
	</div>
</div>

<?php get_footer(); ?>