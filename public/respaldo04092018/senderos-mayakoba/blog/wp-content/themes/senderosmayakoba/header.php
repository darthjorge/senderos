<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="utf-8">
  
  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>Senderos Mayakoba</title>
  
  <meta name="title" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <!--Always force latest IE rendering engine (even in intranet) & Chrome Frame-->
  <link rel="shortcut icon" href="http://senderosmayakoba.mx/web/image/favicon.ico" class="">
  <meta name="google-site-verification" content="" />
  <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
  
  <meta name="author" content="Punk e-Marketing &amp; Consulting" />
  <meta name="Copyright" content="" />
  
  <!--  Mobile Viewport Fix
  http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
  device-width : Occupy full width of the screen in its current orientation
  initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
  maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
  -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />


  <!-- Iconifier might be helpful for generating favicons and touch icons: http://iconifier.net -->
  <link rel="shortcut icon" href="http://senderosmayakoba.mx/senderos-mayakoba/static/img/favicon.ico" />
  <!-- This is the traditional favicon.
     - size: 16x16 or 32x32
     - transparency is OK -->
     
  <link rel="apple-touch-icon" href="http://senderosmayakoba.mx/senderos-mayakoba/static/img/apple-touch-icon.png" />
  <!-- The is the icon for iOS's Web Clip and other things.
     - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for retina display (IMHO, just go ahead and use the biggest one)
     - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
     - Transparency is not recommended (iOS will put a black BG behind the icon) -->
  
  <!-- concatenate and minify for production -->
  <link href="../static/css/reset.css" rel="stylesheet">
  <link href="../static/css/normalize.css" rel="stylesheet">
  
  <link rel="stylesheet" type="text/css" href="http://senderosmayakoba.mx/senderos-mayakoba/static/IconFonts/flaticon.css" />
  <link rel="stylesheet" type="text/css" href="http://senderosmayakoba.mx/senderos-mayakoba/static/font/flaticon.css" />
  <link rel="stylesheet" href="http://senderosmayakoba.mx/senderos-mayakoba/static/css/bootstrap.min.css" type="text/css">
  <link href="http://senderosmayakoba.mx/senderos-mayakoba/static/css/style.css" rel="stylesheet">
  <link href="http://senderosmayakoba.mx/senderos-mayakoba/static/css/menu.css" rel="stylesheet">


  <link href='https://fonts.googleapis.com/css?family=Crimson+Text:400,700,600italic,600,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- This is an un-minified, complete version of Modernizr. 
     Before you move to production, you should generate a custom build that only has the detects you need. -->
  <script src="../static/js/modernizr.custom.js"></script>
  
  <!-- Application-specific meta tags --> 
  <!-- Windows 8 -->
  <meta name="application-name" content="" /> 
  <meta name="msapplication-TileColor" content="" /> 
  <meta name="msapplication-TileImage" content="" />
  <!-- Twitter -->
  <meta name="twitter:card" content="">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:url" content="">
  <!-- Facebook -->
  <meta property="og:title" content="" />
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="" />
	<?php wp_head(); ?>
  <style type="text/css">
        
        
        @media (max-width: 768px) {
            .container { margin:0 10px;}
            
        }
        @media(min-width:768px) and (max-width:991px){
            .post-info {
                padding: 0 30px;
            }
            .size-50 {
                font-size: 25px;
            }
        }
         @media(min-width:992px) and (max-width:1199px){
           .post-info {
                padding: 0 30px;
            }
            .size-50 {
                font-size: 25px;
            }
         }
            
        
    </style>
</head>

<body class = "home <?php if ( is_singular() and !is_page('Blog') ) { echo ' single-post'; }else{ echo 'no-single';}?>">
	  <header id="blog" class="container ">
       
    </header>
    <div id = "main">

      <?php 
        //echo get_page_template_slug();
        //echo get_page_template();
      ?>