<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<?php
/*
Template Name: Page-blog
*/


get_header();
?>

<style>
	
/* The CSS */
select {
    padding:5px;
    margin: 0;
    border-radius:0px;
    background: #f8f8f8;
    border:none;
    outline:none;
    display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
}

/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
    select {padding-right:18px}
}

label.selecstyle {position:relative}
label.selecstyle:after {
    font:11px "Consolas", monospace;
    color:#fff;
    right:8px; top:2px;
    padding:0 0 2px;
    position:absolute;
    pointer-events:none;
}

label.selecstyle:before {
  content: '';
  right: 0px;
  top: -14px;
  width: 30px;
  height: 30px;
  background: #6facc4;
  position: absolute;
  pointer-events: none;
  display: block;
}
</style>
<div class = "container">
	<div class="row">
		<section class="col-md-10 col-md-offset-1">
			<div class = "row">
				<div class = "col-xs-12 text-center">
					<h1 class = "title montserrat color3">Noticias</h1>
					<p class = "title-caption text-center mancha-texto color4">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id neque at aliquid eaque odio libero enim, repudiandae nihil eum eligendi repellendus cumque expedita placeat et praesentium beatae illum nesciunt! Nesciunt.
					</p>
				</div>
			</div>   
		</section>
	</div>
</div>
<div class = "container-fluid thumnails-img">
	<div class = "container">
		<div class = "row">
			<div class = "col-md-12">
				<div class = "row">
					<div class = "col-md-6">
						<form class="form-inline row" role="form">
							<div class = "col-md-12">
			                  <div class = "row">
			                      <div class="form-group blog-filter col-xs-12 text-center">
			                          <label for="ejemplo_email_3" class="color4 montserrat px12 text-righ">Filtrar por fechas: </label>
			                          <!--<input class = "newsletter-input open-sans px12" type="email" class="form-control" id="ejemplo_email_3"
			                                 placeholder="E-mail"/>-->
			                          <select class = "newsletter-input select_date_post open-sans px12" name="select_date" id="">
											<?php 
												$last_post = get_lastpostdate();
												$first = ax_first_post_date();
												$last_post = explode(" ", $last_post);
												$last_post = explode("-", $last_post[0]);
												$first = explode("-", $first);
												$year_init = $first[0];
												$last_post = $last_post[0];
												for ($year_init; $year_init >= $last_post; $year_init--) { 
													for ($h=5; $h >= 3; $h--) { 
															$h1 = $h;
															if($h < 10){
																$h1 = "0".$h1;
															}
														?>
														<option name = "post-date" data-date = "<?php echo $year_init.'/'.$h1.'/'?>" value="<?php echo $year_init.'/'.$h1.'/'?>">
																<?php echo namemonth($h)."-".$year_init ?>
														</option>
														<?php
													}
												}
											?>
			                          </select>
			                          <label class = "selecstyle"></label>
			                          <!--<button class = "submit-newsletter color1 montserrat px12 background2" type="submit" class="btn btn-default">Enviar</button>-->
			                      </div>
			                  </div>
			                </div>
						</form>
					</div>
					<div class = "col-md-6">
						<form class="form-inline row" role="form" action="../newsletter.php" method="POST">
							<div class = "col-md-12">
			                  <div class = "row">
			                      <div class="form-group blog-filter col-xs-12 text-center">
			                          <label for="ejemplo_email_3" class="color4 montserrat px12 text-righ">Suscríbete a nuestro Newsletter: </label>
			                          <input class = "newsletter-input open-sans px12" type="email" class="form-control" id="ejemplo_email_3"
			                                 placeholder="E-mail"/>
			                          <button class = "submit-newsletter color1 montserrat px12 background2" type="submit" class="btn btn-default">Enviar</button>
			                      </div>
			                  </div>
			                </div>
						</form>
					</div>	
				</div>
			</div>
		</div>
		<!-- BEGIN: Noticias -->
		<?php
			$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

		?>
		<div class = "row">
			<section class = "col-md-12">
				<div class = "row content-post">
						<?php
							$i = 1;
							while (  have_posts() ) : the_post();
						?>
						<div class ="col-sm-6 padding-medium">
							<div class ="row background7">
								<div class ="col-xs-5 col-sm-5 col-md-5 img-single-post-custom">
									<?php 
										if ( has_post_thumbnail() ) { 
											$image_id = get_post_thumbnail_id();
											$image_url = wp_get_attachment_image_src($image_id,'large', true);
											echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="">';
										}
									?>
								</div>
								<div class = "col-xs-7 col-sm-7 col-md-7">
									<div class = "last-post">
										<h3 class = "title open-sans italic color3"><?php the_title(); ?></h3>
										<p class = "text-blog">
											<?php the_excerpt(); ?>
										</p>	
									</div>
								</div>
							</div>
						</div>
					<?php
						// if multiple of 3 close div and open a new div
						if(( ( $i ) % 2 == 0) &&  $i != wp_count_posts() ) {
							echo '</div><div class = "row content-post">';
						}
            			$i++;
						endwhile;
					?>
				</div>
			</section>
		</div>
		<!-- END: Noticias-->
	</div>
</div>
<?php get_footer(); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>