<?php
/*
Template Name: Page-blog
*/

get_header();
?>
<style type="text/css">
    .center-blog-thumb{display: flex;align-items: center;}
    <?php
    if ( 'es' === $GLOBALS['q_config']['language'])
    {
        echo ".esp-change span{text-decoration: none;color: #e57a73;}";
    }
    else if ( 'en' === $GLOBALS['q_config']['language'])
    {
        echo ".eng-change span{text-decoration: none;color: #e57a73;}";
    }
    ?> 
    #item-menu-noticias{background-color: #e57a73!important;}
    @media (max-width: 767px) {
           .center-blog-thumb{display: block;align-items: center;}
           .info{padding: 20px 80px!important;}
        }
</style>
<div id="fullpage">
    <div class="section banner" style="background-color:green">
    	
        <div class="container" style="position:relative">
		    <div class="row">
		        <div class="col-sm-12" style="position:absolute; z-index:2; padding-top:15px;">
		            <div class="row">
		                <div class="col-sm-4 col-xs-6 logo">
		                    <a href="http://www.senderosmayakoba.mx/senderos-mayakoba/index.php/"><img class="img-responsive" src="../static/images/general/logotipo-senderos-mayakoba(2x).png"></a>
		                </div>
		                <div class="col-sm-4 col-xs-6 menu pull-right">
		                    <a class="cd-primary-nav-trigger no-decoration" href="#">
		                        <span class="white-text flaticon-configuration21" style="margin-right: 8px"></span>
		                        <?php
                                if ( 'es' === $GLOBALS['q_config']['language'])
                                {
                                echo'<span class="cd-menu-text roboto bold size-20 white-text"> MENÚ</span>';
                                }
                                if ( 'en' === $GLOBALS['q_config']['language'])
                                {
                                echo'<span class="cd-menu-text roboto bold size-20 white-text"> MENU</span>';
                                }
                                ?>
		                    </a> <!-- cd-primary-nav-trigger -->
		                </div>
		            </div>
		        </div>           
		    </div>
		</div>

<?php
    if ( 'es' === $GLOBALS['q_config']['language'])
    {
    	echo'
<nav>
    <ul class="cd-primary-nav">

                        
                        <a class="close-menu no-decoration" href="#"><span class="white-text flaticon-cross108"></span></a>
                        <p class="lang-menu" style="">
                            <a class=" no-decoration esp-change" href="#" rel="http://senderosmayakoba.mx/senderos-mayakoba/blog/?lang=es"><span class="roboto bold size-20 white-text menu-item" style="padding: 2px 5px">ESP</span></a>
                            <span class="roboto bold size-20 white-text">/</span>
                            <a class=" no-decoration eng-change" href="#" rel="http://senderosmayakoba.mx/senderos-mayakoba/blog/?lang=en"><span class="roboto bold size-20 white-text menu-item" style="padding: 2px 5px">ENG</span></a>
                        </p>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-home">INICIO</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/master-plan?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-master-plan">MASTER PLAN</a>
                        </li>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/amenidades?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-amenidades">AMENIDADES</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/ubicacion?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-ubicacion">UBICACIÓN</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/disponibilidad?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-disponibilidad">DISPONIBILIDAD</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/financiamiento?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-financiamiento">FINANCIAMIENTO</a>
                        </li>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a class="roboto bold size-20 white-text menu-item dropdown-toggle" id="item-menu-galeria" data-toggle="dropdown" href="#">GALERÍA</a>
                            <ul class="dropdown-menu">
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/renders?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-renders">Imágenes</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/avance?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-avance">Avance de Obra</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/eventos?lang=es" class="maven roboto size-20 white-text menu-item" id="item-menu-eventos">Eventos</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/videos?lang=es" class="maven roboto size-20 white-text menu-item" id="item-menu-videos">Videos</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/blog?lang=es" class="roboto bold size-20 white-text menu-item" id="item-menu-noticias">NOTICIAS</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="#contacto-popup" class="roboto bold size-20 white-text menu-item contacto-popup">CONTACTO</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php?lang=es" class="roboto bold size-20 white-text menu-item contacto-popup">SENDEROS NORTE</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <hr class="linea-menu ">
                        </li>

                        <li class="roboto bold size-20 white-text col-sm-4 col-sm-offset-4 col-xs-12 margin-bottom-20">Suscríbete a nuestro Newsletter</li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <form role="form">
                                <div class="form-group r">
                                    <input type="email" class="noto-sans color8 italic form-control input-newsletter col-sm-9 col-xs-9" id="email" placeholder="Email">
                                    <button  type="submit" class="btn btn-default col-sm-3 col-xs-3 btn-send">ENVIAR</button>
                                </div>
                            </form>
                        </li>
                        <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <li class="redes-menu">
                                <a class="white-text no-decoration facebook-icon padding-0 hover-blue"href="https://www.facebook.com/senderosmayakoba"><span class="flaticon-socialnetwork83 color0"></span></a> 
                                <a class="white-text no-decoration instagram-icon padding-0 hover-blue" href="http://instagram.com/senderosmayakoba"><span class="flaticon-instagram19 color0"></span></a>
                                <a class="white-text no-decoration tweet-icon padding-0 hover-blue" href="https://twitter.com/SenderosMk"><span class="flaticon-logo22 color0"></span></a>
                                <span class="roboto bold size-20 white-text redes-texto-menu" style="margin-top: 10px;float: right;"> /SenderosMayakoba</span>
                            </li>
                        </div>
    </ul>                   
</nav>';
}
if ( 'en' === $GLOBALS['q_config']['language'])
    {
    	echo'
<nav>
    <ul class="cd-primary-nav">

                        
                        <a class="close-menu no-decoration" href="#"><span class="white-text flaticon-cross108"></span></a>
                        <p class="lang-menu" style="">
                            <a class=" no-decoration esp-change" href="#" rel="http://senderosmayakoba.mx/senderos-mayakoba/blog/?lang=es"><span class="roboto bold size-20 white-text menu-item" style="padding: 2px 5px">ESP</span></a>
                            <span class="roboto bold size-20 white-text">/</span>
                            <a class=" no-decoration eng-change" href="#" rel="http://senderosmayakoba.mx/senderos-mayakoba/blog/?lang=en"><span class="roboto bold size-20 white-text menu-item" style="padding: 2px 5px">ENG</span></a>
                        </p>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/?lang=en" class="roboto bold size-20 white-text menu-item" id="item-menu-home">HOME</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/master-plan?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-master-plan">MASTER PLAN</a>
                        </li>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/amenidades?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-amenidades">AMENITIES</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/ubicacion?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-ubicacion">LOCATION</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/disponibilidad?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-disponibilidad">DISPONIBILITY</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/financiamiento?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-financiamiento">FINANCING</a>
                        </li>
                        
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a class="roboto bold size-20 white-text menu-item dropdown-toggle" id="item-menu-galeria" data-toggle="dropdown" href="#">GALERY</a>
                            <ul class="dropdown-menu">
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/renders?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-renders">Images</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/avance?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-avance">Building in progress</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/eventos?lang=en"" class="maven roboto size-20 white-text menu-item" id="item-menu-eventos">Events</a></li>
                                <li class="col-sm-12"><a href="http://senderosmayakoba.mx/senderos-mayakoba/index.php/videos?lang=en"" class="maven roboto size-20 white-text menu-item" id="item-menu-videos">Videos</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-mayakoba/blog?lang=en"" class="roboto bold size-20 white-text menu-item" id="item-menu-noticias">NEWS</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="#contacto-popup" class="roboto bold size-20 white-text menu-item contacto-popup">CONTACT</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <a href="http://senderosmayakoba.mx/senderos-norte/index.php?lang=en"" class="roboto bold size-20 white-text menu-item contacto-popup">SENDEROS NORTE</a>
                        </li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <hr class="linea-menu ">
                        </li>

                        <li class="roboto bold size-20 white-text col-sm-4 col-sm-offset-4 col-xs-12 margin-bottom-20">Suscribe to our Newsletter</li>
                        <li class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <form role="form">
                                <div class="form-group r">
                                    <input type="email" class="noto-sans color8 italic form-control input-newsletter col-sm-9 col-xs-9" id="email" placeholder="Email">
                                    <button  type="submit" class="btn btn-default col-sm-3 col-xs-3 btn-send">SEND</button>
                                </div>
                            </form>
                        </li>
                        <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                            <li class="redes-menu">
                                <a class="white-text no-decoration facebook-icon padding-0 hover-blue"href="https://www.facebook.com/senderosmayakoba"><span class="flaticon-socialnetwork83 color0"></span></a> 
                                <a class="white-text no-decoration instagram-icon padding-0 hover-blue" href="http://instagram.com/senderosmayakoba"><span class="flaticon-instagram19 color0"></span></a>
                                <a class="white-text no-decoration tweet-icon padding-0 hover-blue" href="https://twitter.com/SenderosMk"><span class="flaticon-logo22 color0"></span></a>
                                <span class="roboto bold size-20 white-text redes-texto-menu" style="margin-top: 10px;float: right;"> /SenderosMayakoba</span>
                            </li>
                        </div>
    </ul>                   
</nav>';
}
?>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators 
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		  </ol>-->

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      <img class="img-respinsive" src="../static/images/blog/banner-familia-sendero-norte.jpg" alt="Senderos Mayakoba Entrada">
		      <div class="carousel-caption">
                    <?php
                    if ( 'es' === $GLOBALS['q_config']['language'])
                    {
                        echo'<h1 class="maven size-60 white-text bold italic">Blog</h1>
                    <h2 class="maven bold size-40 white-text">Conoce las últimas noticias de Senderos de Mayakoba.</h2>';
                     }
                     if ( 'en' === $GLOBALS['q_config']['language'])
                    {
                        echo'<h1 class="maven size-60 white-text bold italic">Blog</h1>
                    <h2 class="maven bold size-40 white-text">Get the latest news from Senderos de Mayakoba.</h2>';
                     }
                     ?>
                    
                    <!--<p class="maven bold size-20 white-text" style="margin-top: 150px;">CONOCE MÁS</p>
                    <p><img src="../static/images/general/icono-logo-negro(2x).png" style="width: 32px!important; margin-right: 10px"></p>-->
                </div>
		    </div>

		  </div>

		  <!-- Left and right controls 
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>-->
		</div>
    </div>
    <!-- BEGIN: Noticias -->
	<?php
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$args = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => 30,
			'paged' => $paged,
		);
		$i = 1;
		$the_query = new WP_Query($args);
		$flat_wp_first_post = 1;
		$paginator_page_number = $the_query->max_num_pages;
		//print_r($the_query);
	?>
	
			
				
				
				
	<?php
		while ( $the_query->have_posts() ) : $the_query->the_post();
			if(( ( $i ) % 2 == 0) &&  $i != $the_query->post_count ) {
	?>
		<div class="section banner" style="">
			<div class = "container-fluid thumnails-img padding-0 crema-bg center-blog-thumb" style="">						
				
				<div class = "col-xs-12 col-sm-6 col-md-6 info" style="padding:0 80px;">
					<div class = "last-post text-blog">
						<h3 class = "text-center maven medium size-50 orange-text italic"><?php the_title(); ?></h3>
						<p class = "roboto gray-text size-17 text-justify">
							<?php the_excerpt(); ?>
								<?php 
								if ( 'en' === $GLOBALS['q_config']['language'])
                						{
									printf('<p style="margin-top: 50px;text-align: center;"><a class="conoce-mas no-decoration bold" href="'. get_permalink( get_the_ID() ) . '">READ MORE </a></p>');
									}
									if ( 'es' === $GLOBALS['q_config']['language'])
                						{
									printf('<p style="margin-top: 50px;text-align: center;"><a class="conoce-mas no-decoration bold" href="'. get_permalink( get_the_ID() ) . '">LEER MÁS </a></p>');
									}

							  ?>
						</p>	
					</div>
				</div>
				<div class ="col-xs-12 col-sm-6 col-md-6 img-single-post-custom padding-0">
					<?php 
						if ( has_post_thumbnail() ) { 
							$image_id = get_post_thumbnail_id();
							$image_url = wp_get_attachment_image_src($image_id,'large', true);
							echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="">';
						} 
					?>
				</div>
			</div>
		</div>	

	<?php
		}
		else{
	?>
		<div class="section banner" style="">
			<div class = "container-fluid thumnails-img padding-0 amenidades-home center-blog-thumb" style="">						
				
				<div class = "col-xs-12 col-sm-6 col-md-6 col-sm-push-6 col-md-push-6 info" style="padding:0 80px;">
					<div class = "last-post text-blog">
						<h3 class = "text-center maven medium size-50 blue-text italic"><?php the_title(); ?></h3>
						<p class = "roboto gray-text size-17 text-justify">
							<?php the_excerpt(); ?>
							<?php 
								if ( 'en' === $GLOBALS['q_config']['language'])
                						{
									printf('<p style="margin-top: 50px;text-align: center;"><a class="conoce-mas no-decoration bold" href="'. get_permalink( get_the_ID() ) . '">READ MORE </a></p>');
									}
									if ( 'es' === $GLOBALS['q_config']['language'])
                						{
									printf('<p style="margin-top: 50px;text-align: center;"><a class="conoce-mas no-decoration bold" href="'. get_permalink( get_the_ID() ) . '">LEER MÁS </a></p>');
									}

							  ?>
						</p>	
					</div>
				</div>
				<div class ="col-xs-12 col-sm-6 col-md-6 col-sm-pull-6 col-md-pull-6 img-single-post-custom padding-0">
					<?php 
						if ( has_post_thumbnail() ) { 
							$image_id = get_post_thumbnail_id();
							$image_url = wp_get_attachment_image_src($image_id,'large', true);
							echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="">';
						} 
					?>
				</div>
			</div>
		</div>							
		<?php
			// if multiple of 2 close div and open a new div

			}
			$i++;
			endwhile;
		?>
		<!-- END: Noticias-->
			
		
</div>
<style type="text/css">
	
	.thumnails-img .img-responsive{width: 100%;}
</style>


<?php get_footer(); ?>
