<?php
    require_once 'clases/app.php';
    switch ($_REQUEST["filtro"]) {
        case "todos":
            $list=App::ObtenerRegistros("todas");
            break;
        case "rango":
            $list=App::ObtenerRegistros("fecha",$_REQUEST["from"],$_REQUEST["to"]);
            break;
        case "por_area":
            $list=App::Obtener_PorArea($_REQUEST["area"]);
            break;
        case "eliminar":
           $resultado=App::EliminarRegistro($_REQUEST["id_eliminar"]);   
           unset($resultado);
            break;
        default:
            break;
            } 
    ?>
    <table class="main-table table table-hover">
      <thead>
    <tr class="table-header">
                <td>
                    Registro #...
                </td>
                <td> Nombre</td>
                <td>Correo</td>
                <td>Teléfono</td>
                <td>
                    Fecha de captura (AAAA-MM-DD)
                </td>
                <td>Canal</td>
                 <td>
                    Eliminar registro
                </td>
   </tr>
    </thead>
    <?php
    if(!count($list)==0){
        $i=1;
        foreach($list as $item){
         ?>  
                <tr id="tr-<?php echo $item->id; ?>" class="item-list">
                   <td >
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $item->nombre; ?></td>
                    <td><?php echo $item->email; ?></td>
                    <td><?php echo $item->telefono; ?></td>
                    <td><?php echo $item->fecha; ?>
                    </td>
                    <td><?php echo $item->canal; ?></td><td>
                      <a class="item-encuesta" rel="eliminar" href="#" onclick="EliminarEncuesta(<?php echo $item->id; ?>)"><img class="boton-basura" src="images/basura.png" alt="Eliminar"/></a>
                      </td>
                </tr>
                <tr class="resultado">
                <td><span class="pregunta">Nombre: </span><span class="respuesta"><?php echo $item->nombre; ?></span></td>
            <td> <span class="pregunta">Empresa: </span><span class="respuesta"><?php echo $item->ciudad; ?></span></td>
             <td>   <span class="pregunta">Email: </span><span class="respuesta"><?php echo $item->correo; ?></span></td>
             <td>   <span class="pregunta">Áreas de interés: </span><span class="respuesta"><?php echo $item->telefono; ?></span></td>
            </tr>
          <?php
             $i++;
        }
    }
    else{ ?>
        <tr>
                <td>
                    No se encontraron resultados
                </td>
              </tr>
         
   <?php } ?>
    </table>
    