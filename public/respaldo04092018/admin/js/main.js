function MostrarEncuesta(id){$("#tr-"+id).next().toggle();}
function EliminarEncuesta(id){
    
     var eliminar = confirm("¿De verdad desea eliminar esta encuesta?");
     if(eliminar){
                 $.ajax({
                url:"filtrar.php",
                //data:"accion=Localidades&id_localidad="+$(this).val(),
                data: "filtro=eliminar&id_eliminar="+id,
                success: function(){
                    $('.trigger-false').trigger('click');                   
                }
            });
     } // si fue eliminado?
}
function BusquedaCombo(area){
                $.ajax({
                url:"filtrar.php",
                data: "filtro=por_area&area="+area.value,
                success: function(html){
                        $("#lista-encuestas").html(html);
                }
            });
}

function Excel(){
$("#datos_a_enviar").val( $("<div>").append( $("#exportar_a_excel").eq(0).clone()).html());
     $("#FormularioExportacion").submit();

}
$(document).ready(function(){
 
                          $(".chzn-select").chosen();
 
   var dates = $( "#from, #to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
                        dateFormat:     "yy-mm-dd",
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
    $(".filtro").click(function(){
    $(".filtro").removeClass('selected');
    $(this).addClass("selected"); 
        if($(this).attr("rel")==="rango"){
            var from=$(this).parent().parent().find("input#from").val();
            var to=$(this).parent().parent().find("input#to").val();
            $.ajax({
                url:"filtrar.php",
                data: "filtro="+$(this).attr("rel")+"&from="+from+"&to="+to,
                success: function(html){
                        $("#lista-encuestas").html(html);
                }
            });
        }
        else{
            $.ajax({
                url:"filtrar.php",
                data: "filtro="+$(this).attr("rel"),
                success: function(html){
                        $("#lista-encuestas").html(html);
                }
            });
        }
    });
$('.trigger-false').trigger('click');
});
