<!DOCTYPE html>
<?php
session_start();
if (isset ($_SESSION["datos_cliente"])){?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="css/jquery-ui-1.8.20.custom.css"/>
            <link href="../static/css/bootstrap.css" rel="stylesheet">
  
    <link rel="stylesheet" href="css/style.css"/>
                    <link rel="stylesheet" href="css/chosen.css" />
        <title> Senderos Mayakoba | Registros </title>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <img class="logo" src="../web/image/General/logo.png" alt="">
                <p> Registros Senderos Mayakoba</p>
            </div>
            <div id="panel-filtro">
                <table class="table ">
                    <tr>
                        <td>
                            <a class="filtro trigger-false" rel="todos" href="#">Todos los registros</a>
                        </td>
                    
                    </tr>
                 
                    <tr class="rango">
                        <td>
                            <span>VER REGISTROS DEL </span> <input type="text" id="from"/> <span>HASTA EL </span> <input type="text" id="to"/> &nbsp;<span><a class="filtro" rel="rango" href="#"><img class="boton-ir" src="images/ir.png"/></a> </span> 
                        </td>
                    </tr>
                </table>
            </div>
            <div id="boton-salir">
            <a href="logout.php"><img src="images/salir.png" alt="Salir"></a>
            </div>
            <div id="lista-encuestas">
            </div>
            
            
            <!-- Exportar a excel-->
            <?php include 'Exportar.php' ?>
                
        </div>
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/chosen.jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.20.custom.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        
    </body>
</html>
<?php 
}
else{
    echo '<script LANGUAGE="javascript">
                        document.location=("login.php");
                      </script>';
}?>
