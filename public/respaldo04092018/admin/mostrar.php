<?php
    require_once 'clases/app.php';
    $encuesta=App::ObtenerEncuesta($_REQUEST["id"]);
    $preguntas=array(
        "Nombre",
        "Correo electrónico",
        "Fecha de nacimiento",
        "¿Le resultó fácil comprar con nosotros?",
        "¿Tuvo algún problema?",
        "Si su respuesta fue si, ¿qué problema tuvo?",
        "¿Qué tan atractiva le pareció la tienda en línea de Zingara?",
        "¿El envío se realizó satisfactoriamente? ",
        "¿Está satisfecha con su compra? ",
        "¿Cómo ha sido el resultado del producto que compró",
        "¿Recibió una adecuada atención del equipo de customer service?",
        "Compraría con nosotros nuevamente",
        "¿Qué mejoras consideras que podríamos hacer?"
    );
    $i=0;
//    print_r($encuesta);
    foreach($encuesta as $respuesta){
        echo '
            <p>
                <span class="pregunta">'.$preguntas[$i].':</span><span class="respuesta"> '.$respuesta.'</span>
            </p>
            ';
        $i++;
    }