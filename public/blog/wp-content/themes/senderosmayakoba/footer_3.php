<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
	   <footer class="footer">
        <div class="container-fluid">
          <div class="row padding-tb-50">
            <div class="text-xs-center col-sm-6 left font-size-15 color-5 font-family-lato">
              <a href="https://www.facebook.com/senderosmayakoba/" target="_blank"><span class="margin-left-35 margin-0-ipad color-2 flaticon-facebook-app-logo"></span></a>
              <a href="https://twitter.com/SenderosMk" target="_blank"><span class="color-2 flaticon-twitter-logo-silhouette"></span></a>
              <a href="https://www.instagram.com/senderosmayakoba/" target="_blank"><span class="color-2 flaticon-instagram-logo"></span></a>


              <a href="https://es.pinterest.com/senderosmk/" target="_blank"><span class="color-2 flaticon-pinterest-logotype-circle"></span></a>


              <span class="margin-0 font-size-15 margin-left-20">/SENDEROSMAYAKOBA</span>
            </div>
            <div class="col-sm-6 text-center font-size-15 color-5 font-family-lato">
              <p class="margin-0 padding-top-11">TODOS LOS DERECHOS RESERVADOS &#174; SENDEROS CIUDAD MAYAKOBA</p>
            </div>
          </div>
        </div>
      </footer>
<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script src="http://www.senderosmayakoba.mx/popup-navigation/js/jquery-2.1.1.js"></script>
<script src="http://www.senderosmayakoba.mx/popup-navigation/js/main.js"></script> <!-- Resource jQuery -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script src="http://www.senderosmayakoba.mx/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.senderosmayakoba.mx/bower_components/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="http://www.senderosmayakoba.mx/static/js/main.js"></script>
<script src="http://www.senderosmayakoba.mx/static/js/countUp.js"></script>


<script type="text/javascript">

</script>

<script type="text/javascript">
  $(document).ready(function() {
    $(".fancybox").fancybox();
  });
</script>
<script type="text/javascript">
  $('.grid').masonry({
  // options
  itemSelector: '.grid-item'
  });
</script>

<script type="text/javascript">
  var home_options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : '' 
};
  var home_options1 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : 'km' 
};
  var home_options2 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : 'm&sup2;' 
};
  var home_options3 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : '' 
};

var demo = new CountUp("home-count", 0, 20000, 0, 2.5, home_options);
demo.start();
var demo = new CountUp("home-count1", 0, 5, 0, 7.5, home_options1);
demo.start();
var demo = new CountUp("home-count2", 0, 130000, 0, 2.5, home_options2);
demo.start();
var demo = new CountUp("home-count3", 0, 20, 0, 5.0, home_options3);
demo.start();
</script>
<script type="text/javascript">
  var ciudad_mayakoba_options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : '%' 
};
  var ciudad_mayakoba_options1 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : '' 
};
  var ciudad_mayakoba_options2 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : ' ha' 
};
  var ciudad_mayakoba_options3 = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '', 
  suffix : ' km' 
};
var demo = new CountUp("ciudad-mayakoba-count", 0, 75, 0, 6.5, ciudad_mayakoba_options);
demo.start();
var demo = new CountUp("ciudad-mayakoba-count1", 0, 95, 0, 6.5, ciudad_mayakoba_options1);
demo.start();
var demo = new CountUp("ciudad-mayakoba-count2", 0, 25, 0, 6.5, ciudad_mayakoba_options2);
demo.start();
var demo = new CountUp("ciudad-mayakoba-count3", 0, 40, 0, 6.5, ciudad_mayakoba_options3);
demo.start();
</script>

<script type="text/javascript" src="http://senderosmayakoba.mx/static/js/validate.js" /></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#news').validate({

            submitHandler: function(form) {
                $.ajax("http://www.senderosmayakoba.mx/newsletter.php",{
                data:$("#news").serialize(),
                type:'post',
                cache:false,
                beforeSend: function(result){
                },
                success: function(result){

                    console.log(result);
                    $("#news").each(function(){
                        this.reset();
                    });
                    $( ".news-popup" ).trigger( "click" );
                },
                error:function(result){
                }
            })
        }});
        $('#news-menu').validate({
            submitHandler: function(form) {
                $.ajax("http://www.senderosmayakoba.mx/newsletter.php",{
                data:$("#news-menu").serialize(),
                type:'post',
                cache:false,
                beforeSend: function(result){
                },

                success: function(result){
                    alert("1")
                    console.log(result);
                    $("#news").each(function(){
                        this.reset();
                    });
                    $( ".news-popup" ).trigger( "click" );
                },
                error:function(result){
                }
            })
        }});
    });
</script>
<script type="text/javascript">
        $("select.mapa").change(function (){
            var option=document.getElementById("mapas").value;
            $(".all-select").removeClass("show");
            $(option).addClass( "show" );
        });
</script>

<script type="text/javascript">
  var plan = JSON.parse('{{plan|json_encode|raw}}');
  $('.images-icons a.images-icons-content').click(function (){
    $('.all-icons').fadeIn('slow','swing');
    $('.images-icons').fadeOut(0,'swing');
    var id = $(this).attr('data-id');
    $('.array-image').attr("src",plan[id]['url']);
    document.getElementById("description").innerHTML = plan[id]['des'];
    });
</script>

<script type="text/javascript">
  $('.position-relative a.cerrar').click(function (){
    $('.all-icons').fadeOut(0,'swing');
    $('.images-icons').fadeIn('slow','swing');
  });
</script>

<script type="text/javascript">
  var images = JSON.parse('{{images|json_encode|raw}}');
  $('.images-show a.point').click(function (){
    $('.images-content-show').fadeIn('slow','swing');
    $('.images-content').fadeOut(0,'swing');
    var id = $(this).attr('data-id');
    $('.array-image').attr("src",images[id]['url']);
    });
</script>

<script type="text/javascript">
  $('a.cerrar2').click(function (){
    $('.images-content-show').fadeOut(0,'swing');
    $('.images-content').fadeIn('slow','swing');
  });
</script>

<script type="text/javascript">
  $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
  })
</script>


<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
     http://chromium.org/developers/how-tos/chrome-frame-getting-started -->
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<!-- Asynchronous google analytics; this is the official snippet.
   Replace UA-XXXXXX-XX with your site's ID and uncomment to enable.
   
<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXXX-XX']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
-->
  <?php wp_footer(); ?>
</body>
</html>