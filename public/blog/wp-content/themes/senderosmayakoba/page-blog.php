<?php
/*
Template Name: Page-blog
*/


get_header();
$language = $GLOBALS['q_config']['language'];
?>
		<div class="container-fluid" style="padding-top: 100px;">
		  	<!--<div class="row">
				  <img class="img-responsive width-100" src="http://www.senderosmayakoba.mx/images/home/banner-home-senderos-ciudad-mayakoba.jpg">  
		  	</div>-->
		  	<?php
				if ($language == "es"){
			?>
		  	<div class="container">
		  		<div class="row">
		  			<div class="col-sm-12 text-center padding-tb-60">
		  				<h1 class="font-family-lato italic font-size-45 color-2 margin-top-0">BLOG</h1>
		  				<p class="font-family-lato font-size-30 margin-0 color-3"></p>
		  				<p class="font-family-lato font-size-30 margin-0 color-3">No te pierdas nuestras últimas noticias y útiles consejos para disfrutar de los alrededores de lo que será tu futuro hogar en la Riviera Maya.</p>
		  			</div>
		  		</div>
		  	</div>
		  	<?php
			  }
			?>
			<?php
			if ($language == "en"){
			?>
			<div class="container">
		  		<div class="row">
		  			<div class="col-sm-12 text-center padding-tb-60">
		  				<h1 class="font-family-lato italic font-size-45 color-2 margin-top-0">BLOG</h1>
		  				<p class="font-family-lato font-size-30 margin-0 color-3"></p>
		  				<p class="font-family-lato font-size-30 margin-0 color-3">Don't miss our latest news and helpful tips to enjoy the surroundings of what will be your future home in the Riviera Maya.</p>
		  			</div>
		  		</div>
		  	</div>
		  	<?php
			  }
			?>
		<!-- BEGIN: Noticias -->
		<?php
			$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

			$args = array(
			    'post_type' => 'post',
			    'post_status' => 'publish',
			    'posts_per_page' => 30,
			    'paged' => $paged,
			);
			$i = 1;
			$the_query = new WP_Query($args);
			$flat_wp_first_post = 1;
			$paginator_page_number = $the_query->max_num_pages;
		?>
		<div class = "row">
			<section class = "col-md-12">
				<div class = "row content-post">
						<?php
							while ( $the_query->have_posts() ) : $the_query->the_post();
						?>
						<div class ="col-sm-4 padding-medium min-height-blog-responsive
						min-height-blog-ipad " style="min-height:650px">
							<div class ="row background7">
								<div class ="col-xs-12 col-sm-12 col-md-12 img-single-post-custom text-center">
									<?php 
										if ( has_post_thumbnail() ) { 
											$image_id = get_post_thumbnail_id();
											$image_url = wp_get_attachment_image_src($image_id,'large', true);
											echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="">';
										} 
									?>
								</div>
								<div class = "col-xs-12 col-sm-12 col-md-12 text-center">
									<div class = "last-post text-blog font-family-lato color-3" style="font-size: 17px">
										<a href="<?php the_permalink(); ?>" rel="bookmark"><h3 class = "font-family-lato italic color-2 margin-top-0" style="font-size:23px!important;margin-top: 20px;"><?php the_title(); ?></h3></a>
										<p class = " abajo ">
											<?php the_excerpt(); ?>
										</p>	
									</div>
								</div>
							</div>
						</div>
					<?php
						// if multiple of 3 close div and open a new div
						endwhile;
					?>
				</div>
			</section>
		</div>
		<!-- END: Noticias-->
	</div>
</div>
<?php get_footer(); ?>