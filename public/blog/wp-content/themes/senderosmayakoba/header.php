<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
// echo site_url();
// echo $GLOBALS['q_config']['language'] ;
// variable = $GLOBALS['q_config']['language'] ;
$language = $GLOBALS['q_config']['language'];
?>



<!-- if (variable == "es"){
  menu en español
}
elseif (variable == "en"){
  menu en ingles
} -->

<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head id="www-sitename-com" data-template-set="html5-reset">

  <meta charset="utf-8">
    <link rel="icon" type="image/png" sizes="32x32" href="http://www.senderosmayakoba.mx/images/general/favicon.ico">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>Senderos Mayakoba</title>
  
  <meta name="title" content="Senderos" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <!--Always force latest IE rendering engine (even in intranet) & Chrome Frame-->
  
  <meta name="google-site-verification" content="" />
  <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
  
  <meta name="author" content="Punk e-Marketing &amp; Consulting" />
  <meta name="Copyright" content="" />
  
  <!--  Mobile Viewport Fix
  http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
  device-width : Occupy full width of the screen in its current orientation
  initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
  maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
  -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />


  <!-- Iconifier might be helpful for generating favicons and touch icons: http://iconifier.net -->
  <link rel="shortcut icon" href="http://www.senderosmayakoba.mx/static/img/favicon.ico" />
  <!-- This is the traditional favicon.
     - size: 16x16 or 32x32
     - transparency is OK -->
     
  <link rel="apple-touch-icon" href="{{BASE_URL}}static/img/apple-touch-icon.png" />
  <!-- The is the icon for iOS's Web Clip and other things.
     - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for retina display (IMHO, just go ahead and use the biggest one)
     - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
     - Transparency is not recommended (iOS will put a black BG behind the icon) -->
  
  <!-- concatenate and minify for production -->
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx/popup-navigation/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx/popup-navigation/css/style.css"> <!-- Resource style -->
  <link href="http://www.senderosmayakoba.mx/static/css/reset.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/static/css/style.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/static/css/font/flaticon.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx/bower_components/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />

    <!--script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "Brand Name",
    "url" : "http://www.mysite.mx",
    "logo" : "http://www.sitename.mx/static/website/image/logo-gulf.png",
    "sameAs" : [
      "https://www.facebook.com/sitename..",
      "https://twitter.com/sitename..",
      "https://www.instagram.com/sitename.."
    ],
    "contactPoint" : [{
      "@type" : "ContactPoint",
      "telephone" : "+52-800-000-0000",
      "contactType" : "customer service",
      "contactOption" : "TollFree",
      "areaServed" : "MX"
      }]
  }
  </script-->
  <!-- This is an un-minified, complete version of Modernizr. 
     Before you move to production, you should generate a custom build that only has the detects you need. -->

  <script src="http://www.senderosmayakoba.mx/bower_components/modernizr/modernizr.js"></script>
 


  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>
  
  
  <script src="http://www.senderosmayakoba.mx/popup-navigation/js/modernizr.js"></script> <!-- Modernizr -->
  





  <!-- Application-specific meta tags -->
  <!-- Windows 8 -->
  <meta name="application-name" content="" /> 
  <meta name="msapplication-TileColor" content="" /> 
  <meta name="msapplication-TileImage" content="" />
  <!-- Twitter -->
  <meta name="twitter:card" content="">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:url" content="">
  <!-- Facebook -->
  <meta property="og:title" content="" />
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
  <style type="text/css">
    .grid-item { width: 425px; }
    .grid-item--width2 { width: 400px; }
  </style>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122678839-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122678839-1');
</script>
	<link rel="stylesheet" href="http://www.senderosmayakoba.mx/assets/css/estilos-index.css">
  <style>
    .navbar {
    background: url(http://www.senderosmayakoba.mx/assets/frontend/index/img/fondo-index.jpg);
    padding: 0;
    margin: 0;
}

.fixed-c {
    position: fixed;
    top:0; left:0;
    width: 100%; }

    button.contact-button-link.show-hide-contact-bar
    {
        display:none; 
    }
    
    .box-footers a
    {
        color:white;
    }

    .cd-header
    {
      display: none;
    }

  </style>
</head>

<body class="{% block bodyclass %}{% endblock %}">

<div class="transp">
	<nav class="navbar navbar-default navbar-fixed-top shrink fixed-c">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			  	</button>

			 <div class="caja-logo">
                  <a href="http://www.senderosmayakoba.mx"><img src="http://www.senderosmayakoba.mx/assets/frontend/index/img/logotipo.png" class="logo-publico"></a>
            </div>

			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  	<ul class="nav navbar-nav pull-right">
			    	<li><a href="/">INICIO</a></li>
			    	<li><a href="/desarrollos">DESARROLLOS</a></li>
			     	<li><a href="/amenidades">AMENIDADES</a></li>      
			     	<li><a href="/ubicacion">UBICACIÓN</a></li>        
			    	<li><a href="/ciudad-mayakoba">CIUDAD MAYAKOBA</a></li>
			    	<li><a href="/galeria">GALERÍA</a></li>
			    	<li><a href="/blog">BLOG</a></li>
			    	<li><a href="/contacto" class="activo">CONTACTO</a></li>
			    </ul>
			</div>
		</div>
	</nav>
</div>

