<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<?php
/*
Template Name: Page-blog
*/


get_header();
?>

<style>
	
/* The CSS */
select {
    padding:5px;
    margin: 0;
    border-radius:0px;
    background: #f8f8f8;
    border:none;
    outline:none;
    display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
}

/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
    select {padding-right:18px}
}

label.selecstyle {position:relative}
label.selecstyle:after {
    font:11px "Consolas", monospace;
    color:#fff;
    right:8px; top:2px;
    padding:0 0 2px;
    position:absolute;
    pointer-events:none;
}

label.selecstyle:before {
  content: '';
  right: 0px;
  top: -14px;
  width: 30px;
  height: 30px;
  background: #6facc4;
  position: absolute;
  pointer-events: none;
  display: block;
}
</style>
<div class = "container">
	<div class="row">
		<section class="col-md-10 col-md-offset-1">
			<div class = "row">
				<div class = "col-xs-12 text-center">
					<h1 class = "title montserrat color3">Noticias</h1>
					<p class = "title-caption text-center mancha-texto color4">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id neque at aliquid eaque odio libero enim, repudiandae nihil eum eligendi repellendus cumque expedita placeat et praesentium beatae illum nesciunt! Nesciunt.
					</p>
				</div>
			</div>   
		</section>
	</div>
</div>
<div class = "container-fluid thumnails-img">
	<div class = "container">
		<!-- BEGIN: Noticias -->
		<div class="container-fluid">
  	<div class="row">
		  <img class="img-responsive width-100" src="{{BASE_URL}}/images/home/banner-home-senderos-ciudad-mayakoba.jpg">  
  	</div>
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12 text-center padding-tb-60">
  				<h1 class="font-family-lato italic font-size-45 color-2 margin-top-0">BLOG</h1>
  				<p class="font-family-lato font-size-30 margin-0 color-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
  				<p class="font-family-lato font-size-30 margin-0 color-3">tempor incididunt ut labore et dolore magna aliqua.</p>
  			</div>
  		</div>
  	</div>
    <div class="row">
      
    </div>
		<?php
			$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

		?>
		<div class = "row">
			<section class = "col-md-12">
				<div class = "row content-post">
						<?php
							$i = 1;
							while (  have_posts() ) : the_post();
						?>
						<div class ="col-sm-6 padding-medium">
							<div class ="row background7">
								<div class ="col-xs-5 col-sm-5 col-md-5 img-single-post-custom">
									<?php 
										if ( has_post_thumbnail() ) { 
											$image_id = get_post_thumbnail_id();
											$image_url = wp_get_attachment_image_src($image_id,'large', true);
											echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="">';
										}
									?>
								</div>
								<div class = "col-xs-7 col-sm-7 col-md-7">
									<div class = "last-post">
										<h3 class = "title open-sans italic color3"><?php the_title(); ?></h3>
										<p class = "text-blog">
											<?php the_excerpt(); ?>
										</p>	
									</div>
								</div>
							</div>
						</div>
					<?php
						// if multiple of 3 close div and open a new div
						if(( ( $i ) % 2 == 0) &&  $i != wp_count_posts() ) {
							echo '</div><div class = "row content-post">';
						}
            			$i++;
						endwhile;
					?>
				</div>
			</section>
		</div>
		<!-- END: Noticias-->
	</div>
</div>
<?php get_footer(); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>