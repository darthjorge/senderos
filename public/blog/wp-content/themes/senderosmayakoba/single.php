<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
// echo site_url();
// echo $GLOBALS['q_config']['language'] ;
// variable = $GLOBALS['q_config']['language'] ;
$language = $GLOBALS['q_config']['language'];
?>



<!-- if (variable == "es"){
  menu en español
}
elseif (variable == "en"){
  menu en ingles
} -->

<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head id="www-sitename-com" data-template-set="html5-reset">

  <meta charset="utf-8">
    <link rel="icon" type="image/png" sizes="32x32" href="http://www.senderosmayakoba.mx/images/general/favicon.ico">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>Senderos Mayakoba</title>
  
  <meta name="title" content="Senderos" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <!--Always force latest IE rendering engine (even in intranet) & Chrome Frame-->
  
  <meta name="google-site-verification" content="" />
  <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
  
  <meta name="author" content="Punk e-Marketing &amp; Consulting" />
  <meta name="Copyright" content="" />
  
  <!--  Mobile Viewport Fix
  http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
  device-width : Occupy full width of the screen in its current orientation
  initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
  maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
  -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />


  <!-- Iconifier might be helpful for generating favicons and touch icons: http://iconifier.net -->
  <link rel="shortcut icon" href="http://www.senderosmayakoba.mx/static/img/favicon.ico" />
  <!-- This is the traditional favicon.
     - size: 16x16 or 32x32
     - transparency is OK -->
     
  <link rel="apple-touch-icon" href="{{BASE_URL}}static/img/apple-touch-icon.png" />
  <!-- The is the icon for iOS's Web Clip and other things.
     - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for retina display (IMHO, just go ahead and use the biggest one)
     - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
     - Transparency is not recommended (iOS will put a black BG behind the icon) -->
  
  <!-- concatenate and minify for production -->
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx/popup-navigation/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx/popup-navigation/css/style.css"> <!-- Resource style -->
  <link href="http://www.senderosmayakoba.mx/static/css/reset.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/static/css/style.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/static/css/font/flaticon.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/static/css/font1/flaticon.css" rel="stylesheet">
  <link href="http://www.senderosmayakoba.mx/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <link rel="stylesheet" href="http://www.senderosmayakoba.mx//bower_components/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />

    <!--script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "Brand Name",
    "url" : "http://www.mysite.mx",
    "logo" : "http://www.sitename.mx/static/website/image/logo-gulf.png",
    "sameAs" : [
      "https://www.facebook.com/sitename..",
      "https://twitter.com/sitename..",
      "https://www.instagram.com/sitename.."
    ],
    "contactPoint" : [{
      "@type" : "ContactPoint",
      "telephone" : "+52-800-000-0000",
      "contactType" : "customer service",
      "contactOption" : "TollFree",
      "areaServed" : "MX"
      }]
  }
  </script-->
  <!-- This is an un-minified, complete version of Modernizr. 
     Before you move to production, you should generate a custom build that only has the detects you need. -->

  <script src="http://www.senderosmayakoba.mx/bower_components/modernizr/modernizr.js"></script>
 


  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>

  
  <script src="http://www.senderosmayakoba.mx/popup-navigation/js/modernizr.js"></script> <!-- Modernizr -->






  <!-- Application-specific meta tags -->
  <!-- Windows 8 -->
  <meta name="application-name" content="" /> 
  <meta name="msapplication-TileColor" content="" /> 
  <meta name="msapplication-TileImage" content="" />
  <!-- Twitter -->
  <meta name="twitter:card" content="">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:url" content="">
  <!-- Facebook -->
  <meta property="og:title" content="" />
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
  <style type="text/css">
    .grid-item { width: 425px; }
    .grid-item--width2 { width: 400px; }
  </style>
</head>

<body class="{% block bodyclass %}{% endblock %}">
<?php
if ($language == "es"){
?>
<header class="cd-header" style="background-color:rgba(0,0,0,0.6)!important;">
    <div class="cd-logo">
      <a href="http://www.senderosmayakoba.mx/index.php?lang=es"><img class="max-width-50 img-responsive" src="http://www.senderosmayakoba.mx/images/general/logotipo-senderos-ciudad-mayakoba.png" alt="Logo"></a>
    </div>

    <nav>
      <ul class="cd-secondary-nav">
        <a href="http://www.senderosmayakoba.mx/index.php/contacto?lang=es#form"><button type="submit" class="agendar-button button-hover">AGENDAR CITA</button></a>
      </ul>
    </nav> <!-- cd-nav -->

    <a class="cd-primary-nav-trigger" href="#0">
      <span class="cd-menu-text">MENÚ</span><span class="cd-menu-icon"></span>
    </a> <!-- cd-primary-nav-trigger -->
  </header>
  <nav>
    <ul class="cd-primary-nav menu-a">
      <?php
        if ($language == "es"){
      ?>
          <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=es" style="color:#49B2A4;">ESP</a><span class="color-1">/</span><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=en">ENG</a></li>
      <?php
        }
      ?>
      
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=es">INICIO</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/senderos-mayakoba?lang=es">SENDEROS</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/senderos-norte?lang=es">SENDEROS NORTE</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/galeria?lang=es">GALERÍA</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/ciudad-mayakoba?lang=es">CIUDAD MAYAKOBA</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/?lang=es/blog?lang=es">NOTICIAS</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/contacto?lang=es">CONTACTO</a></li>
      <hr class="width-33" style="margin-left:33.5% ">
      <p class="font-size-25 font-family-lato color-1">SUSCRÍBETE A NUESTRO NEWSLETTER:</p>

      <form class="margin-bottom-20">
            <input type="text" required="" class="width-50-responsive enviar-button-menu width-23 margin-bottom-20-ipad" name="" placeholder="TU E-MAIL" required>
          
            <input type="submit" required="" class="width-50-responsive enviar-button-menu width-10" value="ENVIAR">
            </form>

      <a class="padding-0" href="https://www.facebook.com/senderosmayakoba/" target="_blank"><span class="color-1 font-size-25  flaticon-facebook-app-logo"></span></a>
      <a class="padding-0" href="https://twitter.com/SenderosMk" target="_blank"><span class="color-1 font-size-25  flaticon-twitter-logo-silhouette"></span></a>
      <a class="padding-0" href="https://www.instagram.com/senderosmayakoba/" target="_blank"><span class="color-1 font-size-25  flaticon-instagram-logo"></span></a>
      <a class="padding-0" href="https://es.pinterest.com/senderosmk/" target="_blank"><span class="color-1 font-size-25  flaticon-pinterest-logotype-circle"></span></a>
      <span class="color-1 font-size-25  font-family-lato margin-left-20"> /SenderosMayakoba</span>
    </ul>
  </nav>
<?php
  }
?>
<?php
if ($language == "en"){
?>
<header class="cd-header">
    <div class="cd-logo"><a href="http://www.senderosmayakoba.mx/index.php?lang=en"><img class="max-width-50 img-responsive" src="http://www.senderosmayakoba.mx/images/general/logotipo-senderos-ciudad-mayakoba.png" alt="Logo"></a></div>

    <nav>
      <ul class="cd-secondary-nav">
        <a href="http://www.senderosmayakoba.mx/index.php/contacto?lang=en#form"><button type="submit" class="agendar-button button-hover">SCHEDULE APPOINTMENT</button></a>
      </ul>
    </nav> <!-- cd-nav -->

    <a class="cd-primary-nav-trigger" href="#0">
      <span class="cd-menu-text">MENU</span><span class="cd-menu-icon"></span>
    </a> <!-- cd-primary-nav-trigger -->
  </header>
  <nav>
    <ul class="cd-primary-nav menu-a">
      
      <?php
        if ($language == "en"){
      ?>
          <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=es">ESP</a><span class="color-1">/</span><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=en" style="color:#49B2A4;">ENG</a></li>
      <?php
        }
      ?>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php?lang=en">HOME</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/senderos-mayakoba?lang=en">SENDEROS</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/senderos-norte?lang=en">SENDEROS NORTE</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/galeria?lang=en">GALLERY</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/ciudad-mayakoba?lang=en">CIUDAD MAYAKOBA</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/blog?lang=en">NEWS</a></li>
      <li><a class="font-size-25 font-family-lato color-1" href="http://www.senderosmayakoba.mx/index.php/contacto?lang=en">CONTACT</a></li>
      <hr class="width-33 text-center" style="margin-left: 33.5%">
      
      <p class="font-size-25 font-family-lato color-1">SUSCRIBE TO OUR NEWSLETTER:</p>

      <form class="margin-bottom-20">
        <input type="text" required="" class="width-50-responsive enviar-button-menu width-23 margin-bottom-20-ipad" name="" placeholder="TU E-MAIL" required>
      
        <input type="submit" required="" class="width-50-responsive enviar-button-menu width-10" value="SEND">
      </form>

      <a class="padding-0" href="https://www.facebook.com/senderosmayakoba/" target="_blank"><span class="color-1 font-size-25  flaticon-facebook-app-logo"></span></a>
      <a class="padding-0" href="https://twitter.com/SenderosMk" target="_blank"><span class="color-1 font-size-25  flaticon-twitter-logo-silhouette"></span></a>
      <a class="padding-0" href="https://www.instagram.com/senderosmayakoba/" target="_blank"><span class="color-1 font-size-25  flaticon-instagram-logo"></span></a>
      <a class="padding-0" href="https://es.pinterest.com/senderosmk/" target="_blank"><span class="color-1 font-size-25  flaticon-pinterest-logotype-circle"></span></a>
      <span class="color-1 font-size-25  font-family-lato margin-left-20"> /SenderosMayakoba</span>
    </ul>
  </nav>
<?php
  }
?>


<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<style>
	.text-blog img{
		width: 100%;
		height: auto;
		padding-bottom: 30px;
	}
	.single-content-text{
		padding: 0px;
	}
	.font-size-45{font-size: 45px}
	.color-2{color:#49B2A4;}
	.italic{font-style: italic;}
	.font-size-30{font-size: 21px}
	.color-3{color:#868686;}
	.margin-0{margin:0 }
</style>
<?php while ( have_posts() ) : the_post(); ?>
<div class = "single-post-noticia">
	<div class = "container">
		<div class="row height-blog" style="height: 160px">
			
		</div>
		<div class="row">
			<section class="col-md-10 col-md-offset-1">
				<div class = "row">
					<div class = "col-xs-12 text-center">
						<h1 class = "font-family-lato italic font-size-45 color-2 margin-top-0 font-size-30-ipad"><?php  the_title(); ?></h1>
					</div>
				</div>
				<div class = "row">
					<div class = "col-md-12 text-center img-single-content">
						<?php 
							/*if ( has_post_thumbnail() ) { 
								$image_id = get_post_thumbnail_id();
								$image_url = wp_get_attachment_image_src($image_id,'large', true);
								echo '<img class = "img-responsive" src="'. $image_url[0] .'" alt="'. $image_url[1] .'">';
							} */
						?>
					</div>
				</div>
				<div class = "row single-content-text font-family-lato font-size-30 margin-0 color-3">
					<div class = "col-md-12 text-blog font-size-18-responsive" style="font-size:17px">
						<?php
							the_content();
						?>
					</div>
				</div>
   
			</section>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>