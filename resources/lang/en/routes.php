<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes Language Lines
    |--------------------------------------------------------------------------
    */

	'profile'              => 'profile',
	'projects'             => 'projects',
	'services'             => 'services',
	'contact'   		   => 'contact',
	'privacy'			   => 'privacy'
   
];