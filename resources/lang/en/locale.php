<?php 

return [

"frontend.services.title" 			=> "Services",
"frontend.contact.title" 			=> "Contact",
"frontend.contact.label.name" 		=> "Name",
"frontend.contact.label.phone" 		=> "Phone",
"frontend.contact.label.email" 		=> "Email",
"frontend.contact.label.message" 	=> "Message",
"frontend.contact.label.btn-send"	=> "Submit",
"frontend.page.project.label.title"	=> "Projects",
"frontend.header.links.profile"		=> "profile",
"frontend.header.links.projects"	=> "projects",
"frontend.header.links.services"	=> "services",
"frontend.header.links.contact"		=> "contact",
"frontend.label.privacy-policy"		=> "Privacy Policy."


];

?>