<?php 

return [

"frontend.services.title" 			=> "Servicios",
"frontend.contact.title" 			=> "Contacto",
"frontend.contact.label.name" 		=> "Nombre",
"frontend.contact.label.phone" 		=> "Teléfono",
"frontend.contact.label.email" 		=> "Correo",
"frontend.contact.label.message" 	=> "Mensaje",
"frontend.contact.label.btn-send"	=> "Enviar",
"frontend.page.project.label.title"	=> "Proyectos",
"frontend.header.links.profile"		=> "perfil",
"frontend.header.links.projects"	=> "proyectos",
"frontend.header.links.services"	=> "servicios",
"frontend.header.links.contact"		=> "contacto",
"frontend.label.privacy-policy"		=> "Política de privacidad."


];

?>