<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes Language Lines
    |--------------------------------------------------------------------------
    */

	'profile'              => 'perfil',
	'projects'             => 'proyectos',
	'services'             => 'servicios',
	'contact'   		   => 'contacto',
	'privacy'			   => 'privacidad'
  
];