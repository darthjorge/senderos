@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
    <div class="row text-center">
        <div class="col-md-4">
            <form method="post" action="{{url('/create/status')}}">
        <div class="form-group">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <label for="title">Lotes numero! :</label>
            <input type="text" class="form-control" name="name" placeholder="Nombre del status" />
        </div>

        <div class="form-group">
            <input type="text" class="form-control" name="description" placeholder="Descripcion del status" />
        </div>

        <button type="submit" class="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
</div>
@endsection