@extends('layouts.app')

@section('content')
<div class="container">

	@if(\Session::has('success'))
	        <div class="alert alert-success">
	            {{\Session::get('success')}}
	        </div>
    @endif
   
    <div class="row">
       <a href="{{url('/create/status')}}" class="btn btn-success">Crear Status para lote</a>
       <a href="{{url('/status')}}" class="btn btn-default">Todos los status disponibles</a>
    </div>

	<br />

    <table class="table table-striped">
        <thead>
            <tr>
              <td>ID</td>
              <td>Name</td>
              <td>Description</td>
              <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($status as $state)
            <tr>
                <td>{{$state->id}}</td>
                <td>{{$state->name}}</td>
                <td>{{$state->description}}</td>
                <td><a href="{{url('/edit/status', [$state->id]) }}">Edit</a></td>
                <td>Delete</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection