@extends("frontend/layout")

@section("contenedor-superior")
<link rel="stylesheet" href="{{ asset('assets/css/ciudad-mayakoba.css') }}">
<style>
.shadow-photo
{
    position: absolute;
    height: 100%;
    width: 100%;
    background: #00000073;
}

.mayakoba .container-images .s1 .text-custom
{
    z-index:3;
}

.shadow-photo2
{
    width: 100%;
    height: 100%;
    background: #131212a8;
    position: absolute;
    z-index: 1;
}

.text-custom
{
    z-index:2;
}

</style>
@endsection

@section('contenedor-principal')

<div class="container-fluid img-fondo">
	<div class="overlay-custom">
		<div class="col-md-12 text-center">
			<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/ciudad-mayakoba.png') }}" alt="">
			<p>Ciudad Mayakoba es la primer Comunidad Planeada Sustentable de la Riviera Maya, México. Comprometida con <br> la preservación de los ecosistemas y la calidad de vida de sus habitantes.
			</p>
		</div>
	</div>
	<div class="line">
		
	</div>
</div>
<div class="container-fluid section-last">
	<div class="col-md-6 section-i">
		<div class="box">
			<p class="padding10">
				El proyecto considera la menor afectación al entorno, promoviendo que el desarrollo urbano sea controlado e integrado al paisaje existente.
			</p>
			<p class="padding10">
				La construcción y planeación del proyecto fomenta la reducción de uso de automóvil, promoviendo así el caminar y el uso de bicicleta.
			</p>
			<div class="line-green"></div>
		</div>
	</div>
	<div class="col-md-6 section-d">
		<img src="{{ asset('assets/frontend/index/img/obra1.jpg') }}">
	</div>
</div>
<div class="container-fluid mayakoba">
	<div class="container-images">
		<h1>BENEFICIOS</h1>
		<p>Caracterizado por distintas tipologías de viviendas Ciudad Mayakoba <br> contempla además diferentes servicios.</p>
		<div class="s1" style="">
		    <div class="shadow-photo"></div>
			<div class="text-custom">
				<p>EDUCACIÓN</p>
				<p>UNIVERSIDADES <strong>/</strong> COLEGIOS</p>
			</div>
			<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/image1.jpg') }}">
		</div>
		<div class="s2">
			<div>
				<div class="text-custom">
					<p>SALUD</p>
					<p>HOSPITAL <strong>/</strong> CONSULTORIOS</p>
				</div>
				<div class="shadow-photo2"></div>
				<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/image2.jpg') }}">
			</div>
			<div>
				<div class="text-custom">
					<p>ARTE Y CULTURA</p>
					<p>HOSPITAL <strong>/</strong> CONSULTORIOS</p>
				</div>
				<div class="shadow-photo2"></div>
				<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/image3.jpg') }}">	
			</div>
		</div>
		<div class="s3">
			<div>
				<div class="text-custom">
					<p>SUSTENTABILIDAD</p>
				</div>
				<div class="shadow-photo2"></div>
				<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/image4.jpg') }}">
			</div>
			<div>
				<div class="text-custom">
					<p>DEPORTES</p>
				</div>
				<div class="shadow-photo2"></div>
				<img src="{{ asset('assets/frontend/ciudad-mayakoba/img/image5.jpg') }}">	
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<!--<div class="container-fluid padding0">
	<div class="line-b">
		
	</div>
</div>-->

<!-- <div class="white-space"></div> -->


@endsection

@section('contenedor-inferior')


@endsection