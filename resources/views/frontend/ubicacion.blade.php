@extends("frontend/layout")

@section("contenedor-superior")
<style>
	.transp
	{
		position: relative;
	}

	.navbar
{
	 
	background: url("/assets/frontend/index/img/fondo-index.jpg");
	padding: 0;
	margin: 0;
}

</style>
@endsection

@section('contenedor-principal')

<!-- <div class="container-fluid img-fondo3">
	
</div> -->
<div class="container-fluid img-fondo">

	</div>
<div class="overlay-title">
	<div class="col-md-12 text-center">
		<div class="title-square">
			<h1>VIVE LO MEJOR DE PLAYA DEL CARMEN</h1>
		</div>
	</div>
</div>

<section class="mar" data-parallax="scroll" data-image-src="/assets/frontend/ubicacion/mar.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="white-box">
					<p>Seguramente más de una vez te has despertado cansado de la rutina, harto del estrés de las grandes ciudades y por tu mente ha cruzado la idea de  “quiero vivir en la Riviera Maya”.</p>
					<hr>
				</div>
			</div>	
			<div class="col-md-4 text-right col-md-offset-3">
				<h2 class="super-title">
					¡Deja de pensarlo<br>
					y haz que se vuelva<br>
					<span>Realidad!</span>
				</h2>

			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="foto-box">
					<div class="mitad family-photo">
						
					</div>
					<div class="mitad txt-family">
						<P>Este paraíso de sol y playa es perfecto, no sólo como destino vacacional, si no también como el lugar perfecto para vivir con tu familia.</P>

						<p>Definitivamente una de las razones principales, la playa. A solo unos minutos de la comodidad de tu hogar, podrás encontrar playas reconocidas mundialmente por su belleza y mar azul turquesa que identifican a este rincón mexicano.
						</P>

						<hr>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="box-green"></div>
<section class="ubicacion-food">
	<div class="mitad">
		<p>Al encontrarnos en un destino multicultural, la Riviera Maya ofrece a residentes y visitantes una gran variedad culinaria que puede satisfacer todos los gustos, que va desde los más sencillos y exquisitos platillos yucatecos y mexicanos, hasta las más finas especialidades internacionales y cocina de autor.</p>
		<hr>
	</div>
	<div class="mitad tacos">
		
	</div>
</section>
<div class="box-green"></div>
<section class="two">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6">
							<img src="/assets/frontend/ubicacion/chichen.jpg" alt="chichen itza">	
						</div>
						<div class="col-md-6">
							<img src="/assets/frontend/ubicacion/ruinas.jpg" alt="ruinas">	
						</div>
					</div>
				</div>

			</div>
			<div class="col-md-4">
				
				<h3>Otra gran ventaja <br> de vivir en el paraíso</h3>

				<P>Es la cantidad de atractivos que puedes encontrar cercanos a ti. Como lo son las playa vírgenes, lagunas, cenotes, arrecifes, ruinas mayas y parques naturales, son actividades que como local no te puedes quedar sin conocer.</P>
			</div>
		</div>
	</div>
</section>
<!-- <div class="box-green"></div> -->
<!-- <img src="/assets/frontend/ubicacion/map.jpg" alt="map" class="cien"> -->

@endsection

@section('contenedor-inferior')

<script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>

@endsection
