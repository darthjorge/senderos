@extends("frontend/layout")

@section("contenedor-superior")

<link rel="stylesheet" href="{{ asset('assets/css/amenidades.css') }}">
@endsection

@section('contenedor-principal')

<!--<div class="container-fluid img-fondo">-->
<!--		<div class="div-bird"></div>-->
<!--		<div class="overlay-custom">-->
<!--    		<div class="col-md-12 text-center">-->
<!--    			<h1>Notice of Privacy</h1>-->
				
<!--			</div>-->
<!--		</div>-->
<!--		<div>-->
<!--			<p class="line-green"></p>-->
<!--		</div>-->
<!--	</div>-->

<div class="container-fluid img-fondo" style="height:auto;">
	<div class="col-md-12 text-center">
    	<h1>Notice of Privacy</h1>
	</div>
</div>

<div class="clearfix"></div>
<div class="container">
		
<h2>COMPREHENSIVE PRIVACY NOTICE NOVATERRA CARIBE, S.A.P.I. DE C.V.</h2>

<p>In compliance and adherence to that provided by the Federal Law of Personal Data Protection in Possession of Individuals and its Regulations we inform the following items:</p>

<h3>1.- PERSONAL DATA CONTROLLER</h3>

<p>For the protection of personal data of owners, NOVATERRA CARIBE, S.A.P.I. DE C.V., is responsible of the use and processing thereof, having as domicile the one located in Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, corner Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, same which is destined to hear and receive notices.</p>

<h3>2.- PRIMARY PURPOSES</h3>

<p>Your personal data shall be used to render you professional services for the promotion and sale of real estate, carry out the proceeding and answer of your request to exercise your “ARCO” (“Access, Rectification, Cancellation or Opposition”) rights or of your request to revoke your consent, to issue electronic invoice that may correspond to your consumption and for marketing, advertising or commercial prospecting purposes. For the above- mentioned purpose, it is necessary to obtain the following personal data: Name, Domicile, Nationality, Marital estatus, Tax ID, Email, Telephone Number, Age. The foregoing shall be obtained through our web page www.senderosmayakoba.mx, by email when you contact us through such means, directly in our facilities or points of sale.</p>

<h3>3.- SECONDARY PURPOSES</h3>

<p>NOVATERRA  CARIBE,  S.A.P.I.  DE  C.V.,  makes  of  your knowledge that the data may be used for the following secondary purposes: Remission of our promotions and discounts for marketing, advertising and commercial prospecting, Notify you about new services or products related with those already hired, that might be of your interest or inform you about changes thereof, Perform periodical evaluations of our products and services in order to improve their quality. For the foregoing purposes in the preceding paragraph, the owner of the data, the owner has 5 business days to state its negative, regardless of these days, at any time of the data´s processing, the owner may manifest his negative through (i) a written motion (addressed to our compliance area) signed by its owner or representative with authorities delivered in Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, corner Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, or (ii) through a written motion signed by its owner or representative with authorities, sent by email to the following address:  egarcia@novaterracaribe.com.mx.</p>

<h3>4.- DATA TRANSFERS</h3>

<p>The personal data that have been requested may be transferred to our affiliates or subsidiaries of Obrascon Huarte Lain Desarrollos, S.L., RLH Propierties, S.A.B. de C.V., MKB Real Estate, S.A. de C.V., and Huaribe S.A. de C.V., Caribeurbana S.A.P.I. de C.V.; such action shall be performed with the purpose of sending you our promotions and discounts for marketing, advertising or commercial prospecting purposes, to notify you about new services or products which are related with those already hired, which are considered that might be of your interest or inform you about the changes thereof, to carry out periodical evaluations of our products and services in order to improve the quality thereof, to carry out the proceeding and response of your request to exercise your “ARCO” rights or your request to revoke your consent and issue the electronic invoice that may correspond to your consumption. For such purpose, we require your consent in this privacy notice. In case we do not obtain an express opposition from your side in order for your personal data to be transferred in the manner and terms before described, we shall understand that you have granted your consent for such purpose.</p>

<h3>5.- LIMIT THE USE OR DISCLOSURE OF YOUR DATA</h3>

<p>In order for the owner to be able, at any time to limit the use and disclosure of his personal data, he may address to  our compliance area located in Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, corner Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710 or through written motion signed by the owner or his representative with authorities sending an email to the following address  egarcia@novaterracaribe.com.mx, or you may obtain more information in this regard on www.senderosmayakoba.mx. We inform you that there are other mechanisms to limit the use and disclosure of your data like Public Registry to Avoid Advertisement (“Registro Público para Evitar Publicidad” (REPEP)) of the Consumer Protection Agency (“PROFECO”) or the Public Registry of Users (“Registro Público de Usuarios” (REUS) of the National Commission for the Protection and Defense of Financial Users (“CONDUSEF”), which may be consulted for more information regarding your record on the web pages of each one of the agencies</p>




<h3>6.- WEB PAGES</h3>

<p>Through this medium, we inform you that on the following web page www.senderosmayakoba.mx; no use of cookies, web beacons or similar technologies is being made through which personal data are obtained. You are hereby informed that there are mechanisms to disable the use of these technologies consulting the tools and/or preferences of your Internet’s navigator</p>

<h3>7.- ARCO RIGHTS.</h3>

<p>We make of your knowledge that at all times you are entitled to access, rectify, cancel or oppose to the processing of your personal data, and also to the revocation of the consent you have provided to us, for this purpose, it is necessary that you address your request through a written motion (addressed to our compliance area) signed by his owner or representative with authorities delivered in Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, corner Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, or, (ii) through a written motion signed by his owner or representative with authorities sent by email to the following address:  egarcia@novaterracaribe.com.mx, or, (iii) through our web page www.senderosmayakoba.mx, whereby you will be provided with more information about the subject and your request will be handled adequately</p>
<p>a)	Necessary information and/or documents to exercise  your ARCO rights: Request to exercise the rights, Copy of the documents that evidence ownership or those of the representative, Contact data (Name, domicile, email and/or telephone) of the owner.<br>
b)	The request for the exercise of ARCO rights shall contain: Owner’s name and domicile, Data regarding the means by which he shall be in contact for notices of the request, Clear and precise description of the personal data with respect to which is sought to exercise any of the rights before mentioned, and Data that helps for the location of the data (Service acquired in NOVATERRA CARIBE, S.A.P.I. DE C.V.).</p>

<h3>8.- REVOCATION OF CONSENT</h3>
<p>Likewise, as we have the obligation to attend your ARCO rights requests, you may also request the revocation of the consent filling the form which can be found on the following electronic address www.senderosmayakoba.mx, or deliver a written request signed to our compliance area located in Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, corner Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, or by email to the following address  egarcia@novaterracaribe.com.mx, in the latter case, he should confirm the reception of the email by telephone in the number 52 (984)109 13 70 to provide you due attention, to carry out the revocation of your consent you shall enclose a copy of the information and documents set forth in subsections a) and b) of preceding number 7</p>

<h3>9.- CHANGES TO THE PRIVACY NOTICE</h3>

<p>This privacy notice may have modifications, changes or updates derived from new legal requirements; from our own needs for the services we offer; from our privacy practices; from changes in our business model, or for other causes. We commit ourselves to maintain you informed about the changes that this privacy notice might have, through its publication on our web page  www.senderosmayakoba.mx.</p>

<p>Last update: 6 de julio del 2018</p>

<div class="container text-center" style="
    margin-bottom: 15%;
"></div>

	</div>

@endsection

@section('contenedor-inferior')
<script>

	if( $(window).width() <= 1200)
	{

		var e1 = $(".section-4");
		e1.prev().detach().insertAfter(e1);

		var e2 = $(".section-8");
		e2.prev().detach().insertAfter(e2);
	}

</script>
@endsection
