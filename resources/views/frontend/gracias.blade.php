@extends("frontend/layout")

@section("contenedor-superior")

<link rel="stylesheet" href="{{ asset('assets/css/amenidades.css') }}">
<style>
*
{
    margin: 0;
}

html, body
{
    height: 100%;
}

footer
{
	margin-top: 0;
     position: relative !important;
    left: 0;
    bottom: 0;
    width: 100%;
    color: white;
    text-align: center;
}



</style>
@endsection

@section('contenedor-principal')
<div class="container-fluid img-fondo" style="height:auto;">
	<div class="col-md-12 text-center">
    	<h1>Gracias, su mensaje ha sido enviado.</h1>
		<a href="./" class="btn btn-success" style="margin: 10px;">
			Inicio
		</a>
	</div>
</div>
<div class="clearfix"></div>
<!-- <div class="container">
	<div class="container text-center" style="margin-bottom: 15%;">
		<a href="./" class="btn btn-success" style="margin: 10px;">
			Inicio
		</a>
	</div>
</div> -->

@endsection

@section('contenedor-inferior')
<script>

	

</script>
@endsection
