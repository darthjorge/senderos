<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" sizes="32x32"/>

	<title>Senderos Mayakoba</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/libs/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/estilos-index.css?version=1.0')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/contact-buttons.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
	
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet"> 
	@yield('contenedor-superior')
	<style>
	    .fixed-c {
    position: fixed;
    top:0; left:0;
    width: 100%; }
    
    
    
    button.contact-button-link.show-hide-contact-bar
    {
        display:none; 
    }
    
    .box-footers a
    {
        color:white;
    }
    
	</style>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M7KRKBS');</script>
  <!-- End Google Tag Manager -->
</head>

<body>

@include('frontend/includes/header')


@yield('contenedor-principal')


@include('frontend/includes/footer')

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title data-modal-title text-center" id="myModalLabel">Cargando...</h4>
      </div>
      <div class="modal-body text-justify">
        <p class="data-modal-content">Cargando...</p>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>

<!-- <script type="text/javascript" src="{{asset('assets/libs/jquery/js/jquery-3.js')}}"></script> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/parallax.min.js')}}"></script>

<script src="{{ asset('assets/js/slide.js') }}"></script>

<!-- <script src="{{ asset('assets/js/jquery.contact-buttons.js') }}"></script>
<script src="{{ asset('assets/js/demo.js') }}"></script> -->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "1520578998159058", // Facebook page ID
            whatsapp: "529843106242", // WhatsApp number
            call_to_action: "Contáctanos", // Call to action
            button_color: "#A8CE50", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->


<script>

    $("form").validate(
    {
        rules:
        {
            firstname: "required",
            lastname: "required",
            phone: "required",
            email: "required",
            interesting: "required",
            reason: "required",
            message: "required"
        },
        messages:
        {
            firstname: "Campo requerido",
            lastname: "Campo requerido",
            phone: "Campo requerido",
            email: "Campo requerido",
            interesting: "Campo requerido",
            reason: "Campo requerido",
            message: "Campo requerido",
            email: "Campo requerido"
        }
        
    });

</script>

<script type="text/javascript">
		
		$(document).on("scroll", function(){
			if
		  ($(document).scrollTop() > 100){
			  $(".navbar-default").addClass("shrink");
			}
			else
			{
				$(".navbar-default").removeClass("shrink");
			}
		});
		
		$(window).scroll(function(){
          var sticky = $('.navbar-default'),
              scroll = $(window).scrollTop();
        
          if (scroll >= 100) sticky.addClass('fixed-c');
          else sticky.removeClass('fixed-c');
        });
        
        

</script>

@yield('contenedor-inferior')


</body>


</html>