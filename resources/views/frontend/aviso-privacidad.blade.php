@extends("frontend/layout")

@section("contenedor-superior")

<link rel="stylesheet" href="{{ asset('assets/css/amenidades.css') }}">
@endsection

@section('contenedor-principal')

<!--<div class="container-fluid img-fondo">-->
<!--	<div class="div-bird"></div>-->
<!--	<div class="overlay-custom">-->
<!--		<div class="col-md-12 text-center">-->
<!--			<h1>Aviso de Privacidad</h1>-->
			
<!--		</div>-->
<!--	</div>-->
<!--	<div>-->
<!--		<p class="line-green"></p>-->
<!--	</div>-->
<!--</div>-->
<div class="container-fluid img-fondo" style="height:auto;">
	<div class="col-md-12 text-center">
    	<h1>Aviso de Privacidad</h1>
	</div>
</div>


	<div class="clearfix"></div>
	<div class="container">

		
		<h2>AVISO DE PRIVACIDAD SIMPLIFICADO NOVATERRA CARIBE, S.A.P.I. DE C.V.</h2>

<p>En cumplimiento y apego a lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares y su Reglamento damos a conocer los siguientes puntos:</p>

<h3>1.- RESPONSABLE DE DATOS PERSONALES.</h3>

<p>Para la protección de datos personales de titulares NOVATERRA    CARIBE, S.A.P.I.    DE    C.V.,    es responsable del uso y manejo de los mismos, teniendo como domicilio ubicado en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, mismo que está destinado para oír y recibir notificaciones.</p>

<h3>2.- FINALIDADES PRIMARIAS.</h3>

<p>Sus datos personales serán utilizados para prestar a usted servicios profesionales de promoción y venta de bienes inmuebles, realizar el trámite y respuesta de su solicitud de ejercicio de derechos “ARCO” o de su solicitud de revocación de consentimiento, para expedir la factura electrónica que corresponda a su consumo y para fines mercadotécnicos, publicitarios o de prospección comercial. Para lo cual es necesario recabar los siguientes datos personales: Nombre, Domicilio, Nacionalidad, Correo electrónico, Número telefónico, Edad. Lo anterior será obtenido a través de nuestra página de internet www.senderosmayakoba.mx, por correo electrónico cuándo así usted nos contacte por ese medio o directamente en nuestras instalaciones o puntos de promoción y venta.</p>

<h3>3.- FINALIDADES SECUNDARIAS.</h3>

<p>NOVATERRA CARIBE, S.A.P.I. DE C.V., hace de su conocimiento que los datos podrán ser usados para las siguientes finalidades secundarias: Envió de nuestras promociones y descuentos  para fines  mercadotécnicos, publicitarios o de prospección comercial, Notificarle sobre nuevos servicios o productos que tengan relación con los ya contratados, con los que se considere puedan ser de su interés o bien comunicarle cambios en los mismos, Realizar evaluaciones periódicas de nuestros productos y servicios a efecto de mejorar la calidad de los mismos. Para las finalidades establecidas en el párrafo anterior, el titular de los datos cuenta con 5 días hábiles para manifestar su negativa, independientemente de estos días en cualquier momento del tratamiento el titular puede manifestar su negativa a través de (i) un escrito libre (dirigido a nuestra área de cumplimiento) firmado por su titular o representante con facultades entregado en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710 o bien (ii) mediante escrito libre firmado por su titular o representante con facultades enviado por correo electrónico	a	la	siguiente	dirección: egarcia@novaterracaribe.com.mx </p>

<h3>4.- MECANISMOS	PARA CONOCER EL AVISO DE PRIVACIDAD INTEGRAL</h3>

<p>Puede consultar nuestro aviso de privacidad integral en ______________ </p>

<div class="container text-center" style="
    margin-bottom: 15%;
"><a href="./aviso-privacidad-completo" class="btn btn-success" style="
    margin: 10px;
">Aviso de privacidad completo</a>

<a href="./aviso-privacidad-ingles" class="btn btn-success" style="
    margin: 10px;
">Aviso de privacidad completo en inglés</a></div>

	</div>

@endsection

@section('contenedor-inferior')
<script>

	if( $(window).width() <= 1200)
	{

		var e1 = $(".section-4");
		e1.prev().detach().insertAfter(e1);

		var e2 = $(".section-8");
		e2.prev().detach().insertAfter(e2);
	}

</script>
@endsection
