<footer>
	<div class="fondo-footer">
		<div class="container-fluid">
		<div class="box-footer">
			<div class="row">
				<div class="col-md-4 box-footers">
					<a href="https://www.facebook.com/SenderosCiudadMayakoba/" target="_blank"><i class="fa fa-facebook-square"></i></a>
					<a href="https://www.instagram.com/senderoscdmk/" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="https://www.youtube.com/channel/UCu6dVjmbD_ZxbcKuGTMQsww" target="_blank"><i class="fa fa-youtube"></i></a>		
					<!-- <i class="fab fa-linkedin"></i> -->
					<a href="http://m.me/senderosciudadmayakoba" target="_blank"><i class="fab fa-facebook-messenger"></i></a>
					<span class="redes-sendero">/SenderosCiudadMayakoba</span>
				</div>
				<div class="col-md-4 box-footers text-center">
					<span><img src="{{asset('assets/frontend/index/img/logofooter.png')}}" class="logofooter"></span>
				</div>
				<div class="col-md-4 box-footers">
					<span class="footer-derechos">TODOS LOS DERECHOS RESERVADOS.</span> <br> 
					<span class="footer-derechos"><a href="/aviso-privacidad">Aviso de Privacidad</a></span>
				</div>
			</div>
		</div>
		</div>
	</div>

	

	<div class="box-green text-center">
		<a href="http://molcajetemkt.com" class="powered" target="_blank"><span>Powered By MolcajeteMkt</span></a>
	</div>

	
</footer>

    <script src="{{ asset('js/app.js') }}"></script>
