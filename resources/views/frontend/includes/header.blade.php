<div class="transp">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			  	</button>

			 <div class="caja-logo">
                  <a href="{{url('/')}}"><img src="{{ asset('assets/frontend/index/img/logotipo.png')}}" class="logo-publico"></a>
            </div>

			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  	<ul class="nav navbar-nav pull-right">
			    	<li><a href="/" >INICIO</a></li>
			    	<li><a href="{{url('/cotizador') }}">COTIZADOR</a></li>
			    	<li><a href="/desarrollos" >DESARROLLOS</a></li>
			     	<li><a href="/amenidades" >AMENIDADES</a></li>      
			     	<li><a href="/ubicacion">UBICACIÓN</a></li>        
			    	<li><a href="/ciudad-mayakoba" >CIUDAD MAYAKOBA</a></li>
			    	<li><a href="/galeria" >GALERÍA</a></li>
			    	<li><a href="/blog" >BLOG</a></li>
			    	<li><a href="/contacto" class="activo">CONTACTO</a></li>
			    </ul>
			</div>
		</div>
	</nav>
</div>