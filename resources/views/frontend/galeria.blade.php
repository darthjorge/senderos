@extends("frontend/layout")

@section("contenedor-superior")
<link rel="stylesheet" href="{{ asset('assets/css/contacto.css') }}">
<style>

/* override contacto.css*/

.badge-inner
{
    text-align:center;
}

/* override contacto.css*/

.navbar
{
	
	background: url("/assets/frontend/index/img/fondo-index.jpg");
}

.transp
{
	position: relative;
}

.gallery .box
{
	padding: 3%;
	height: 200px;
    background: white;
    text-align: center;
}

.gallery h1
{
	font-weight: 700;
}

.green-line
{
	background: #96cbab;
    padding: 10px;
}

.padding0
{
	padding: 0;
}

.gallery .gallery-images
{
	padding: 5% 10% 5% 10%;
    background: #d0d0d0;
}

.gallery .section1
{
	height: 500px;
    width: 100%;
    display: flex;
}

.gallery .section1 .container1
{
	width: 60%;
    height: 100%;
    padding-right: 10px;
}

/*.gallery .section1 .container1 img
{
	width: 100%;
    height: 100%;
    object-fit: cover;
}*/

.gallery .section1 .container2
{
	width: 40%;
    height: 100%;
}

.gallery .section1 .container2 .zoom-image2:first-child img
{
	width: 100%;
    /*height: 50%;*/
    
    /*object-fit: cover;*/
}

.gallery .section1 .container2 .zoom-image2:last-child img
{
	width: 100%;
    /*height: 50%;*/
    padding-top: 10px;
    /*object-fit: cover;*/
}

.gallery .section2
{
	display: flex;
    height: 450px;
    padding: 10px 0px;
}

.gallery .section2 .container1
{
	width: 40%;
}

.gallery .section2 .container1 img
{
	width: 100%;
    /*height: 100%;*/
    /*object-fit: cover;*/
    
}

.gallery .section2 .container2
{
    width: 80%;
}

.gallery .section2 .container2 img
{
	width: 100%;
    /*height: 100%;*/
    object-fit: cover;
    padding-left: 10px;
}


.gallery .section3
{
	display: flex;
    height: 250px;
    padding-bottom: 10px;
}

.gallery .section3 .container1, .gallery .section3 .container2, .gallery .section3 .container3
{
    width: 33.3%;
}

.gallery .section3 .container1 img, .gallery .section3 .container2 img, .gallery .section3 .container3 img
{
    width: 100%;
    /*height: 100%;*/
    object-fit: cover;
}

.gallery .section3 .container2 img
{
	padding: 0px 10px;
}

.gallery .section4
{
	height: 450px;
    display: flex;
}

.gallery .section4 .container1
{
	width: 66.7%;
    padding-right: 10px;
}

.gallery .section4 .container1 img
{
	width: 100%;
    /*height: 100%;*/
    object-fit: cover;
}

.gallery .section4 .container2
{
	width: 33.3%;
}

.gallery .section4 .container2 img
{
 	width: 100%;
    /*height: 100%;*/
    object-fit: cover;
}


.fondo-footer
{
	background-color: #d0d0d0;
	max-height: 350px;
}


* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
      -ms-box-sizing: border-box;
          box-sizing: border-box;
}

/*GROW*/

.zoom-image
{
	overflow: hidden;
	height: 100%;
}

.zoom-image2
{
	overflow: hidden;
	height: 50%;
}

.zoom-image img
{
	width: 100%;
	height: 100%;
	object-fit: cover;
   	transition: all 0.7s ease;
}

.zoom-image2 img
{
	width: 100%;
	height: 100%;
	object-fit: cover;
   	transition: all 0.7s ease;
}
 
.zoom-image img:hover
{
  	width: 112%;
  	height: 112%;
}
 
.zoom-image2 img:hover
{
 	width: 112%;
  	height: 112%;
    padding-bottom: 10px;
}

/* override */
body
{
    background:#d0d0d0;    
}

.img-fondo
{
    height:auto;
}

.gallery .box
{
    max-width:inherit;
    box-shadow:none;
}

.navbar
{
    margin-bottom:0;
}


.new-gallery img
{
    height:180px;
    width:100%;
    object-fit:cover;
    box-shadow: 3px 3px 3px transparent;
    transition:0.3s all;
    margin: 10px 0px;
}

.new-gallery img:hover
{
    box-shadow: 3px 3px 3px #757070;
    transform:scale(1.05);
}

.container-group-images
{
    padding:10px 0px;
}

.title-square h1
{
    border: 5px solid #414141;
}

/* override */

@media(max-width: 768px)
{
    .gallery .box
    {
        height: auto;
        padding: 10% 3%;
    }
}
    
</style>
@endsection

@section('contenedor-principal')

	<div class="container-fluid img-fondo">

	</div>
	<div class="container-fluid gallery padding0">
		<div class="box">
			<div class="header">
                <div class="title-square" style="padding:0;">
				    <h1>GALERÍA</h1>
                </div>
			</div>
		</div>
		<div class="green-line">	
		</div>
		<div class="gallery-images">
    		<div class="container new-gallery">
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image1.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image1.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image2.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image2.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image3.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image3.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image4.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image4.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image5.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image5.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image6.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image6.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image7.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image7.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image8.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image8.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image9.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image9.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image10.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image10.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image11.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image11.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image12.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image12.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image13.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image13.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image14.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image14.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image15.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image15.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image16.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image16.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image17.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image17.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image18.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image18.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image19.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image19.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image20.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image20.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image21.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image21.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image22.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image22.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image23.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image23.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image24.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image24.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image25.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image25.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image26.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image26.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image27.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image27.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image28.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image28.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image29.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image29.jpg') }}">
        		       </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image30.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image30.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image31.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image31.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image32.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image32.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image33.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image33.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image34.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image34.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image35.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image35.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image36.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image36.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image37.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image37.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image38.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image38.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image39.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image39.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image40.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image40.jpg') }}">
        		        </a>
        		    </div>
    		    </div>
    		    <div class="row container-group-images">
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image41.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image41.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image42.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image42.jpg') }}">
        		        </a>
        		    </div>
        		    <div class="col-md-3">
        		        <a data-fancybox="gallery" href="{{ asset('assets/frontend/galeria/image43.jpg') }}">
        		            <img src="{{ asset('assets/frontend/galeria/image43.jpg') }}">
        		        </a>
        		    </div>
        		    
    		    </div>
    		</div>
    	</div>
		<!--<div class="gallery-images">
			<div class="section1">
				<div class="container1">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image1.jpg') }}" alt="">
					</div>
				</div>
				<div class="container2">
					<div class="zoom-image2">
						<img src="{{ asset('assets/frontend/galeria/image2.jpg') }}" alt="">
					</div>
					<div class="zoom-image2">
						<img src="{{ asset('assets/frontend/galeria/image3.jpg') }}" alt="">
					</div>
				</div>
			</div>
			<div class="section2">
				<div class="container1">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image4.jpg') }}" alt="">
					</div>
				</div>
				<div class="container2">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image5.jpg') }}" alt="">
					</div>
				</div>
			</div>
			<div class="section3">
				<div class="container1">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image6.jpg') }}" alt="">
					</div>
				</div>
				<div class="container2">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image7.jpg') }}" alt="">
					</div>
				</div>
				<div class="container3">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image8.jpg') }}" alt="">
					</div>
				</div>
			</div>
			<div class="section4">
				<div class="container1">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image9.jpg') }}" alt="">
					</div>
				</div>
				<div class="container2">
					<div class="zoom-image">
						<img src="{{ asset('assets/frontend/galeria/image10.jpg') }}" alt="">
					</div>
				</div>
			</div>
		</div>-->
	</div>
	<div class="container container-contact">
		    <div class="col-md-12">
    			<h1>CONTÁCTANOS</h1>
			</div>
			<div class="col-md-12">
				<div class="box">
					<div class="badge-inner">
					        <div class="points">
								<span>
								<i class="fas fa-map-marker-alt"></i>
								</span>
								<span>
									Boulevard Playa del Carmen kilómetro 299
								</span>
							</div>
							<div class="points">
								<span>
									<i class="far fa-envelope"></i>
								</span>
								<span>
									<a href="mailto:info@senderosciudadmayakoba.com">info@senderosciudadmayakoba.com</a>
								</span>
							</div>
							<div class="points">
								<span>
									<i class="fas fa-phone"></i>
								</span>
								<span>
									Playa del Carmen: <a href="tel:+52(984)109 13 70">+52(984)109 13 70</a><br>
									Cancún: <a href="tel:+52(998)884 41 45">+52(998)884 41 45</a>

								</span>
							</div>
						<div class="badge-social-icons">
							<span><i class="fab fa-facebook-square"></i></span>
							<span><i class="fab fa-instagram"></i></span>						
							<span><i class="fab fa-linkedin-in"></i></span>
							<span><i class="fab fa-facebook-messenger"></i></span> 
						</div>
					</div>
					<form action="{{ url('contacto') }}" method="post">
					    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    					<div class="form-group inputs">
    						<input class="form-control" name="firstname" placeholder="Nombre">
    						<input class="form-control" name="lastname" placeholder="Apellidos">
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group inputs">
    						<input class="form-control" name="phone" placeholder="Teléfono">
    						<input class="form-control" name="email" placeholder="Email">
    					</div>
    					<div class="form-group inputs">
    						<input class="form-control" name="interesting" placeholder="Interesados en">
    						<input class="form-control" name="reason" placeholder="Motivo">
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group inputs">
    						<textarea class="form-control" name="message" rows="5" placeholder="Mensaje"></textarea>
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group input-submit">
    						<input type="submit" class="btn btn-primary" value="ENVIAR">
    					</div>
    				</form>
				</div>
			</div>
		</div>

@endsection

@section('contenedor-inferior')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>

@endsection