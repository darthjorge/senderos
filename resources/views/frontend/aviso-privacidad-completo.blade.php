@extends("frontend/layout")

@section("contenedor-superior")

<link rel="stylesheet" href="{{ asset('assets/css/amenidades.css') }}">
@endsection

@section('contenedor-principal')

<!--<div class="container-fluid img-fondo">-->
<!--	<div class="div-bird"></div>-->
<!--	<div class="overlay-custom">-->
<!--		<div class="col-md-12 text-center">-->
<!--			<h1>Aviso de Privacidad</h1>-->
			
<!--		</div>-->
<!--	</div>-->
<!--	<div>-->
<!--		<p class="line-green"></p>-->
<!--	</div>-->
<!--</div>-->
<div class="container-fluid img-fondo" style="height:auto;">
	<div class="col-md-12 text-center">
    	<h1>Aviso de Privacidad Completo</h1>
	</div>
</div>


	<div class="clearfix"></div>
	<div class="container">

		
		<h2>AVISO DE PRIVACIDAD INTEGRAL NOVATERRA CARIBE, S.A.P.I. DE C.V</h2>

<p>En cumplimiento y apego a lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares y su Reglamento damos a conocer los siguientes puntos:</p>

<h3>1.- RESPONSABLE DE DATOS PERSONALES.</h3>

<p>Para la protección de datos personales de titulares, NOVATERRA CARIBE, S.A.P.I. DE C.V., es responsable del uso y manejo de los mismos, teniendo como domicilio ubicado en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, mismo que está destinado para oír y recibir notificaciones.</p>

<h3>2.- FINALIDADES PRIMARIAS.</h3>

<p>Sus datos personales serán utilizados para prestar a usted servicios profesionales de promoción y venta de bienes inmuebles, realizar el trámite y respuesta de su solicitud de ejercicio de derechos “ARCO” o de su solicitud de revocación de consentimiento, para expedir la factura electrónica que corresponda a su consumo y para fines mercadotécnicos, publicitarios o de prospección comercial. Para lo cual es necesario recabar los siguientes datos personales: Nombre, Domicilio, Nacionalidad, Estado civil, Registro federal de contribuyentes, Correo electrónico, Número telefónico, Edad. Lo anterior será obtenido a través de nuestra página de internet www.senderosmayakoba.mx, por correo electrónico cuándo así usted nos contacte por ese medio o directamente en nuestras instalaciones o puntos de promoción y venta.</p>

<h3>3.- FINALIDADES SECUNDARIAS.</h3>

<p>NOVATERRA   CARIBE,  S.A.P.I.   DE   C.V.,   hace   de   su conocimiento que los datos podrán ser usados para las siguientes finalidades secundarias: Envió de nuestras promociones y descuentos para fines mercadotécnicos, publicitarios o de prospección comercial, Notificarle sobre nuevos servicios o productos que tengan relación con los ya contratados, con los que se considere puedan ser de su interés o bien comunicarle cambios en los mismos, Realizar evaluaciones periódicas de nuestros productos y servicios a efecto de mejorar la calidad de los mismos. Para las finalidades establecidas en el párrafo anterior, el titular de los datos cuenta con 5 días hábiles para manifestar su negativa, independientemente de estos días en cualquier momento del tratamiento el titular puede manifestar su negativa a través de (i) un escrito libre (dirigido a nuestra área de cumplimiento) firmado por su titular o representante con facultades entregado en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710 o bien (ii) mediante escrito libre firmado por su titular o representante con facultades enviado por correo electrónico a la siguiente dirección:  egarcia@novaterracaribe.com.mx.</p>

<h3>4.- TRANSFERENCIAS DE DATOS</h3>

<p>Los datos personales que han sido solicitados podrán ser transferidos a nuestras empresas filiales o subsidiarias de Obrascon Huarte Lain Desarrollos, S.L., RLH Propierties, S.A.B. de C.V., MKB Real Estate, S.A. de C.V., y Huaribe S.A. de C.V., Caribeurbana S.A.P.I. de C.V.; dicha acción se realizará con el fin de enviarle nuestras promociones y descuentos para fines mercadotécnicos, publicitarios o de prospección comercial, para notificarle sobre nuevos servicios o productos que tengan relación con los ya contratados, con los que se considere puedan ser de su interés o bien comunicarle cambios en los mismos, para realizar evaluaciones  periódicas  de  nuestros  productos  y  servicios  a efecto de mejorar la calidad de los mismos, para realizar el trámite y respuesta de su solicitud de ejercicio de derechos “ARCO” o de su solicitud de revocación de consentimiento, y para expedir la factura electrónica que corresponda a su consumo. Para ello requerimos el consentimiento de su parte en el presente aviso. En caso de que no obtengamos una oposición expresa de su parte para que sus datos personales sean transferidos en la forma y términos antes descritos, entenderemos que ha otorgado su consentimiento para ello</p>

<h3>5.- LIMITAR EL USO O DIVULGACIÓN DE SUS DATOS.</h3>

<p>Para que el titular pueda, en todo momento limitar el uso y divulgación de sus datos personales puede dirigirse a nuestra área de cumplimiento ubicada en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710 o bien mediante escrito libre firmado por su titular o representante con facultades enviando correo electrónico a la siguiente dirección egarcia@novaterracaribe.com.mx, o bien usted puede obtener más información al respecto en  www.senderosmayakoba.mx. Se hace de su conocimiento que existen otros mecanismos para limitar el uso y divulgación de sus datos como son Registro Público para Evitar  Publicidad (REPEP) de la PROFECO o el Registro Público de Usuarios (REUS) de la CONDUSEF de los cuales los puede consultar mayor información acerca de su registro en las páginas web de cada una de las dependencias.</p>




<h3>6.- PÁGINAS DE INTERNET.</h3>

<p>Por este  medio le  informamos  que en la siguiente dirección electrónica www.senderosmayakoba.mx; no se haceuso de cookies, web beacons o tecnologías similares mediante las cuales se obtienen datos personales. Se hace de su conocimiento que existen mecanismos para deshabilitar el uso de estas tecnologías consultando las herramientas y/o preferencias de su navegador de Internet.</p>

<p>a)	Información y/o documentos necesarios para el ejercicio de derechos ARCO: Solicitud de ejercicio de derechos, Copia de los documentos de acreditación del titular o del representante, Datos de contacto (Nombre, domicilio, correo electrónico y/o teléfono) del titular.<br>
b)	La solicitud para el ejercicio de derechos ARCO  deberá contener: Nombre y domicilio del titular, Dato del medio por el cual se estará en contacto para las notificaciones de la solicitud, Descripción clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos antes mencionados, y Dato que ayude para la localización de los datos (Servicio adquirido en NOVATERRA CARIBE, S.A.P.I. DE C.V.).</p>

<h3>8.- REVOCACIÓN DEL CONSENTIMIENTO</h3>
<p>Así como tenemos la obligación de atender sus solicitudes de derechos ARCO también puede pedir la revocación del consentimiento llenando el formulario que se encuentra en la siguiente dirección electrónica www.senderosmayakoba.mx, o entregar una solicitud firmada y por escrito a nuestra área de cumplimiento ubicada en Plaza Steren, locales 13, 14 y 15 manzana 30, lote 3, esquina Avenida Solidaridad, Playa del Carmen, Quintana Roo, C.P. 77710, o bien vía correo electrónico a la siguiente dirección   egarcia@novaterracaribe.com.mx, en éste último caso deberá confirmar la recepción del correo vía telefónica al número 52 (984)109 13 70  para darle la atención debida, para llevar a cabo la revocación de su consentimiento deberá acompañar una copia de la información y documentos señalados en los incisos a) y b) del numeral 7 anterior.</p>

<h3>9.- CAMBIOS EN EL AVISO DE PRIVACIDAD</h3>

<p>El presente aviso de privacidad puede sufrir  modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas. Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, mediante su publicación en nuestra página de internet www.senderosmayakoba.mx.</p>

<p>Última actualización: 6 de julio del 2018</p>

<div class="container text-center" style="margin-bottom: 15%;"></div>
</div>

@endsection

@section('contenedor-inferior')
<script>

	if( $(window).width() <= 1200)
	{

		var e1 = $(".section-4");
		e1.prev().detach().insertAfter(e1);

		var e2 = $(".section-8");
		e2.prev().detach().insertAfter(e2);
	}

</script>
@endsection
