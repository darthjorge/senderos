@extends("frontend/layout")

@section("contenedor-superior")
<style>
	
	body
	{
		background: url("/assets/frontend/index/img/fondo-index.jpg");
	}

	.transp
	{
		position: relative;
	}

	.disponibilidad
	{
	    padding: 8% 8% 12% 8%;
	    height: 100%;
	    text-align:center;
	}

	.senderos
	{
		width: 72%;
    	display: inline-block;
    	padding-right: 10px;
	}

	.etapa
	{
		width: 28%;
    	display: inline-block;
	}

	.selector
	{
		display: flex;
	}

	.btn-back, .btn-back:hover
	{
		background:#75c080;
		border-color: #75c080;
		border-radius: 0;
	}

	.box
	{
		background-color: #fbf9f97d;
	}


	.special-container
	{
		display: none;
	}

	.special-container img
	{
		height: 100%;
    	width: 100%;
	}

	#header-information .item
	{
		padding: 3%;
		height: auto;
	}
	
	#section-logo
	{
		max-width: 100%;
		height: auto;
		object-fit: cover;
	}
	
	.fondo-footer
	{
		margin-top: -9%;
	}

	select[name=sendero], select[name=etapa],select[name=sendero]:focus, select[name=etapa]:focus
	{
		background: #b0b0b0;
		border-color: #b0b0b0;
		border-radius: 0;
		box-shadow: inherit;
	}

</style>
<link rel="stylesheet" href="{{ asset('assets/libs/fancybox/dist/jquery.fancybox.css') }}">
@endsection

@section('contenedor-principal')

	<div class="container-fluid img-fondo">
		
	</div>
	<div class="container-fluid box">
		<div class="disponibilidad">
			<div id="header-information">
				<div class="col-md-4 item ">
					<a href="/desarrollos" class="btn btn-primary btn-back"><i class="fas fa-chevron-left"></i>&nbsp;&nbsp;&nbsp;Desarrollos</a>
				</div>
				<div class="col-md-4 item ">
					<img id="section-logo" src="{{ asset('assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/logo-senderos-ciudad-mayakoba.png') }}" alt="">
				</div>
				<div class="col-md-4 item selector">
					<div class="form-group senderos">
						<select name="sendero" class="form-control">
	    					<option value="1">Senderos Ciudad Mayakoba</option>
	    					<option value="2">Senderos Norte</option>
	    					<!-- <option value="3">Senderos Poniente</option> -->
	    				</select>
					</div>
					<div class="form-group etapa">
						<select name="etapa" class="form-control">
	    					<option value="1">Etapa 1</option>
	    					<option value="2">Etapa 2</option>
	    					<option value="3">Etapa 3</option>
	    					<option value="4">Etapa 4</option>
	    				</select>
					</div>
				</div>
			</div>
			<div id="senderos-ciudad-mayakoba-etapa1" class="special-container">
				<img src="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/mapa-disponibilidad-1er-etapa-senderos-mayakoba.jpg" alt="">
			</div>
			<div id="senderos-ciudad-mayakoba-etapa2" class="special-container">
				<img src="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/mapa-disponibilidad-2da-etapa-senderos-mayakoba.jpg" usemap="#image-map1" class="">
				<map name="image-map1">
					<a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/30-01.jpg">
						<area target="" alt="30-1" title="30-01" href="#" coords="663,600,576,656" shape="rect">
					</a>
					<a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/40-66.jpg">
				    	<area target="" alt="40-66" title="40-66" href="#" coords="412,101,454,165" shape="rect">
				    </a>
				</map>
			</div>
			<div id="senderos-ciudad-mayakoba-etapa3" class="special-container">
				<img src="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/mapa-disponibilidad-3er-etapa-senderos-mayakoba.jpg" alt="" usemap="#image-map2">
				<map name="image-map2">
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/29-02.jpg">
				    	<area target="" alt="29-02" title="29-02" href="" coords="396,84,459,148" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/27-63.jpg">
				    	<area target="" alt="27-63" title="27-63" href="" coords="510,319,579,357" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/31-23.jpg">
				    	<area target="" alt="31-23" title="31-23" href="" coords="492,643,528,711" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/31-29.jpg">
				    	<area target="" alt="31-29" title="31-29" href="" coords="628,646,664,711" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/31-30.jpg">
				    	<area target="" alt="31-30" title="31-30" href="" coords="671,646,701,712" shape="rect">
					</a>
				</map>
			</div>
			<div id="senderos-ciudad-mayakoba-etapa4" class="special-container">
				<img src="{{ asset('assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/mapa-disponibilidad-4ta-etapa-senderos-mayakoba.jpg') }}" alt="" usemap="#image-map4">
				<map name="image-map4">
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/24-40.jpg">
				    	<area target="" alt="24-40" title="24-40" href="" coords="596,25,630,80" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/24-34.jpg">
				    	<area target="" alt="24-34" title="24-34" href="" coords="755,26,787,79" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/24-32.jpg">
				    	<area target="" alt="24-32" title="24-32" href="" coords="810,26,856,75" shape="rect">
				    </a>
				    <a class="fancybox image" href="/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/popups/28-37.jpg">
				    	<area target="" alt="28-37" title="28-37" href="" coords="328,548,385,583" shape="rect">
					</a>
				</map>
			</div>
			<!-- senderos norte -->
			<div id="senderos-norte-etapa1" class="special-container">
				<img src="{{ asset('assets/frontend/desarrollos/disponibilidad/senderos-norte/mapa-disponibilidad-1er-etapa-senderos-norte.jpg') }}" alt="">
			</div>
			<div id="senderos-norte-etapa2" class="special-container">
				<div class="pattern"></div>
				<img src="{{ asset('assets/frontend/desarrollos/disponibilidad/senderos-norte/mapa-disponibilidad-2da-etapa-senderos-norte.jpg') }}" alt="">
			</div>
			<div id="senderos-norte-etapa3" class="special-container">
				<img src="{{ asset('assets/frontend/desarrollos/disponibilidad/senderos-norte/mapa-disponibilidad-3er-etapa-senderos-norte.jpg') }}" alt="">
			</div>

		</div>
	</div>

@endsection

@section('contenedor-inferior')
<script src="{{ asset('assets/libs/rwdImageMaps/jquery.rwdImageMaps.min.js') }}"></script>
<script src="{{ asset('assets/libs/fancybox/dist/jquery.fancybox.min.js') }}"></script>
<script>
	
	$(window).load(function()
	{
		var width = parseInt($("#senderos-ciudad-mayakoba-etapa1").width());
		var height = parseInt($("#senderos-ciudad-mayakoba-etapa1").height());
		
		$(".special-container img").css("width",width);
		$(".special-container img").css("height",height);

    	$('img[usemap]').rwdImageMaps();
	});

	$(document).on("ready",function()
	{
		$(document).ready(function() {
		$(".fancybox").fancybox();
	});
	
	    var desarrollo = "<?php if(isset($_GET["desarrollo"])) echo $_GET["desarrollo"] ?>";
	    
	    if(desarrollo == "senderos-norte")
	    {
	        $("select[name=sendero]").val(2);
	        $("select[name=etapa] option[value=" + 4 + "]").hide();
	        getMap($etapa = 1);
	    }
	    else
	    {
	        getMap($etapa = 4);
		    $("select[name=etapa]").val(4);
	    }

	});

	$(document).on("change","select[name=sendero]",function()
	{

		$etapa = $("select[name=etapa]").val();
		$etapa = parseInt($etapa);
		getMap($etapa);
		
		if($(this).val() == 2)
		{
		    $("select[name=etapa] option[value=" + 4 + "]").hide();
		    getMap($etapa = 3);
		    $("select[name=etapa]").val(3);
		    
		}
		else
		{
		    $("select[name=etapa] option[value=" + 4 + "]").show();
		}
		
		
		
	});

	$(document).on("change","select[name=etapa]",function()
	{

		$etapa = $(this).val();
		$etapa = parseInt($etapa);
		getMap($etapa);

	});

	function getMap($etapa)
	{
		$sendero = $("select[name=sendero]").val();
		$sendero = parseInt($sendero);
		process($sendero, $etapa);
	}

	function process($senderos, $etapa)
	{

		$(".special-container").css("display","none");

		switch($senderos)
		{
			case 1:

				$("#section-logo").attr("src","/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/logo-senderos-ciudad-mayakoba.png");
				$("#senderos-ciudad-mayakoba-etapa" + $etapa + "").fadeIn();

			break;

			case 2:

				$("#section-logo").attr("src","/assets/frontend/desarrollos/disponibilidad/senderos-norte/logo-senderos-norte.png");
				$("#senderos-norte-etapa" + $etapa + "").fadeIn();
			
			break;

			case 3:

				//$("#section-logo").attr("src","http://local.senderos.com/assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/logo-senderos-ciudad-mayakoba.png");
				$("#senderos-poniente-etapa" + $etapa + "").fadeIn();
			
			break;
		}

	}

</script>

@endsection