@extends("frontend/layout")

@section("contenedor-superior")
<style>
	.box-slide1
	{
		position: relative;
		width: auto;
		left: 0;
		height: 350px;
    	display: flex;
    	align-items: center;
    	margin-top: 0;
	}

	.box-white
	{
		position: relative;
		left: 0;
		max-width: 500px;
		display: flex;
    	align-items: center;
    	justify-content: center;
	}
	
	.box-slide .container
	{
		padding-top: 4%;
		padding-bottom: 4%;
	}

	@media(max-width: 1024px)
	{
		.box-slide
		{
			height: auto;
		}

		.box-white
		{
			max-width: inherit;
			width: 100%;
		}

		
	}
	

</style>
@endsection

@section('contenedor-principal')

<div class="container-fluid img-fondo" data-parallax="scroll" data-image-src="/assets/frontend/index/img/fondo-index.jpg" >
	<div id="mensaje">
		<img src="{{asset('assets/frontend/index/img/logo-senderos-poniente-blanco.png')}}"><br />
		<h2>TU NUEVA OPORTUNIDAD</h2><br />
	</div>
	<i class="fa fa-chevron-down"></i>
</div>



<div class="box-green"></div>


<div class="box-slide">
	<div class="container">
		<div class="col-md-6">
			<div class="box-slide1">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="item active">
				      <img class="d-block w-100" src="{{asset('assets/frontend/index/img/slide1.jpg')}}" alt="First slide">
				    </div><div class="clearfix"></div>
				    <div class="clearfix"></div>
				  </div> 
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>	
			</div>
		</div>
		<div class="col-md-6">
			<div class="box-white">
				<div>
					<h2>UN NUEVO <br> SENDERO</h2>
					<p>Lotes residenciales desde 160 m2 con vistas y accesos a parques y/o áreas verdes. Conexión subterránea. Infraestructura urbana. Iluminación ecológica en parques y avenidas. Entrada restringida con acceso por tarjeta. Seguridad privada 24/7</p>
					<hr>
				</div>
			</div>	
		</div>	
	</div>
</div>

<div class="clearfix"></div>
		<div class="col-md-12 box-compromiso">
			<h2>COMPROMISO SENDEROS</h2>
		</div>
<div class="clearfix"></div>

<div class="container-fluid box-photos">
	<div class="box-gallery">
		<p>DISEÑO<br>ARQUITECTÓNICO</p>
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/box-01.jpg')}}" alt="First slide">
	</div>
	<div class="box-gallery">
		<p>ARMONÍA DE<br>LOS ESPACIOS</p>
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/box-02.jpg')}}" alt="First slide">
	</div>
	<div class="box-gallery">
		<p>SUSTENTABILIDAD</p>
	    <img class="d-block w-100" src="{{asset('assets/frontend/index/img/box-03.jpg')}}" alt="First slide">
	</div>
	
	<div class="box-gallery">
		<p>CALIDAD <br>DE VIDA</p>
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/box-04.jpg')}}" alt="First slide">
	</div>
</div>


<div class="seccion-promo">
	<div class="box-guacamaya">
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/paj1.png')}}" alt="First slide">
	</div>

	<ul class="rslides">
	  <li><img src="{{asset('assets/frontend/slide/1.jpg')}}" alt=""></li>
	  <li><img src="{{asset('assets/frontend/slide/2.jpg')}}" alt=""></li>
	  <li><img src="{{asset('assets/frontend/slide/3.jpg')}}" alt=""></li>
	</ul>
	 
</div>

<div class="container">
	<div class="row">
		<div class="col-md-6 box-vendidos">
			<div class="box2">
					<div class="body">
						<a href="/desarrollos/disponibilidad?desarrollo=senderos-norte">
							<img src="{{asset('assets/frontend/index/img/senderos-norte-logo.png')}}" alt="First slide" width="322px">
						</a>
					</div>
					<div class="footer">
						<P>VENDIDO</P>
					</div>
				</div>
		</div>
		<div class="col-md-6 box-vendidos">
				
				<div class="box2">
					<div class="body">
						<a href="/desarrollos/disponibilidad">
						<img src="{{asset('assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/logo-senderos-ciudad-mayakoba.jpg')}}" alt="First slide" width="322px">
						</a>
					</div>
					<div class="footer">
						<P>95% VENDIDO</P>
					</div>
				</div>
		</div>	
	</div>
</div>

<div class="container-fluid box-vive-playa">
	<div class="container">
		<div class="row">
			<div class="col-md-12 box-pla-titulo">
				<h2>VIVE LO MEJOR DE PLAYA DEL CARMEN</h2>
			</div>
			<div class="col-md-4">
				<div class="box-vive-footer">
					<a href="" class="show-modal" data-toggle="modal" data-target="#myModal" data-title="Inversiones" data-description="
					<p><i class='fas fa-circle green'></i> Las inversiones inmobiliarias son una de las herramientas financieras más seguras.</p>
					<p><i class='fas fa-circle green'></i> La marca Senderos cuenta con el respaldo de 2 proyectos consolidados y el apoyo de Ciudad Mayakoba como una comunidad planeada.</p>
					<p><i class='fas fa-circle green'></i> Somos una empresa líder en el sector que ha marcado tendencia en la industria a nivel estatal con proyectos y conceptos innovadores.</p>
					<p><i class='fas fa-circle green'></i> La inversión en terrenos ofrece una protección a tu patrimonio en casos de inflación o devaluación.</p>
					<p><i class='fas fa-circle green'></i> Contamos con excelentes planes de financiamiento propio en pesos.</p>

					">
					<img class="d-block w-100" src="{{asset('assets/frontend/index/img/INVERSIONES.jpg')}}" alt="Inversiones"></a>
					<span class="espacio"></span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box-vive-footer">
					<a href="" class="show-modal" data-toggle="modal" data-target="#myModal" data-title="Calidad de Vida" data-description="
					<p><i class='fas fa-circle green'></i> Perfecto equilibrio entre la sustentabilidad, la seguridad y las comodidades de la vida moderna.</p>
					<p><i class='fas fa-circle green'></i> Ubicado en el corazón de la Riviera Maya, cercano a los principales atractivos turísticos de la zona y colindante con la principal vía de comunicación, el Bulevar Playa del Carmen que le proporciona óptima movilidad.</p>
					<p><i class='fas fa-circle green'></i> Dentro de Ciudad Mayakoba, la primera comunidad planeada de la Riviera Maya, con infraestructura de primer nivel, además de hospital, escuelas, parques, centros comerciales, oficinas, iglesia e instalaciones deportivas.</p>
					<p><i class='fas fa-circle green'></i> Amenidades como ciclopistas, casa club, cenotes, pet park y más harán de cada día una experiencia nueva.</p>">
					<img class="d-block w-100" src="{{asset('assets/frontend/index/img/CALIDADDEVIDA.jpg')}}" alt="Calidad de Vida"></a>
					<span class="espacio"></span>
				</div>	
			</div>
			<div class="col-md-4">
				<div class="box-vive-footer">
					<a href="" class="show-modal" data-toggle="modal" data-target="#myModal" data-title="Plusvalia" data-description="
					<p><i class='fas fa-circle green'></i> La Riviera Maya es la zona con mayor valor en el mercado inmobiliario.</p>
					<p><i class='fas fa-circle green'></i> La Riviera Maya tiene un constante crecimiento en inversiones e infraestructura.</p>
					<p><i class='fas fa-circle green'></i> Quintana Roo tiene un crecimiento económico que supera a la media nacional; en la zona norte el mercado inmobiliario está en auge.</p>
					<p><i class='fas fa-circle green'></i> Siendo un proyecto que incrementará su valor gracias a las amenidades y a un mantenimiento de altos niveles de calidad; es una excelente opción patrimonial.</p>">
					<img class="d-block w-100" src="{{asset('assets/frontend/index/img/PLUSVALiA.jpg')}}" alt="Plusvalia"></a>
					<span class="espacio"></span>
				</div>	
			</div>
		</div>
	</div>		
</div>




	

@endsection

@section('contenedor-inferior')

<!--<script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>-->
<script type="text/javascript">
	
	$('.carousel').carousel({
  		interval: 1000
	});
	
	$(".show-modal").on("click",function()
	{
	    
	    var title = $(this).attr("data-title");
	    var data = $(this).attr("data-description");
	    
	    $(".data-modal-title").html(title);
	    $(".data-modal-content").html(data);
	    
	});
	
	
	
	
</script>
<script>
  $(function() {
    $(".rslides").responsiveSlides();
  });
</script>


@endsection