@extends("frontend/layout")

@section("contenedor-superior")
<link rel="stylesheet" href="{{ asset('assets/css/contacto.css') }}">
<style>

	.form-control::-webkit-select-placeholder
	{
	    color: #929192;
	}

	.title-square h1
	{
		color: white;
	}

	

</style>
@endsection

@section('contenedor-principal')

	<div class="container-fluid img-fondo">
		<div class="container container-contact">
		    <div class="col-md-12">
		    	<div class="title-square">
    				<h1>CONTÁCTANOS</h1>
		    	</div>
			</div>
			<div class="col-md-12 padding0">
				<div class="box">
					<div class="badge-inner">
					        <div class="points">
								<span>
								<i class="fa fa-map-marker-alt"></i>
								</span>
								<span>
									Boulevard Playa del Carmen kilómetro 299
								</span>
							</div>
							<div class="points">
								<span>
									<i class="fa fa-envelope"></i>
								</span>
								<span style="font-size: 12px">
									<a href="mailto:ventas@senderosciudadmayakoba.com">ventas@senderosciudadmayakoba.com</a>
								</span>
							</div>
							<div class="points">
								<span>
									<i class="fa fa-phone"></i>
								</span>
								<span style="font-size: 12px">
									Playa del Carmen 1: <a href="tel:+52(984)109 13 70">+52(984)109 13 70</a><br>
									Playa del Carmen 2: <a href="tel:+52(998)884 41 45">+52(984)109 13 52</a><br>
									Showroom: <a href="tel:+529846885354">+52(984)688 53 54</a>
								</span>
							</div>
						<div class="badge-social-icons">
							<a href="https://www.facebook.com/SenderosCiudadMayakoba/" target="_blank">
								<span>
										<i class="fa fa-facebook-square"></i>
								</span>
							</a>
							<a href="https://www.instagram.com/senderoscdmk/" target="_blank">
								<span><i class="fa fa-instagram"></i></span>
							</a>
							<a href="http://m.me/senderosciudadmayakoba" target="_blank">
								<span><i class="fa fa-comments"></i></span>
							</a>
							<a href="https://www.youtube.com/channel/UCu6dVjmbD_ZxbcKuGTMQsww" target="_blank">
								<span><i class="fa fa-youtube"></i></span>
							</a>
						</div>
					</div>
					<form action="{{ url('contacto') }}" method="post">
					    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    					<div class="form-group inputs">
    						<div>
    							<input class="form-control" name="firstname" placeholder="Nombre">
    						</div>
    						<p class="space8"></p>
    						<div>
    							<input class="form-control" name="lastname" placeholder="Apellidos">
    						</div>
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group inputs">
    						<div>
    							<input class="form-control" name="phone" placeholder="Teléfono">
    						</div>
    						<p class="space8"></p>
    						<div>
    							<input class="form-control" name="email" placeholder="Email">
    						</div>
    					</div>
    					<div class="form-group inputs">
    						<div>
    							<select name="interesting" id="" class="form-control" placeholder="Interesados en">
	    							<option value="">Interesados en</option>
	    							<option value="Senderos lotes desde 300m²">Senderos lotes desde 300m²</option>
	    							<option value="Senderos Norte lotes desde 160m²">Senderos Norte lotes desde 160m²</option>
	    						</select>
    						</div>
    						
    						<p class="space8"></p>
    						<div>
	    						<select name="reason" id="" class="form-control">
	    							<option value="">Motivo</option>
	    							<option value="Información">Información</option>
	    							<option value="Financiamiento">Financiamiento</option>
	    						</select>	
    						</div>
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group inputs">
    						<textarea class="form-control" name="message" rows="5" placeholder="Mensaje"></textarea>
    					</div>
    					<div class="clearfix"></div>
    					<div class="form-group input-submit">
    						<input type="submit" class="btn btn-primary" value="ENVIAR">
    					</div>
    				</form>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('contenedor-inferior')

<script>

	if( $(window).width() < 768)
	{
		$(".badge-inner").insertAfter(".input-submit");
	}

</script>

@endsection