@extends("frontend/layout")

@section("contenedor-superior")

<link rel="stylesheet" href="{{ asset('assets/css/amenidades.css') }}">
@endsection

@section('contenedor-principal')
	
	<div class="container-fluid img-fondo">
		<div class="div-bird"></div>
		<div class="overlay-custom">
    		<div class="col-md-12 text-center">
    			<h1>AMENIDADES</h1>
				<p>SENDEROS de Ciudad Mayakoba se muestra como un magnífico acoplamiento entre el lujo y la naturaleza, un<br>
					oasis de diseño sofisticado con amenidades y servicios innovadores.<br>
					<strong>¡Descúbrelos todos!</strong>
				</p>
			</div>
		</div>
		<div>
			<p class="line-green"></p>
		</div>
	</div>
	<div class="col-md-12 padding0 fondo-planta">
		<div class="col-sm-12 col-md-12 col-lg-6 section-1">
			<h1>SENDEROS</h1>
			<img src="{{ asset('assets/frontend/amenidades/image1.jpg') }}" alt="">
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6 section-2 white-color chunk-left">
			<p class="text-right">
				El eje del proyecto son los senderos que recorren todo el <br> residencial y se conectan para caminar en las áreas verdes <br> que les rodean y a su vez comunican con los parques. 

			</p>
			<p class="text-right">
				A una distancia muy corta de tu casa siempre habrá un <br> sendero que te lleve a disfrutar los atractivos de <br>
				<strong>
					SENDEROS de Ciudad Mayakoba.
				</strong>
			</p>
			<div>
				<p class="line-green"></p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 padding0 fondo-planta">
		
		<div class="col-sm-12 col-md-12 col-lg-6 section-3 white-color">
			<p class="text-justify">
				<strong>SENDEROS de Ciudad Mayakoba</strong>
				cuenta con cinco parques:
			</p>
			<p class="text-justify">
				<strong>Parque Sendero de los Cenotes:</strong>
				selva nativa, observación de aves, pacíficos recorridos, auténtico contacto con la naturaleza.
			</p>
			<p class="text-justify">
				<strong>Parque Alameda de los Niños:</strong>
				juegos interactivos, fuentes, pista de patinaje, foro infantil.

			</p>
			<p class="text-justify">
				<strong>Baby Park:</strong>
				un lugar pensado especialmente para los más pequeños del hogar.
			</p>
			<p class="text-justify">
				<strong>Parque de las Mascotas:</strong>
				área de juegos y descanso para las mascotas de la familia.
			</p>
			<p class="text-justify">
				<strong>Parque Mini Veleros:</strong>
				Un espacio donde podrás disfrutar de la tranquilidad y belleza de nuestro espejo de agua mientras te diviertes navegando tu barco a escala.
			</p>
			<p class="text-justify">
				<strong>Parque de los niños:</strong>
				 un lugar lleno de juegos y diversión para disfrutar con la familia.
			</p>
			<div>
				<p class="line-green"></p>
			</div>
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6 section-4">
			<h1>PARQUES</h1>
			<img src="{{ asset('assets/frontend/amenidades/image2.jpg') }}" alt="">
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 padding0 fondo-planta">
		<div class="col-sm-12 col-md-12 col-lg-6 section-5">
			<h1>CASA CLUB</h1>
			<img src="{{ asset('assets/frontend/amenidades/image3.jpg') }}" alt="">
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6 section-6 white-color chunk-right">
			<p class="text-justify">
				<strong>Punto de encuentro para jóvenes y adultos, contará con:</strong>
			</p>
			<p class="text-justify">
				Gimnasio con máquinas para tonificación muscular, máquinas para entrenamiento aeróbico.
			</p>
			<p class="text-justify">
				<strong>Parque Alameda de los Niños:</strong>
				Alberca con 2 carriles de 25 metros, dimensionamiento olímpico para entrenamiento y practica de natación semi profesional.

			</p>
			<p class="text-justify">
				Alberca recreativa con barras de snack y zona de asoleaderos.
			</p>
			<p class="text-justify">
				Zona para Yoga y Tai Chi.
			</p>
			<p class="text-justify">
				Salón de usos múltiples con capacidad máxima para 60 personas; para reuniones, eventos, conferencias, proyecciones etc.
			</p>
			<p class="text-justify">
				Duchas y sanitarios.
			</p>
			<p class="text-justify">
				Terraza para eventos al aire libre con equipamiento para preparación de alimentos al carbón y con capacidad máxima de 60 personas.
			</p>
			<p class="text-justify">
				Cancha de pádel.
			</p>
			<div>
				<p class="line-green"></p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 padding0 fondo-planta">
		<div class="col-sm-12 col-md-12 col-lg-6 section-7 white-color">
			<p class="text-justify">
				<strong>SENDEROS de Ciudad Mayakoba</strong> privilegia el uso de la bicicleta y ha diseñado una ciclopista a lo largo de las vialidades principales, donde además de pedalear podrás realizar actividades como andar en patineta y ejercitarte rodeado de un ambiente natural; los recorridos podrán ser tan amplios como tu resistencia física.
			</p>
			<div>
				<p class="line-green"></p>
			</div>
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6 section-8">
			<h1>CICLOPISTA</h1>
			<img src="{{ asset('assets/frontend/amenidades/image4.jpg') }}" alt="">
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 padding0 fondo-planta">
		<div class="col-sm-12 col-md-12 col-lg-6 section-9">
			<h1>NATURALEZA</h1>
			<img src="{{ asset('assets/frontend/amenidades/image5.jpg') }}" alt="">
		</div>
		<div class="col-sm-12 col-md-12 col-lg-6 section-10 white-color chunk-right">
			<p class="text-right">
				<strong>SENDEROS de Ciudad Mayakoba</strong> es un acoplamiento perfecto entre el lujo y la naturaleza, un residencial que respeta y aprecia el medio ambiente. Contamos con más de 130 mil metros cuadrados de áreas verdes respetando la vegetación nativa, corredores ecológicos y pasos de animales en veinte calles del residencial. Tu hogar estará siempre rodeado de naturaleza.
			</p>
			<div>
				<p class="line-green"></p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>

@endsection

@section('contenedor-inferior')
<script>

	if( $(window).width() <= 1200)
	{

		var e1 = $(".section-4");
		e1.prev().detach().insertAfter(e1);

		var e2 = $(".section-8");
		e2.prev().detach().insertAfter(e2);
	}

</script>
@endsection