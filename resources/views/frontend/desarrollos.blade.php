@extends("frontend/layout")

@section("contenedor-superior")
<style>

/* overflow contacto.css */

body
{
    background:transparent !important;
}

.img-fondo
{
    height:auto !important;
}

/* overflow contacto.css */

.navbar
{

	background: url("/assets/frontend/index/img/fondo-index.jpg");
	padding: 0;
	margin: 0;
}
        
        .transp
        {
            position:relative;
        }
        
        .img-fondo2
        {
            background:transparent;
        }
        
        .img-fondo3 .container-fluid
        {
            padding:0;
        }
        
        .sombra
        {
            
            height:100%;
        
        }
        
       /* .sombra h2
        {
            
            border: 5px solid #fff;
            width: 300px;
            padding: 10px;
            margin: 0 auto;
            color: #414141;
            font-weight: bold;
        }*/

    @media(max-width:1024px)
    {
       
        
        
        .sombra
        {
            
            position:relative;
        }
        
        .box-desarrollos, .box-desarrollos .col-md-5, .img-fondo2 .container-fluid
        {
            padding:0;
        }
    }
    
    .see-availability
    {
        text-align: center;
        background: #96cbab;
        padding: 2%;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: 1px;
        font-size: 18px;
        

    }
    
    .caja
    {
        height:auto;
        max-height:inherit;
    }
    
    .box-availability
    {
        padding:5%;
    }
    
    .box-presell
    {
        padding-top:3%;
        max-width:190px;
        margin:0 auto;
    }
    
    .box-presell p
    {
        padding:5%;
    }

    .sombra
    {
    	position: relative;
    	height: auto;
    }
    
    /**/
    
    .contact-background2
    {
        background: url(/assets/frontend/index/img/gris.jpg);
        padding: 20px 0px;
    }
    
    .badge-inner
    {
        text-align: center;
    }
    
    @media(max-width: 768px)
    {

        .sombra h2
        {
            padding-top: 30px !important;
        }

        .img-fondo
        {
            padding-bottom: 0 !important;
        }

    }
    
</style>
<link rel="stylesheet" href="{{ asset('assets/css/contacto.css') }}">
@endsection

@section('contenedor-principal')

<div class="container-fluid img-fondo">
</div>
<div class="container-fluid padding0">
	<div class="sombra">
        <div class="title-square">
		  <h1>DESARROLLOS</h1>  
        </div>
		<div class="container box-desarrollos">
			<div class="col-md-5 col-md-offset-1">
				<div class="box-desarrollos">
					<div class="caja">
						<div class="caja-img">
							<img class="d-block w-100" src="{{asset('assets/frontend/desarrollos/disponibilidad/senderos-ciudad-mayakoba/logo-senderos-ciudad-mayakoba.png')}}" alt="First slide">
						</div>
						<p>Senderos ofrece lotes residenciales desde 300 m², siendo la primera comunidad integralmente planeada en la Riviera Maya; diseñada con base en cuatro pilares: sustentabilidad ambiental, planeación urbana, comunidad participativa y seguridad.</p>

						<div class="caja-ven">95% vendido</div>
						<div class="box-availability">
							<a href="desarrollos/disponibilidad" class="see-availability">VER DISPONIBILIDAD</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="box-desarrollos">
					<div class="caja">
						<div class="caja-img">
							<img class="d-block w-100" src="{{asset('assets/frontend/index/img/17223.png')}}" alt="First slide">
						</div>
						<p>Senderos Norte ofrece predios unifamiliares desde 160 m², con seguridad 24/7, servicios subterráneos, avenidas de bajo tráfico, áreas verdes, senderos y parques que cuentan con eco-iluminación para respetar el escenario principal: la naturaleza.</p>

						<div class="caja-ven">Vendido</div>
						<div class="box-availability">
							<a href="desarrollos/disponibilidad?desarrollo=senderos-norte" class="see-availability">VER DISPONIBILIDAD</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="box-green"></div>

<!-- <div class="desarrollo-cont">

	<div class="img-cont">
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/senderos123.png')}}" alt="First slide">
	</div>

	<div class="desarrollo-p">
		<p>Lotes residenciales enmarcados por naturaleza pura, infraestructura y <br>amenidades, todo dentro de un contexto de total seguridad.</p>
	</div>
</div> -->
<!-- <div class="container-fluid box-photos2">
	<div class="box-gallery2 zoom">
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/casa12.png')}}" alt="First slide">
		<p>Lotes dentro de un entorno altamente exclusivo</p>
	</div>
	<div class="box-gallery2 zoom">
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/fle12.png')}}" alt="First slide">
		<p>Alto nivel de plusvalía</p>
	</div>
	<div class="box-gallery2 zoom">
		
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/bill123.png')}}" alt="First slide">
		<p>Atractivos planes de financiamiento</p>
	</div>
	<div class="box-gallery2 zoom">
		
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/arb123.png')}}" alt="First slide">
		<p>Escenarios de inigualable belleza natural</p>
	</div>
	<div class="box-gallery2 zoom">
		
		<img class="d-block w-100" src="{{asset('assets/frontend/index/img/can123.png')}}" alt="First slide">
		<p>Seguridad <br>24/7</p>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="box-presell">
			    <p class="caja-ven">PRE-VENTA</p>
			</div>
			<div class="box-availability">
			    <a href="desarrollos/disponibilidad" class="see-availability">VER DISPONIBILIDAD</a>
			</div>
		</div>
	</div>	
</div> -->

<!-- <div class="mapa">
	<img src="{{asset('assets/frontend/desarrollos/map.jpg')}}" alt="Mapa Proyecto Senderos">
</div> -->


<!--<div class="container obras">
	<div class="box-obra-titulo">
		<h2>AVANCES DE OBRA</h2>
	</div>

	<div class="row box-obra1">
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra1.jpg')}}" alt="First slide"></a>
		</div>
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra2.jpg')}}" alt="First slide"></a>
		</div>
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra3.jpg')}}" alt="First slide"></a>
		</div>
	</div>

	<div class="row box-obra1">
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra4.jpg')}}" alt="First slide"></a>
		</div>
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra5.jpg')}}" alt="First slide"></a>
		</div>
		<div class="col-md-4">
			<a href="#"><img class="d-block w-100" src="{{asset('assets/frontend/index/img/obra6.jpg')}}" alt="First slide"></a>
		</div>

	</div>
</div>-->

<!-- <div class="box-green"></div>  -->

<div class="container seccion-planes">
	<div class="row titulo-planes">
		<div class="col-md-12"><h2>PLANES DE FINANCIAMIENTO</h2></div>
	</div>
	<div class="row box-planes">
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>Lotes desde 160m²</span>
            </p>
		</div>
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>Hasta 36 meses de financiamiento</span>
            </p>
		</div>
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>Plusvalía garantizada</span>
            </p>
		</div>
	</div>
	<div class="row box-planes">
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>30% de enganche</span>
            </p>
		</div>
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>Seguridad inigualable</span>
            </p>
		</div>
		<div class="col-md-4 padding0">
			<p>
                <span><i class="fa fa-check"></i></span>
                <span>Excelentes planes de financiamiento propio en pesos</span>
            </p>
		</div>
	</div>
</div>

<!--<div class="form-desarrollo">-->
<!--	<div class="container">-->
		
<!--		<div class="row">-->
<!--			<div class="col-md-4 col-md-offset-4 text-center">-->
<!--				<h2>Déjanos tus Datos</h2>-->

<!--				<form action="">-->
<!--					<input type="text" name="nombre" placeholder="Nombre">-->
<!--					<input type="text" name="nombre" placeholder="Teléfono">-->
<!--					<input type="text" name="nombre" placeholder="Correo">-->

<!--					<input type="submit" name="" value="Enviar" class="btn">-->
<!--				</form>-->

<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
<div class="container-fluid contact-background2">
    <div class="container container-contact">
        <div class="col-md-12">
    		<h1>CONTÁCTANOS</h1>
    	</div>
    	<div class="col-md-12">
    		<div class="box">
    			<div class="badge-inner">
    			        <div class="points">
    						<span>
    						<i class="fa fa-map-marker-alt"></i>
    						</span>
    						<span>
    							Boulevard Playa del Carmen kilómetro 299
    						</span>
    					</div>
    					<div class="points">
    						<span>
    							<i class="fa fa-envelope"></i>
    						</span>
    						<span>
    							<a href="mailto:info@senderosciudadmayakoba.com">info@senderosciudadmayakoba.com</a>
    						</span>
    					</div>
    					<div class="points">
    						<span>
    							<i class="fa fa-phone"></i>
    						</span>
    						<span>
    							Playa del Carmen: <a href="tel:+52(984)109 13 70">+52(984)109 13 70</a><br>
    							Cancún: <a href="tel:+52(998)884 41 45">+52(998)884 41 45</a>
    
    						</span>
    					</div>
    				<div class="badge-social-icons">
    					<!-- <span><i class="fab fa-facebook-square"></i></span>
    					<span><i class="fab fa-instagram"></i></span>						
    					<span><i class="fab fa-linkedin-in"></i></span>
    					<span><i class="fab fa-facebook-messenger"></i></span>  -->
                        <a href="https://www.facebook.com/SenderosCiudadMayakoba/" target="_blank">
                            <span>
                                    <i class="fa fa-facebook-square"></i>
                            </span>
                        </a>
                        <a href="https://www.instagram.com/senderoscdmk/" target="_blank">
                            <span><i class="fa fa-instagram"></i></span>
                        </a>
                        <a href="http://m.me/senderosciudadmayakoba" target="_blank">
                            <span><i class="fa fa-facebook-messenger"></i></span>
                        </a>
                        <a href="https://www.youtube.com/channel/UC8OhgJXGigU-beDCto8vmJQ" target="_blank">
                            <span><i class="fa fa-youtube"></i></span>
                        </a>
    				</div>
    			</div>
    			<form action="{{ url('contacto') }}" method="post">
    			    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    				<div class="form-group inputs">
    					<div>
                            <input class="form-control" name="firstname" placeholder="Nombre">
                        </div>
                        <p class="space8"></p>
    					<div>
                            <input class="form-control" name="lastname" placeholder="Apellidos">
                        </div>
    				</div>
    				<div class="clearfix"></div>
    				<div class="form-group inputs">
    					<div>
                            <input class="form-control" name="phone" placeholder="Teléfono">
                        </div>
                        <p class="space8"></p>
    					<div>
                            <input class="form-control" name="email" placeholder="Email">
                        </div>
    				</div>
    				<!-- <div class="form-group inputs">
    					<input class="form-control" name="interesting" placeholder="Interesados en">
    					<input class="form-control" name="reason" placeholder="Motivo">
    				</div> -->
                    <div class="form-group inputs">
                        <!-- <input class="form-control" name="interesting" placeholder="Interesados en"> -->
                        <div>
                            <select name="interesting" id="" class="form-control">
                                <option value="">Interesados en</option>
                                <option value="Senderos lotes desde 300m²">Senderos lotes desde 300m²</option>
                                <option value="Senderos Norte lotes desde 160m²">Senderos Norte lotes desde 160m²</option>
                            </select>
                        </div>
                        <p class="space8"></p>
                        <div>
                            <select name="reason" id="" class="form-control">
                                <option value="">Motivo</option>
                                <option value="Información">Información</option>
                                <option value="Financiamiento">Financiamiento</option>
                            </select>
                        </div>
                            <!-- <input class="form-control" name="reason" placeholder="Motivo"> -->
                    </div>
    				<div class="clearfix"></div>
    				<div class="form-group inputs">
    					<textarea class="form-control" name="message" rows="5" placeholder="Mensaje"></textarea>
    				</div>
    				<div class="clearfix"></div>
    				<div class="form-group input-submit">
    					<input type="submit" class="btn btn-primary" value="ENVIAR">
    				</div>
    			</form>
    		</div>
    	</div>
    </div>
</div>
	

@endsection

@section('contenedor-inferior')

@endsection