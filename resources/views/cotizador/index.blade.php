@extends("frontend/layout")

@section("contenedor-superior")
<style>
	.box-slide1
	{
		position: relative;
		width: auto;
		left: 0;
		height: 350px;
    	display: flex;
    	align-items: center;
    	margin-top: 0;
	}

	.box-white
	{
		position: relative;
		left: 0;
		max-width: 500px;
		display: flex;
    	align-items: center;
    	justify-content: center;
	}
	
	.box-slide .container
	{
		padding-top: 4%;
		padding-bottom: 4%;
	}

	@media(max-width: 1024px)
	{
		.box-slide
		{
			height: auto;
		}

		.box-white
		{
			max-width: inherit;
			width: 100%;
		}

		
	}
	

</style>
@endsection

@section('contenedor-principal')

<div class="container-fluid img-fondo" data-parallax="scroll" data-image-src="/assets/frontend/index/img/fondo-index.jpg" >
	
	<div class="row">
	  <div class="col-sm-8"></div>
	  <canvas id='canvas' width='1224' height='792'></canvas>

	</div>

</div>



<div class="box-green"></div>

@endsection