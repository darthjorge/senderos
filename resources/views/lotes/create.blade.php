@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
    <div class="row text-center">
        <div class="col-md-4">
            <form method="post" action="{{url('/create/lotes')}}">
        <div class="form-group">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <h3>CREA UN LOTE</h3><br />
            <input type="text" class="form-control" name="numero" placeholder="Numero del Lote" />
        </div>

        <div class="form-group">
          <label for="sel1">Selecciona el Status del Lote</label>
          <select class="form-control" id="sel1" name="status_code" onchange="setPrices(event)">
            @foreach($status as $state)
                <option value="{{$state->id}}">{{$state->name}}</option>
            @endforeach
          </select>
        </div> 

        <script>
            function setPrices(e) {
                if(e.target.value ==  1)
                    document.getElementById("precioUnidad").value = 4500

                else if(e.target.value ==  2)
                    document.getElementById("precioUnidad").value = 4900
                else if(e.target.value ==  3)
                    document.getElementById("precioUnidad").value = 5300
                else if(e.target.value ==  4)
                    document.getElementById("precioUnidad").value = 5900
                else if(e.target.value ==  5)
                    document.getElementById("precioUnidad").value = 6200
                else if(e.target.value ==  6)
                    document.getElementById("precioUnidad").value = 'Ingrese precio'
                else if(e.target.value ==  7)
                    document.getElementById("precioUnidad").value = 'Ingrese precio'
                
            }
        </script>

        <div class="form-group">
            <input type="text" class="form-control" name="mcuadrados" placeholder="Metros cuadrados" onchange="myPrecio(this.value)"/>
        </div>

        <div class="form-group">
            <input type="text" class="form-control" name="precio_unidad" placeholder="Precio por metros cuadrados" id="precioUnidad" value="4500" />
        </div>

        <div class="form-group">
            <input type="text" class="form-control" name="precio_final" placeholder="Precio Final" id="precioFinal">
        </div>

        <script>
            function myPrecio(val) {
                var base = document.getElementById('precioUnidad').value;
                var final = base * val

                console.log(final)

                document.getElementById('precioFinal').value = final
            }
        </script>

        <div class="form-group">
          <label for="sel1">Selecciona la manzana donde se encuentra el lote</label>
          <select class="form-control" id="sel1" name="manzana">
                <option value="001">Manzana 001</option>
                <option value="002">Manzana 002</option>
                <option value="003">Manzana 003</option>
                <option value="004">Manzana 004</option>
                <option value="005">Manzana 005</option>
                <option value="006">Manzana 006</option>
          </select>
        </div> 

        <button type="submit" class="btn btn-primary">Create</button>
        </form>
        </div>

        <div class="col-md-4">
            <img src="{{ asset('images/lote.jpg') }}" id="loteColor" alt="">

        </div>
    </div>
</div>
@endsection