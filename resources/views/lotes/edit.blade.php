@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
    <div class="row">
    <form method="post" action="{{action('LotesController@update', $id)}}" >
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="form-group">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <label for="name">Edita el numero del lote</label>
            <input type="text" class="form-control" name="name" value={{$lotes->numero}} />
        </div>

        <div class="form-group">
            <label for="description">Metros cuadrados</label>
            <input type="text" class="form-control" name="mcuadrados" value="{{$lotes->mcuadrados}}" />
        </div>

        <div class="form-group">
          <label for="sel1">Selecciona el Status del Lote</label>
          <select class="form-control" id="sel1" name="status_code">
            @foreach($status as $state)
                @if($state->id == $lotes->status_code)
                    <option value="{{$state->id}}" selected>{{$state->name}}</option>
                @endif
                <option value="{{$state->id}}">{{$state->name}}</option>
            @endforeach
          </select>
        </div> 

        <div class="form-group">
            <label for="description">Precio Unidad</label>
            <input type="text" class="form-control" name="precio_unidad" value="{{$lotes->precio_unidad}}" />
        </div>

        <div class="form-group">
            <label for="description">Precio Final</label>
            <input type="text" class="form-control" name="precio_final" value="{{$lotes->precio_final}}" />
        </div>

        <div class="form-group">
            <label for="description">Metros cuadrados</label>
            <input type="text" class="form-control" name="manzana" value="{{$lotes->manzana}}" />
        </div>

        <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
</div>
@endsection