@extends('layouts.app')

@section('content')
<div class="container">

	@if(\Session::has('success'))
	        <div class="alert alert-success">
	            {{\Session::get('success')}}
	        </div>
    @endif

	<br />

    <table class="table table-striped">
        <thead>
            <tr>
              <td>Numero lote</td>
              <td>Numero de Manzana</td>
              <td>Metros cuadrados</td>
              <td>Precio Unidad</td>
              <td>Precio Final</td>
              <td>Estado del lote</td>
              <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($lotes as $lote)
            <tr>
                <td>{{$lote->numero}}</td>
                <td>{{$lote->manzana}}</td>
                <td>{{$lote->mcuadrados}}</td>
                <td>{{$lote->precio_unidad}}</td>
                <td>{{$lote->precio_final}}</td>
                <td>
                    @if($lote->status_code == 1)
                      <span style="background-color: orange; padding:3px; border-radius: 5px;">Lote Disponible</span>
                    @endif

                    @if($lote->status_code == 2)
                      <span style="background-color: pink; padding:3px; border-radius: 5px;">Lote Disponible</span>
                    @endif

                    @if($lote->status_code == 3)
                      <span style="background-color: blue; color: white; padding:3px; border-radius: 5px;">Lote Disponible</span>
                    @endif

                    @if($lote->status_code == 4)
                      <span style="background-color: purple; color: white; padding:3px; border-radius: 5px;">Lote Disponible</span>
                    @endif

                    @if($lote->status_code == 5)
                      <span style="background-color: gold; padding:3px; border-radius: 5px;">Lote Disponible</span>
                    @endif

                    @if($lote->status_code == 6)
                      <span style="background-color: gray; color:white; padding:3px; border-radius: 5px;">Lote Reservado</span>
                    @endif

                    @if($lote->status_code == 7)
                      <span style="background-color: black; color: white; padding:3px; border-radius: 5px;">Lote Vendido</span>
                    @endif
                </td>
                <td><a href="{{url('/edit/lotes', [$lote->id]) }}">Edit</a></td>
                <td>Delete</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection