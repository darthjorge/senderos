<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Language::create([
            'id'    => '1',
            'name' => 'spanish',
            'code' => 'es'
        ]);

        Language::create([
            'id'    => 2,
            'name' => 'english',
            'code' => 'en'
        ]);
    }
}
