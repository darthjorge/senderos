<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Project::create([
			'id'       		=> 1,
			'name'     		=> 'Diseño Urbano',
			'image'    		=> '',
			'language_id' 	=> 1
        ]);

        Project::create([
			'id'       		=> 2,
			'name'     		=> 'Diseño Arquitectónico',
			'image'    		=> '',
			'language_id' 	=> 1
        ]);

        Project::create([
			'id'       		=> 3,
			'name'     		=> 'Comerciales y servicios',
			'image'    		=> '',
			'language_id' 	=> 1
        ]);


        Project::create([
			'id'       		=> 4,
			'name'     		=> 'Urban Design',
			'image'    		=> '',
			'language_id' 	=> 2
        ]);

        Project::create([
			'id'       		=> 5,
			'name'     		=> 'Architectural Design',
			'image'    		=> '',
			'language_id' 	=> 2
        ]);

        Project::create([
			'id'       		=> 6,
			'name'     		=> 'Commercial and services',
			'image'    		=> '',
			'language_id' 	=> 2
        ]);

    }
}
