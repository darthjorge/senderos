<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFeatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_feature', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('image1');
            $table->string('image2');
            $table->string('image3');
            $table->string('description');
            $table->integer('project_gallery_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->timestamps();
        });

        // Table additional properties
        Schema::table('project_feature', function ($table)
        {
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->foreign('project_gallery_id')->references('id')->on('project_gallery')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_feature');
    }
}
