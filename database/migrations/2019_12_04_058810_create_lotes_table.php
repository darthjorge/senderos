<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->decimal('mcuadrados', 20,4);
            $table->decimal('precio_unidad', 20,4);
            $table->decimal('precio_final', 25, 4);
            $table->string('manzana');
            $table->unsignedinteger('status_code');
            $table->timestamps();

            $table->foreign('status_code')
                  ->references('id')->on('status_lotes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
